---
title: Siril 1.2.0
author: Cyril Richard
date: 2023-09-15T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.2.0/
version: "1.2.0"
linux_appimage: "https://free-astro.org/download/Siril-1.2.0-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.0-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.0_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.0-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.0-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.0.tar.bz2"
---

Nous sommes ravis de vous informer que Siril 1.2.0 est maintenant disponible. De nombreux bugs ont été corrigés depuis la précédente release candidate, ce qui rend cette nouvelle version apte à devenir la version finale.

Cette version n'apporte aucune nouveauté par rapport à la version 1.2.0-rc1, mais surtout un lot de correction de bugs très important. En effet, la liste des utilisateurs devenant de plus en plus importante, les rapports de bugs se sont accumulés et ont permis de résoudre un bon nombre de problèmes. Merci à vous.

**Liste des bugs connus**

* Aucun pour l'instant, on croise les doigts.

**Liste des bugs corrigés (depuis la rc1)**

* Correction d'un crash lors de la suppression des échantillons d'extraction de l'arrière-plan (#1123)
* Correction d'un crash dans le binning avec des images ushort (4d4d4878)
* Correction d'un crash dans findstar lorsqu'une étoile saturée est proche du bord.
* Correction des fuites de mémoire dans le code de déconvolution (3e122ad7)
* Correction du tri dans les colonnes les plus à droite de la fenêtre PSF dynamique (75586c04)
* Ajout de l'enregistrement des commandes tapées dans stdout (06f67292)
* Amélioration de la vérification du chemin et des messages pour le solveur local d'astrometry.net (Windows) (!524)
* Prévention d'un crash lors de l'utilisation des pipettes de l'outil de recomposition sans que les images ne soient chargées (!526)
* Correction de l'échec du script HaOiii si l'image d'entrée a des dimensions impaires (!533)
* Correction d'erreurs dans la gestion du processus GNUplot (!538)
* Correction d'un crash avec la commande seqextract_HaOIII (!535)
* Correction d'un crash lors de l'exportation d'un csv à partir d'un graphe vide (#1150)
* Correction de la suppression des informations RA/Dec lors de l'annulation d'une résolution astrométrique (#1119)
* Amélioration de la détection des erreurs et des messages pour l'astrométrie (!516)
* Correction de la spécification de la plage d'échantillonnage pour le solveur interne de Siril (!549)
* Correction de toutes les descriptions de commandes (!546)
* Correction de l'orientation SER après la conversion AVI (#1120)
* Correction de la remise à l'échelle des images floats au chargement lorsqu'elles ne sont pas dans la plage [0,1] (#1155)
* Correction de l'initialisation de l'image guide pour l'algorithme anti artefact de l'interpolation de l'alignement (#1114)
* Correction de la désactivation de la pondération dans l'interface graphique lorsque le widget n'est pas visible (#1158)
* Correction du comportement output_norm pour l'empilement afin d'ignorer les valeurs des bordures (#1159)
* Correction de la vérification de l'absence d'espace dans la conversion (#1108)
* Correction de l'absence de DATE_OBS sur seqapplyreg si interp est réglé sur NONE (#1156)
* Correction du problème de photométrie avec un fichier .ser 8-bit (#1160)
* Correction de la suppression des valeurs zéro dans les calculs d'histogramme (#1164)
* Correction de la taille des pixels après l'extration de HaOIII (#1175)
* Correction d'un crash lors du passage de constantes dans une expression pm (#1176)
* Correction d'un canal incorrect lors de l'ajout d'une étoile à partir d'une sélection dans le port RGB (#1180)
* Autoriser l'accès à un disque non local (#1182)
* Autoriser le défilement de l'interface utilisateur de la recomposition des étoiles lorsqu'elle est plus grande que le dialogue (#1172)
* Correction d'une mauvaise interprétation de matrice de Bayer après la conversion FITS-SER (#1193)
* Correction de l'erreur d'analyse des arguments de pixelmath (#1189)

# Sommaire

{{< table_of_contents >}}

# Téléchargements
Siril 1.2.0 est distribué comme d'habitude pour les 3 plateformes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://siril.readthedocs.io/fr/stable/installation/source.html).

# Quels sont les changements majeurs de cette version depuis la dernière version stable, 1.0.6

Le nombre de changements apporté est très important, probablement plus important qu'entre les versions 0.9.12 et 1.0.0. Il serait bien trop long, et je pense que personne ne les lirait, d'écrire des notes de version détaillées de chaque nouvelle fonctionnalité. Nous avons donc décidé de nous arrêter sur les plus importantes, et si possible, de renvoyer vers les tutoriels ou notre toute nouvelle documentation.

La liste complète des changements/améliorations/corrections, le Changelog en anglais, se trouve [ici](https://gitlab.com/free-astro/siril/-/raw/1.2.0/ChangeLog).

* Pour voir les évolutions apportées par les version beta, rendez-vous [ici](../2023-02-24-siril-1.2.0-beta1/) et [ici](../2023-03-12-siril-1.2.0-beta2).
* Pour voir les évolutions apportées par la version rc, rendez-vous [ici](../2023-06-01-siril-1.2.0-rc1).


# Contribuer à Siril
Bien sûr, Siril est un logiciel informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en découvrir, merci de nous [contacter](../../faq/#comment-puis-je-contacter-léquipe-de-siril-) ou d'[écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug si ce dernier n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). Une [page de la documentation](https://siril.readthedocs.io/fr/stable/Issues.html) a été écrite dans le but de vous aider a reporter un problème.


Les **contributeurs** de cette version sont : 

- **Cyril Richard**
- **Vincent Hourdin**
- **Cécile Melis**
- **Adrian Knagg-Baugh**
- **René de Hesselle**
- Fred DJN
- Alexander
- Antoine Hoeffelman
- Garth TianBo
- Jehan
- Jens Bladal
- Joan Vinyals Ylla Català
- Mario Haustein
- Rafel Albert
- Stefan Beck
- Udo Baumgart
- Victor Boesen Gandløse
- Yi Cao
- Yoshiharu Yamashita
- Zachary Wu


# Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
