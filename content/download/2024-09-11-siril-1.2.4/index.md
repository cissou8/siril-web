---
title: Siril 1.2.4
author: Cyril Richard
date: 2024-09-11T02:00:00+00:00
categories:
  - News
tags:
  - new release
aliases:
  - /download/1.2.4/
version: "1.2.4"
linux_appimage: "https://free-astro.org/download/Siril-1.2.4-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.4-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.4_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.4-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.4-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.4.tar.bz2"
---

### Siril 1.2.4 Release Notes

This release of Siril 1.2.4 continues the effort to stabilize the 1.2 series by addressing a number of bugs that were reported by our dedicated community of users. Although no new features have been introduced in this version, the program is now more robust and reliable. 

**List of fixed bugs (since 1.2.3):**

- Fixed CFA statistics (#1342)
- Fixed calibration command bug disabling cosmetic correction (#1348)
- Fixed calibration GUI not parsing correcting flat when using path parsing (#1348)
- Fixed non-deterministic parallel subsky RBF (#1352)
- Fixed incorrect deconvolution command arg setting (issue raised on pixls.us)
- Fixed GHT and HT not reverting previews from other dialogs (#1356)
- Fixed crash when saving image (#1353)
- Fixed execute button in wavelets not reverting preview (#1362)
- Fixed CLAHE crash on mono image (#1354)
- Fixed crash while extracting Green channel on CFA FITSEQ (#1305)
- Fixed inverted sequences on exporting to SER (#1361)
- Fixed a bug with histogram hi / mid / lo entry callback behavior (!735)
- Fixed free space disk computation on macOS (#1368)

# Downloads

Siril 1.2.4 is available for download on the most common platforms (Windows, macOS, GNU/Linux). You can download it from the [download](../../../download) page.

If you want to build Siril from source, please check the [installation](https://siril.readthedocs.io/en/stable/installation/source.html) page.

# A Stabilization Version

As with previous updates, Siril 1.2.4 is focused on stabilizing the 1.2 series based on user feedback. This ensures a more reliable and efficient experience, without introducing new features. We encourage you to report any bugs you find to help us continue improving Siril.

# What's Coming with Siril 1.4?

The development of Siril 1.4 is progressing very well, and the number of new features is truly monumental. This version will mark a real break from the 1.2 series, as it introduces a host of improvements and changes that will significantly enhance the user experience.

There are still a few things left to do, and as of now, it is impossible for us to provide a precise release date. We generally follow the rule: it will be released when it is ready.

Stay tuned for updates, as Siril 1.4 promises to be a major step forward for all users!

# Contribute to Siril

Of course, Siril is a computer program developed by fallible humans, and bugs may still exist. If you think you've discovered one, please [contact](../../faq/#how-can-I-contact-siril-team-) or [write](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) a bug report if it isn't already present in the [list of known bugs](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). A [documentation page](https://siril.readthedocs.io/en/stable/Issues.html) has been written to help you report a problem.

# Donate

Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development, you’re welcome to [donate](../../donate) a small amount of your choice.

