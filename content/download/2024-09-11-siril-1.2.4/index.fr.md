---
title: Siril 1.2.4
author: Cyril Richard
date: 2024-09-11T02:00:00+00:00
categories:
  - Nouvelles
tags:
  - nouvelle version
aliases:
  - /download/1.2.4/
version: "1.2.4"
linux_appimage: "https://free-astro.org/download/Siril-1.2.4-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.4-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.4_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.4-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.4-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.4.tar.bz2"
---

### Notes de version de Siril 1.2.4

Cette version de Siril 1.2.4 poursuit l'effort de stabilisation de la série 1.2 en corrigeant un certain nombre de bugs signalés par notre communauté d'utilisateurs dévoués. Bien qu'aucune nouvelle fonctionnalité n'ait été introduite dans cette version, le programme est désormais plus robuste et fiable.

**Liste des bugs corrigés (depuis la version 1.2.3) :**

- Correction des statistiques CFA (#1342)
- Correction d'un bug dans la commande de calibration désactivant la correction cosmétique (#1348)
- Correction de l'interface de calibration qui ne parvenait pas à interpréter correctement les flats en utilisant l'analyse des chemins (#1348)
- Correction d'un comportement non déterministe de la fonction RBF dans la soustraction du ciel parallèle (#1352)
- Correction de la configuration incorrecte des arguments de la commande de déconvolution (problème signalé sur pixls.us)
- Correction de GHT et HT qui ne rétablissaient pas les aperçus depuis d'autres dialogues (#1356)
- Correction d'un crash lors de l'enregistrement d'une image (#1353)
- Correction du bouton d'exécution dans les ondelettes qui ne rétablissait pas l'aperçu (#1362)
- Correction d'un crash de CLAHE sur une image monochrome (#1354)
- Correction d'un crash lors de l'extraction du canal vert sur une séquence FITSEQ CFA (#1305)
- Correction de séquences inversées lors de l'exportation en SER (#1361)
- Correction d'un bug avec le comportement des entrées du histogramme hi / mid / lo (#735)
- Correction du calcul de l'espace disque libre sur macOS (#1368)

# Téléchargements

Siril 1.2.4 est disponible en téléchargement pour les plateformes les plus courantes (Windows, macOS, GNU/Linux). Vous pouvez le télécharger depuis la page [téléchargements](../../../download).

Si vous souhaitez compiler Siril à partir des sources, veuillez consulter la page [installation](https://siril.readthedocs.io/fr/stable/installation/source.html).

# Une version de stabilisation

Comme pour les mises à jour précédentes, Siril 1.2.4 se concentre sur la stabilisation de la série 1.2 en fonction des retours des utilisateurs. Cela garantit une expérience plus fiable et efficace, sans introduire de nouvelles fonctionnalités. Nous vous encourageons à signaler tous les bugs que vous trouvez afin de nous aider à continuer d'améliorer Siril.

# Quoi de neuf avec Siril 1.4 ?

Le développement de Siril 1.4 progresse très bien, et le nombre de nouveautés est véritablement colossal. Cette version va marquer une véritable rupture avec la série 1.2, car elle introduit de nombreuses améliorations et changements qui amélioreront considérablement l'expérience utilisateur.

Il reste encore quelques points à finaliser, et pour le moment, il nous est impossible de fournir une date de sortie précise. En général, nous suivons la règle : cela sortira quand ce sera prêt.

Restez à l'écoute pour plus d'informations, car Siril 1.4 s'annonce comme une avancée majeure pour tous les utilisateurs !

# Contribuer à Siril

Bien entendu, Siril est un programme informatique développé par des humains faillibles, et des bugs peuvent toujours exister. Si vous pensez en avoir découvert un, veuillez [nous contacter](../../faq/#how-can-I-contact-siril-team-) ou [écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug s'il n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). Une [page de documentation](https://siril.readthedocs.io/fr/stable/Issues.html) a été écrite pour vous aider à signaler un problème.

# Faire un don

Développer des logiciels est amusant, mais cela prend aussi presque tout notre temps libre. Si vous aimez Siril et souhaitez nous soutenir dans le développement continu, vous êtes les bienvenus pour [faire un don](../../donate) d'un petit montant de votre choix.

