---
title: Siril 1.0.2
author: Cyril Richard
date: 2022-05-16T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.0.2/
version: "1.0.2"
linux_appimage: "https://free-astro.org/download/Siril-1.0.2-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.2-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.2-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.2.tar.bz2"
---

Continuant sur notre rythme incroyable d'une sortie par mois, nous avons le plaisir de vous annoncer la sortie de la nouvelle version de Siril 1.0.2. Celle-ci est une version de stabilisation, qui fait suite à la v1.0.1. Nous vous conseillons de mettre à jour sans attendre afin de profiter des corrections de bugs et nouveautés que nous allons aborder dans cette page.

### Téléchargements
Siril 1.0.2 est distribué comme d'habitude pour les 3 plates-formes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://free-astro.org/index.php?title=Siril:install/fr).

### Quelles sont les nouveautés
Cette version 1.0.2 embarque très peu de nouveautés car c'est avant tout une version de stabilisation de la 1.0.1 sortie le mois dernier. Elle comporte donc essentiellement des correctifs de bugs (et de crash!!) recensés par les utilisateurs et reportés sur [cette page](https://free-astro.org/index.php?title=Siril:1.0.1/fr#Probl.C3.A8me_connus). Cependant, l'outil de retrait de gradient a été complètement revu et corrigé et une nouvelle méthode d'interpolation a même été ajoutée.

Bien sûr, Siril est un logiciel informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en découvrir, merci de nous [contacter](../../faq/#comment-puis-je-contacter-léquipe-de-siril-) ou d'[écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug si ce dernier n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).

#### Le retrait de gradient, la grande nouveauté
Toute la beauté des logiciels open-source réside dans le fait que chacun peut lire et modifier le code informatique. Ainsi, une communauté se crée autour du programme avec des collaborations, occasionnelles ou non. C'est ainsi que le développeur principal du tout nouveau logiciel [GraXpert](https://www.graxpert.com/), dédié à la suppression de gradient dans les images astro et écrit en Python, nous a aidé a implémenter un tout nouvel algorithme dans Siril. Cet algorithme, au doux nom de [RBF](https://en.wikipedia.org/wiki/Radial_basis_function_interpolation), pour Radial Basis Function, permet de traiter des gradients beaucoup plus complexes que ne le permet l'algorithme polynomial.

Un nouveau tutoriel sur le retrait de gradient a été écrit et est disponible [ici](../../tutorials/gradient).

**Contributeurs**:
Les contributeurs de cette verion sont : Cécile Melis, Vincent Hourdin, Cyril Richard, Steffen Hirtle

### Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
