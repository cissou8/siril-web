---
title: Siril 1.0.2
author: Cyril Richard
date: 2022-05-16T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /download/1.0.2/
version: "1.0.2"
linux_appimage: "https://free-astro.org/download/Siril-1.0.2-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.2-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.2-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.2.tar.bz2"
---

Keeping up on the fantastic pace of one release per month, we are pleased to announce the release of Siril 1.0.2. This is once again a stabilisation version and a continuation of 1.0.1. We recommend you update to benefit from the bug corrections and new features described in this page.


### Downloads
Siril 1.0.2 is distributed for the 3 most common platforms (Windows, MacOS, GNU / Linux). Check out the [downloads](../../../download) page.

But of course, as Siril is a free software, you can build from the sources, available through the [installation](https://free-astro.org/index.php?title=Siril:install) page.

### So what's new
This version 1.0.2 has very few new features because it is above all a stabilisation version of the 1.0.1 released last month. So it contains mainly bug fixes reported by users and available through this [page](https://free-astro.org/index.php?title=Siril:1.0.1#Known_issues). However, the Backgroung Extraction tool has been greatly improved, and we will see the changes in the next section.

Of course, despite all our efforts, bugs may still exist. If you think you have found one, please reach out to us through the [forum](https://discuss.pixls.us/c/software/siril/34). Even better, do file a bug report [there](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) if you see it is not already listed as a [known bug](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).

#### Background extraction
The beauty of open-source software lies in the fact that anybody can read and modify the sources. As the community grows around a software, some users step up and decide to contribute, whether it is for a single contribution or on regular basis. That's how the head dev of the brand new [GraXpert](https://www.graxpert.com/), dedicated to gradient removal in astrophotography and written in Python, reached out to us and helped implementing a new algorithm in Siril. This algorithm, that goes by the name [RBF](https://en.wikipedia.org/wiki/Radial_basis_function_interpolation) for Radial Basis Function, enables to remove much more complex gradients than the existing polynomial tool.

A new tutorial on gradient extraction has been written for the occasion and can be found [here](../../tutorials/gradient).

**Contributors**:
Contributors to this release are: Cécile Melis, Vincent Hourdin, Cyril Richard, Steffen Hirtle.

### Donate 
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.
