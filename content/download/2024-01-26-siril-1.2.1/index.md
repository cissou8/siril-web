---
title: Siril 1.2.1
author: Cyril Richard
date: 2024-01-26T00:00:00+00:00
categories:
  - News
tags:
  - new release
aliases:
  - /download/1.2.1/
version: "1.2.1"
linux_appimage: "https://free-astro.org/download/Siril-1.2.1-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.1-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.1_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.1-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.1-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.1.tar.bz2"
---

We are pleased to announce that Siril 1.2.1 is now available. Many bugs have been fixed compared to the 1.2.0 release last year, and we hope that version 1.2.1 will be as stable as possible.

**List of known bugs**

* None for the moment, fingers crossed.

**List of fixed bugs (since 1.2.0)**

* Fixed Anscombe VST noise reduction option for mono images
* Fixed HEIF import (#1198)
* Fixed Noise Reduction Anscombe VST bug with mono images (#1200)
* Fixed problems with Fourier Transform planning > Estimate (#1199)
* Fixed data initialisation bugs in copyfits() and RGB compositing tool
* Fixed exported x-column for lightcurves when Julian date is not selected (#1220)
* Fixed sampling tolerance for astrometry which was incorrectly read (#1231)
* Allowed for RA/DEC to be sorted in PSF windows (#1214)
* Added SET-TEMP as valid FITS header to be saved (#1215)
* Added configurable color for background extraction sample and standard annotations (#1230)
* Fixed argument parsing error in makepsf (!593)
* Fixed light_curve and csv export from plot when some images were unselected from the sequence (#1169)
* Added undo/redo when platesolving with astrometry.net (#1233)
* Fixed crash in findstar when detecting stars close to the border (#1237)
* Fixed using wcs info when using light_curve command (#1195)
* Allowed moving file into workdir to be picked up for livestacking (#1223)
* Fixed the way we check if there is enough space to use quick photometry (#1238)
* Fixed bit depth evaluation for 8-bit images (#1244)
* Fixed division by 0 in PixelMath (#1249)

# Table of Contents

{{< table_of_contents >}}

# Download
Siril 1.2.1 is distributed for the 3 most common platforms (Windows, macOS, GNU / Linux). Check out the [downloads](../../../download) page.

But of course, as Siril is a free software, you can build from the sources, available through the [installation](https://siril.readthedocs.io/en/stable/installation/source.html) page.

# A stabilization version
As previously stated, this is a stabilization release and does not bring any new features. It is, however, an important release, in response to the many comments received following the release of the first version. One feature has been added, however: color configuration. Indeed, as some people have difficulty seeing colors, we decided to make them configurable. However, in this version, only annotations and gradient removal samples are configurable.
{{<figure src="config_color.png" link="config_color.png" width="100%" caption="Preferences window with color settings.">}}

# And the future? Siril 1.4.0, major changes
In the meantime, we're not content with tracking down bugs in the stable version. In fact, VERY active development is intensifying on the development version, currently called Siril-1.3.0-alpha-dev. The new features are enormous, and we're going to try and present the main ones in this release note.

## Siril Plot, a graphical display system in Siril
The biggest improvements are not necessarily the most visible. For the time being, Siril 1.2.1 requires the installation of GNUPlot, an external program, to display certain graphics, notably light curves. Without going into too much detail, the use of external programs complicates maintenance, especially for a multiplatform software like Siril. It was therefore decided to develop our own graph display module, in order to be able to display large amounts of data more simply. We called this module Siril Plot. It allows you to interact with the graph. You can zoom in and out, or save in different formats.

## Intensity profiling
This is probably the oldest open ticket in Siril and was recently resolved. It is directly based on the development of Siril Plot, and allows the display of intensity profiles when a straight line is drawn on the image.
{{<figure src="profile.png" link="profile.png" width="100%" caption="Intensity profile of a photo region.">}}

## Refactoring of L-RGB compositing
One of the biggest weaknesses of the L-RGB compositing module was the need to use previously aligned images as input, if field rotation was involved. Aware of the difficulty this represented for some of you, we have improved the tool to make it simpler. It is now possible to align images with a single click.
{{<figure src="compositing.png" link="compositing.png" width="100%" caption="New, improved L-RGB composition tool.">}}

## New external script retrieval system
You may have already noticed in version 1.2.1, that when you click on "Get more scripts", Siril redirects the user to a [documentation page](https://siril.readthedocs.io/en/stable/Scripts.html#getting-more-scripts). This page contains a link to another page, this time a GitLab repository, which contains numerous third-party scripts. Anyone can add scripts to this repository, provided they follow the correct procedure, and then the user just has to use them. One example is the star reduction script. In the development version, we have simplified the process. Any script previously deposited in this repository can be retrieved from Siril.
{{<figure src="gitscripts.png" link="gitscripts.png" width="100%" caption="Repository scripts are automatically detected by Siril and can be installed with a single click.">}}

## Improved color management and ICC profiles
Color management is a very important new feature of Siril 1.4. It is now possible to use custom or standard color profiles, and thus produce images with faithful color rendering. While color management may not be of prime importance in astronomy during the main part of the process, it becomes so in the final stage. All the more so if the user is juggling several software programs. To this end, a color management tab has been added to Siril's preferences, and the internal image display mechanism has evolved considerably. To find out more, take a look at the [development version documentation](https://siril.readthedocs.io/en/latest/Color-management.html), where everything is explained in detail, including screenshots.
{{<figure src="ICC_dialog.png" caption="Window used to manipulate the ICC profile files of the loaded image.">}}

## Managing distortions in astrometry
Siril's astrometry has improved over the years. This is once again the case with this development version. This time, however, the improvement goes a step further by taking into account image distortions, generally related to the optics used. This functionality is essential for mosaic management, which is the subject of future developments, but also for tools such as color calibration. Knowing the distortion of the image is essential for accurately locating stars near the edges of the image, not just near the center.
An interesting consequence of supporting astrometric distortion is the ability to display guides highlighting optical defects. There's no better way to discover tilt or backfocus errors.
{{<figure src="BF.png" width="100%" caption="A single command displays image distortions, allowing you to visualize optical defects.">}}

{{<figure src="wcs_disto.png" link="wcs_disto.png" width="100%" caption="Comparison of linear plate solving on the left and cubic plate solving on the right. You can see that the stars in the catalog (red crosses) are pointed very precisely (click to enlarge).">}}

## New spectrophotometric color calibration tool
Because everyone's equipment is different, CMOS sensor response is not identical from one model to another, and filter transmission is brand-specific, photometric color calibration is not accurate in all circumstances. To achieve precise colorimetry, it's important to take all these parameters into account. The SPCC (SpectroPhotometric Color Calibration) tool does just that, and operates in a very similar way as PCC. However, unlike photometric color calibration, which can work with local stellar catalogs, this one requires an Internet connection.

{{<figure src="SPCC.png" link="SPCC.png" width="100%" caption="Example of use of the spectrophotometry color calibration tool, using stars up to magnitude 17.">}}

This tool uses the transmission data of the main filters on the market, as well as the response curves of the sensors generally used in astrophotography. However, the equipment you use may not be listed here. That's why we've set up a participatory repository, where anyone can add data corresponding to their own equipment and share it with the whole community. The repository can be found [here](https://gitlab.com/free-astro/siril-spcc-database) and the simple procedure is explained in the README. This database also includes a reference list of the white references used in SPCC. We are counting on you to improve this tool by contributing to the creation of one of the largest free and open databases of astrophotographic material.

## Opening PixInsight's XISF images, and the new image format
One of our most frequent requests concerned the ability to open XISF images, PixInsight's "new" default image format. This is now possible. However, it is not, and will not be, possible to save in this format in Siril. An explanation is given in the [documentation for this development version](https://siril.readthedocs.io/en/latest/file-formats/XISF.html).
Finally, a new format has been added to Siril: JPEG XL. This format is intended to replace the JPEG format. It offers better compression (even lossless) and bit depths of up to 32, ideal for astronomical images. We don't know if this format will ever replace JPEG, but we thought it would be interesting to give you the choice to use it.

# When will Siril 1.4.0 be available?
To all the impatient people who, after reading this release note (probably 2-3 people), are wondering when Siril 1.4.0 will be available? Unfortunately, the answer is: not yet. We've got into the habit of releasing versions when they're ready, finished and complete. This will be no different. What's more, there's still a lot that hasn't been started yet. Be patient, it's for your own good.

Finally, if you can't resist, Siril is free and opensource. It's easy to get the source code and compile the project. Automatically compiled binaries are also available. But beware: we don't recommend using these versions (which change daily) for anything other than testing. Bugs, crashes and data corruption may occur.

# Contribute to Siril
Of course, Siril is a computer program developed by fallible humans, and bugs may still exist. If you think you've discovered one, please [contact](.../.../faq/#how-can-I-contact-siril-team-) or [write](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) a bug report if it isn't already present in the [list of known bugs](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). A [documentation page](https://siril.readthedocs.io/en/stable/Issues.html) has been written to help you report a problem.


# Donate
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.
