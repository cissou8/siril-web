---
title: Siril 1.2.0-rc1
author: Cyril Richard
date: 2023-06-01T00:00:00+00:00
categories:
  - News
tags:
  - new release
aliases:
  - /download/1.2.0-rc1/
version: "1.2.0-rc1"
linux_appimage: "https://free-astro.org/download/Siril-1.2.0-rc1-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.0-rc1-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.0-rc1_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.0-rc1-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.0-rc1-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.0-rc1.tar.bz2"
---

We are pleased to inform you that Siril 1.2.0rc1 is now available, only a few weeks after the release of beta1. Many bugs have been fixed since the previous beta, which makes this new version more stable and reliable.

However, we remind you that the release candidate version of the software is an intermediate stage of development that precedes the final version. It should be noted that a release candidate may contain bugs, unfinished features and stability issues. It is made available to users for feedback and extensive testing. Your contribution in reporting problems will help the developers to improve the final version of the software.

**List of known bugs**

* Crash in background extraction samples removing
* Crash in binning with ushort images
* Crash in findstar when a large saturated patch was close to border

**List of fixed bugs (from beta 2)**

* Fixed handling of special characters in sequence name during conversion
* Fixed crash in seqstarnet when processing single-file sequences (SER, FITSEQ)
* Fixed hang in seqstarnet when processing a single-file sequence with no star mask
* Fixed using default masters names in calibrate (GUI)
* Fixed invoking external programs in CLI mode (Windows only)
* Fixed stream setting for all versions of ffmpeg (mp4 export crash)
* Fixed crash when doing manual registration of sequence with variable image sizes (now disabled)
* Fixed UI and command issues in deconvolution code
* Fixed star recomposition issue where LIVETIME and STACKCNT could be doubled when processing the starless and star mask parts of the same image
* Fixed "image copy error in previews" bug in asinh transformation
* Fixed reset of the viewports when reopening the RGB composition tool after opening a mono sequence
* Fixed star detection for undersampled mono images
* Fixed sequence cleaning with opened image and resetting the reference image
* Fixed photometry with 32bit images from PRISM
* Fixed incorrect behaviour when resetting right-hand stretch type in star recomposition tool
* Fixed sequence handling when images have different number of layers
* Fixed GNUplot terminals so they remain interactive and free resources when closed
* Fixed crash that could occur when parsing string keywords that contained forbidden characters
* Fixed calling external processes that left too many opened file descriptors
* Fixed Stop behavior for starnet and local astrometry.net
* Fixed crash when running merge_cfa in headless mode
* Fixed console logs output on Windows when messages contained widechars
* Fixed networking detection at build-time and made it and exiv2 optional
* Fixed bug in NetPBM file import
* Changed Windows to build in console mode even for stable releases
* Changed gnuplot initialization to keep plots open after stopping main process (and fixed leak)
* Changed image opening for all images not from Siril (ensures DATAMAX is correct)
* Improved parsing of localization data for wider imaging software compatibility
* Improved DATE-OBS control and log for solar system features
* Improved clipping model in luminance-based GHS stretches
* Improved Wayland support
* Reviewed and fixed coordinate conversion from WCS to display for annotations, nomad command, pcc
* Improved astrometry.net handling on Windows to support more recent cygwin builds
* Updated URLs to query online catalogs

**New features**

* Added GHS saturation stretch mode
* Added special formatter to parse date-time in path parsing
* Added Astro-TIFF in sequence export
* Added read of datetime in jpeg and png file with exiv2
* Added Danish language

# Table of Contents

{{< table_of_contents >}}

# Downloads

Siril 1.2.0rc1 is distributed for the 3 most common platforms (Windows, macOS, GNU / Linux). Check out the [downloads](../../../download) page.

But of course, as Siril is a free software, you can build from the sources, available through the [installation](https://siril.readthedocs.io/en/stable/installation/source.html) page.

# What are the major changes in this version since last stable version, 1.0.6

The number of changes is very large, probably larger than between versions 0.9.12 and 1.0.0. It would take too long, and I don't think anyone would read them, to write detailed release notes of every new feature. So we decided to focus on the most important ones, and if possible, to refer to the tutorials or our brand new documentation.

The complete list of changes/improvements/corrections, aka the Changelog, can be found [here](https://gitlab.com/free-astro/siril/-/raw/1.2.0-beta1/ChangeLog).

# New documentation

One of the great additions that accompanies this release is the availability of a new [documentation](https://siril.rtfd.io). The latter has been written and thought to match the application as closely as possible and to evolve at the same time. What could be more unpleasant than reading a documentation that is not up to date. Please note that this new documentation is not yet 100% finished and that translations are in progress. So far, we have had contributors for French and German translations. You can get translated documentation at the bottom of the left pane.

{{<figure src="../2023-02-24-siril-1.2.0-beta1/rtfd.langue.png" caption="Left panel to switch between languages and to access a PDF version.">}}

If you wish to contribute by translating in your own language, please reach out to us by posting a message on the [forum](https://discuss.pixls.us/c/software/siril/34) and we will happily make the necessary setup. No worries, no prior knowledge in coding is required, translations are handled by [weblate](https://weblate.pixls.us/projects/siril) hosted by our friends of [pixls.us](https://discuss.pixls.us) and it is as simple as typing in a browser.

We thank Udo Baumgart for his express translation of the German translation of the documentation.

We have also updated and added many new tutorials. You can check it out [here](https://siril.org/tutorials/).

# Star processing

One of the greatest new features is the introduction of a toolbox dedicated to stars processing.

First of all, efforts have been put in refining stars detection. Indeed, detecting accurately stars on any kind of image is quite a tedious task. One needs to differentiate it from other astronomical objects and from image artifacts, in order not to bias the subsequent analysis based upon their shape and size. The detection algorithm has been completely refactored to improve its efficiency and reliability.

It is now also possible to model stars using Moffat function, which in some cases, can represent them more accurately than the default Gaussian. Using a Moffat profile can be specified in the Dynamic PSF window.

{{<figure src="../2023-02-24-siril-1.2.0-beta1/dynamic-psf.png" caption="Dynamic PSF window, where many star detection options have been added.">}}

On top of better detection, the brand new tools are:

1. Starnet stars removal: Starnet is an external software which can remove stars from images. This can ease the processing of images where one would like to treat differently nebulosities and surrounding stars. In order to use it seamlessly in Siril, you will need to download and install the CLI version, and then tell Siril where it is located. The different steps are detailed in the [documentation](https://siril.readthedocs.io/en/stable/processing/stars/starnet.html).

{{<figure src="../2023-02-24-siril-1.2.0-beta1/starnet.png" caption="StarNet dialog.">}}

1. Stars recomposition: Once you have separately processed the stars and the other objects (nebulae, galaxies, comets...), you will need to recombine them together. This can be done in PixelMath of course, but even better, in the dedicated star recomposition tool. Detailed explanations can be found in the [documentation](https://siril.readthedocs.io/en/stable/processing/stars/star-recomp.html).

{{<figure src="../2023-02-24-siril-1.2.0-beta1/star-recomp.png" caption="Star recomposition tool dialog">}}

1. Unclip stars: If you try to apply some processing like deconvolution on saturated stars, you may end up with unwanted artifacts. Normally, one would try to avoid saturating stars during acquisition, but for the brightest ones, this is not always possible. This new version allows to unclip those stars, as simple as a click. Documentation for this feature can be found [here](https://siril.readthedocs.io/en/stable/processing/stars/unclipped.html)

1. Full star resynthesis: This tool aims at repairing heavily distorted stars, using a new star adjustment tool. This can prove useful to save images affected by coma or other optical aberrations. If Sirl can detect those stars, it can fix them. A warning here, the stars are completely artificial in that case. This tool must therefore be kept as a last resort and used as such. More explanations are given in the [documentation](https://siril.readthedocs.io/en/stable/processing/stars/resynthesis.html).

# New deconvolution tool

A new deconvolution tool has been developed to replace the legacy version, a bit too simple, which would tend to give very perfectible results in any cases. This new feature is much more comprehensive and packed with astronomy-dedicated methods. The dialog is split in two parts.

1. The top part is dedicated to the PSF calculation (the kernel that will be used to deconvolve the image). The latter can be determined blindly, using the stars detected in the image or by specifying manual parameters. There are many options which makes this tool usable in a wide variety of cases.
1. The bottom part specifies the deconvolution calculation. Two new algorithms, in addition to the legacy Lucy-Richardson, have been added. Note they are dedicated to lunar/planetary images.

This tool may look a bit tedious to master at first, and we therefore recommend to work on a cropped version of the image to process before using it on the full image. The [documentation](https://siril.readthedocs.io/en/stable/processing/deconvolution.html) will guide you step by step throughout the settings and buttons of the interface.

{{<figure src="../2023-02-24-siril-1.2.0-beta1/deconv-dialog.png" caption="Deconvolution dialog.">}}

# Denoising

Denoising feature, regularly asked for by users, has finally made its way into this new release. It is fully described [here](https://siril.readthedocs.io/en/stable/processing/denoising.html). It is very simple to use and works best on monochrome images. For color images obtained through debayering, it may turn out a bit to underperform a little in comparison.

{{<figure src="../2023-02-24-siril-1.2.0-beta1/denoise-dialog.png" caption="Denoising dialog.">}}

# Generalized Hyperbolic Stretch interface refactoring

Shortly after version 1.0 was released, we integrated a tool originally developed by a team of [amateur astronomers](https://ghsastro.co.uk/) which only existed as a PixInsight plugin: Generalized Hyperbolic Stretch. This tool allows to process images with high levels of noise by stretching data while preserving details. It offers improved flexibility compared to the more traditional Histogram Stretch as it prevents stars bloating. In this new version, we have completely refactored the dialog, with the addition of an histogram view which simplifies setting the parameters a lot. In order to unleash the full power of this tool, the original authors of the tool have written a [tuotrial](../../tutorials/ghs) and a page in the [documentation](https://siril.readthedocs.io/en/stable/processing/stretching.html#generalised-hyperbolic-stretch-transformations-ghs)].

{{<figure src="../2023-02-24-siril-1.2.0-beta1/ght-dialog.png" caption="Generalized Hyperbolic Stretch new dialog.">}}

# Astrometry and PCC refactoring

Astrometry has, once again, been given a boost, as it is the initial process for many other tools. First of all, it has clearly benefited from the improvements in stars detection, as explained above. Then, we added the possibility to solve the images while offline, by adding local star catalogues. Finally, for wide-field images, where the internal solver may still encounter some difficulties, we have added support for a local installation of astrometry.net. These new functionalities are detailed [here](https://siril.readthedocs.io/en/stable/astrometry/platesolving.html).

Closely related to astrometry, Photometric Color Calibration, PCC in short, has also been revamped. The two processes, astrometry and photometry, that were run in sequence before, have been completely decoupled. This subtle change now allows to run photometric calibration alone on images which have already been platesolved, for instance via an external program such as nova.astrometry.net or ASTAP. It can again prove useful for wide-field images that Siril internal solver may not solve. More details can be found [here](https://siril.readthedocs.io/en/stable/processing/colors.html#photometric-color-calibration).

# PixelMath Improvement

PixelMath tool has also had its share of improvements. It is now possible to save frequently-used formulas as presets for future use, which is handy if you like to apply the same over and over again. Just click on the button to the right of the formula you just typed and it will make its way to the **Presets** area at the bottom of the dialog (you can unfold to see what it contains). You can also force rescaling of the output to make sure your final image stays in the range [0, 1].

Finally, the most important change is the possibility to use statistical functions in the formulas. You can have a look at the [documentation](https://siril.readthedocs.io/en/stable/processing/pixelmath.html) to see the list of all the functions implemented, as well as a more detailed presentation of the tool.

{{<figure src="../2023-02-24-siril-1.2.0-beta1/pixelmath-dialog.png" caption="PixelMath dialog.">}}

# New 2-pass alignment

The main drawback of the current global alignment is the fact that the reference image has to be chosen beforehand. This behavior can of course be modified manually, by chosing the reference image with care, but this was limiting the use of automated scripts. We have therefore integrated a new global registration called `2-pass`, which works in two steps:

1. Run this new registration method. It will just save the registration information in the `.seq` file and pick the best possible image, based on number of stars and FWHM.
{{<figure src="../2023-02-24-siril-1.2.0-beta1/2pass.png" caption="2-pass global registration.">}}
2. You then can apply the registration info with the `Apply Existing Registration` method. You can decide to filter out some images based oon quality indicators such as FWHM or roundness, to avoid exporting poor quality frames. You can also choose to reframe the sequence with the new modes such as min, max or cog, on top of the current framing method.
{{<figure src="../2023-02-24-siril-1.2.0-beta1/applyreg.png" caption="Apply the existing registration which was previusly saved in the seq file.">}}

More information can be found [here](https://siril.readthedocs.io/en/stable/preprocessing/registration.html#pass-registration)

# New commands

An impressive number of new commands have been added, to enable more automation and build a fully-automated processing pipeline through scripting. You can now obtain a processed image from start to finish via a script. Please keep in mind that going through the Processing part manually is likely to give better results as you can control/adjust visually all the tools along the way.

The commands reference is [here](https://siril.readthedocs.io/en/stable/genindex.html).

# FITS header path parsing

In order to give more flexibility to scripting, a new path-parsing functionnality has been added, that can read FITS headers information and convert them to usable paths to write/locate files and folders. This allows to use master libraries or name stacking results with meaningful names, by adding filter or image name for instance. The syntax is detailed in this documentation [entry](https://siril.readthedocs.io/en/stable/Pathparsing.html)

# Livestacking

[Livestacking](https://siril.readthedocs.io/en/stable/Livestack.html) is the last improvement we wanted to introduce in this release note. It simply monitors a folder where new frames are stored during acquisition. Siril preprocesses these images as they arrive and stacks them live, hence the name. Bear in mind it is still experimental. We plan to improve it in the future, based on user feedback.

{{<figure src="../2023-02-24-siril-1.2.0-beta1/https://siril.readthedocs.io/en/stable/_images/ls_main_menu.png" caption="Start the livestacking session by hitting the red button.">}}

# Contribute to Siril

Of course, despite all our efforts, bugs may still exist. If you think you have found one, please reach out to us through the [forum](https://discuss.pixls.us/siril). Even better, do file a bug report [there](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) if you see it is not already listed as a [known bug](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).

We take the opportunity of this release to welcome Adrian Knagg-Baugh to the team of developers. Adrian joined initially to add the Generalized Hyperbolic Stretch that was present in v1.0. Since then, his contributions have been tremendous in the domain of Processing (all the stars tools, Denoising, Deconvolution...) and even the selfless job of hunting memory leaks!

We also welcome René de Hesselle, macOS maintainer of another great FOSS [Inkscape](https://inkscape.org/), who gave our builds on this OS a real boost. Siril is now delivered for both Intel and arm architectures. We thank him very much for taking this burden off our shoulders, as well as streamlining all our complex build pipelines.

We also thank the great team at [pixls.us](https://discuss.pixls.us), Pat, Mica and Darix, for all the help throughout the turmoil of administering our websites.

**Contributors** to this release are:

- **Cyril Richard**
- **Vincent Hourdin**
- **Cécile Melis**
- **Adrian Knagg-Baugh**
- **René de Hesselle**
- Fred DJN
- Alexander
- Zachary Wu
- Joan Vinyals Ylla Català
- Rafel Albert
- Gianluca Arcuri
- Udo Baumgart
- Yi Cao
- Mario Haustein
- Victor Boesen Gandløse
- Frank Soldano
- Pablo Cesar Campos
- Stefan
- Sébastien Rombauts
- kostas

# Donate

Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.
