---
title: Siril 1.2.5
author: Cyril Richard
date: 2024-11-22T02:00:00+00:00
categories:
  - News
tags:
  - new release
aliases:
  - /download/1.2.5/
version: "1.2.5"
linux_appimage: "https://free-astro.org/download/Siril-1.2.5-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.5-2-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.5-2_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.5-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.5-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.5.tar.bz2"
---

### Siril 1.2.5 Release Notes
We are happy to announce the release of Siril 1.2.5, which could be the last maintenance update in the 1.2 series before the major 1.4 release. This version focuses on fixing various issues and improving stability across different platforms, particularly on macOS. While being a maintenance release, it brings important stability improvements that will benefit all users, especially our macOS community. These fixes were made possible thanks to our active user community who took the time to report issues and provide detailed feedback. This continuous interaction helps us make Siril more reliable with each release.

**List of fixed bugs (since 1.2.4):**
* Fixed crash in gaussVertical() on macOS
* Fixed free space disk computation on macOS, probably for good (always stay positive!!!)
* Fixed crash if the working directory changes after a first successfull Automated light Curve process
* Fixed bug in TIFF files that have `TIFFTAG_MINSAMPLEVALUE` and `TIFFTAG_MAXSAMPLEVALUE` set to 0: we have removed these tags
* Added `HISTORY` after `seqcrop` and GUI crop
* Removed support for macOS High Sierra 10.13 and Mojave 10.14
* Better handling of Siril opening when double-clicking on an image, under macOS
* Fixed potential crash in synthstar

# Downloads
Siril 1.2.5 is available for download on the most common platforms (Windows, macOS, GNU/Linux). You can download it from the [download](../../../download) page.

If you want to build Siril from source, please check the [installation](https://siril.readthedocs.io/en/stable/installation/source.html) page.

# What's Coming with Siril 1.4?
The development of Siril 1.4 continues steadily. For those curious about the upcoming features, you can check our previous news article [about the RCE2024 presentation](../../2024/11/siril-at-rce-2024/) which includes a video in French, or watch the English presentation here: [Siril 1.4 Features Revealed: See What's Coming!](https://www.youtube.com/watch?v=tZPl78Cop1A&t=27s).

There are still some features to implement and polish, so as usual, it will be released when it's ready. Stay tuned for more updates!

# Contribute to Siril
Of course, Siril is a computer program developed by fallible humans, and bugs may still exist. If you think you've discovered one, please [contact](../../faq/#how-can-I-contact-siril-team-) or [write](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) a bug report if it isn't already present in the [list of known bugs](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). A [documentation page](https://siril.readthedocs.io/en/stable/Issues.html) has been written to help you report a problem.

# Donate
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development, you're welcome to [donate](../../donate) a small amount of your choice.
