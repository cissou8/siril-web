---
title: Siril 1.2.5
author: Cyril Richard
date: 2024-11-22T02:00:00+00:00
categories:
  - News
tags:
  - new release
aliases:
  - /download/1.2.5/
version: "1.2.5"
linux_appimage: "https://free-astro.org/download/Siril-1.2.5-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.5-2-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.5-2_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.5-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.5-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.5.tar.bz2"
---
### Notes de version de Siril 1.2.5
Nous sommes heureux d'annoncer la sortie de Siril 1.2.5, qui pourrait être la dernière mise à jour de maintenance de la série 1.2 avant la sortie majeure de la version 1.4. Cette version se concentre sur la correction de divers problèmes et l'amélioration de la stabilité sur différentes plateformes, particulièrement sur macOS. Bien qu'il s'agisse d'une version de maintenance, elle apporte des améliorations importantes en termes de stabilité qui profiteront à tous les utilisateurs, notamment sur macOS. Ces corrections ont été rendues possibles grâce à nos utilisateurs actifs qui ont pris le temps de signaler les problèmes et de fournir des retours détaillés. Cette interaction continue nous aide à rendre Siril plus fiable à chaque version.

**Liste des bugs corrigés (depuis 1.2.4) :**
* Correction d'un crash dans gaussVertical() sur macOS
* Correction du calcul de l'espace disque libre sur macOS, probablement définitivement (toujours être positif !!)
* Correction d'un crash si le répertoire de travail change après un premier processus réussi de courbe de lumière automatisée
* Correction d'un bug dans les fichiers TIFF qui avait `TIFFTAG_MINSAMPLEVALUE` et `TIFFTAG_MAXSAMPLEVALUE` mis à 0 : nous avons supprimé ces tags
* Ajout de l'historique (`HISTORY`) après `seqcrop` et le recadrage via l'interface graphique
* Suppression du support pour macOS High Sierra 10.13 et Mojave 10.14
* Meilleure gestion de l'ouverture de Siril lors d'un double-clic sur une image sous macOS
* Correction d'un crash potentiel dans synthstar

# Téléchargements
Siril 1.2.5 est disponible au téléchargement sur les plateformes les plus courantes (Windows, macOS, GNU/Linux). Vous pouvez le télécharger depuis la page de [téléchargement](../../../download).

Si vous souhaitez compiler Siril depuis les sources, veuillez consulter la page d'[installation](https://siril.readthedocs.io/en/stable/installation/source.html).

# Que nous réserve Siril 1.4 ?
Le développement de Siril 1.4 se poursuit régulièrement. Pour ceux qui sont curieux des fonctionnalités à venir, vous pouvez consulter notre précédent article [sur la présentation aux RCE2024](../../2024/11/siril-at-rce-2024/) qui comprend une vidéo en français, ou regarder la présentation en anglais ici : [Siril 1.4 Features Revealed: See What's Coming!](https://www.youtube.com/watch?v=tZPl78Cop1A&t=27s).

Il reste encore quelques fonctionnalités à implémenter et à peaufiner, donc comme d'habitude, la sortie se fera lorsque tout sera prêt. Restez à l'écoute pour plus de mises à jour !

# Contribuer à Siril
Bien sûr, Siril est un programme informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en avoir découvert un, veuillez nous [contacter](../../faq/#how-can-I-contact-siril-team-) ou [écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug s'il n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). Une [page de documentation](https://siril.readthedocs.io/en/stable/Issues.html) a été rédigée pour vous aider à signaler un problème.

# Faire un don
Développer des logiciels est amusant, mais cela prend aussi la quasi-totalité de notre temps libre. Si vous aimez Siril et souhaitez nous soutenir pour poursuivre son développement, vous pouvez [faire un don](../../donate) du montant de votre choix.
