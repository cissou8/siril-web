---
title: Siril 1.2.6
author: Cyril Richard
date: 2025-01-22T00:00:00+00:00
categories:
  - News
tags:
  - new release
aliases:
  - /download/1.2.6/
version: "1.2.6"
linux_appimage: "https://free-astro.org/download/Siril-1.2.6-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.6-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.6_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.6-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.6-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.6.tar.bz2"
---

### Siril 1.2.6 Release Notes
We are happy to announce the release of Siril 1.2.6. Although Siril 1.2.5 was supposed to be the last update in the 1.2 series, a critical bug in the Windows version required us to release a new patch. This bug was introduced after a recent update of `cfitsio` which caused issues with widechar filenames that were not UTF-8 encoded.

This update mainly addresses this problem, ensuring that Siril works seamlessly with non-UTF-8 filenames on Windows. We appreciate your patience and feedback in helping us resolve this issue.

**List of fixed bugs (since 1.2.5):**
* Fixed issue with widechar filenames on Windows after `cfitsio` update.
* Fixed sign error in Messier catalog
* Fixed Save As feature
* Changed configuration directory on macOS to org.siril.Siril

# Downloads
Siril 1.2.6 is available for download on the most common platforms (Windows, macOS, GNU/Linux). You can download it from the [download](../../../download) page.

If you want to build Siril from source, please check the [installation](https://siril.readthedocs.io/en/stable/installation/source.html) page.

# What's Coming with Siril 1.4?
The development of Siril 1.4 is progressing steadily. For those interested, the latest development version, 1.3.6, now includes the Python scripting engine. We encourage all developers to start writing scripts and providing feedback. Your input is valuable to help us refine this feature!

Stay tuned for more updates as we continue to work towards the major release of Siril 1.4. To find out more, check out this [post](../../2024/12/the-twelve-days-of-siril/).

# Contribute to Siril
Of course, Siril is a computer program developed by fallible humans, and bugs may still exist. If you think you've discovered one, please [contact](../../faq/#how-can-I-contact-siril-team-) or [write](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) a bug report if it isn't already present in the [list of known bugs](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). A [documentation page](https://siril.readthedocs.io/en/stable/Issues.html) has been written to help you report a problem.

# Donate
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development, you're welcome to [donate](../../donate) a small amount of your choice.

