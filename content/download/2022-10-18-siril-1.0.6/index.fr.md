---
title: Siril 1.0.6
author: Cyril Richard
date: 2022-10-18T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.0.6/
version: "1.0.6"
linux_appimage: "https://free-astro.org/download/Siril-1.0.6-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.6-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.0.6_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.0.6-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.0.6-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.6.tar.bz2"
---

Alors que la version 1.0.5 pouvait prétendre à être la dernière version disponible de la branche 1.0, il s'avère que quelques bugs assez embêtant nous ont été remonté et ont nécessité d'être traité prioritairement. Siril 1.0.6 est donc mis à disposition dans une optique de correction de bugs uniquement. En effet, le développement de la version 1.2.0 bat son plein et nous prend actuellement beaucoup de temps.

Nous profitons également de l'occasion de la sortie de cette version, le 18 octobre, pour souhaiter un excellent anniversaire à Cécile qui a les mains dans le code de la 1.2.0 depuis déjà plusieurs mois, afin de mettre à disposition une future version qui sera incroyable.

### Téléchargements
Siril 1.0.6 est distribué comme d'habitude pour les 3 plates-formes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://free-astro.org/index.php?title=Siril:install/fr).

### Quels sont les changements de cette version

- Lorsqu'un fichier SER vide ou malformé se trouvait dans le répertoire, sa prévisualisation provoquait un crash de siril.
- L'enregistrement d'une image en TIFF pouvait faire planter siril.
- L'extraction du gradient de fond polynomial pouvait faire planter siril lorsque le nombre d'échantillons était insuffisant.
- La commande `iif` de Pixel Math avait un problème quand on lui donnait des paramètres en argument. De même un bug sur les valeurs négatives a été corrigé.
- Recadrer une séquence avec des images de tailles différentes pouvait provoquer un crash si la zone sélectionnée n'était pas commune à toutes les images de la séquence.
- La commande seqstat pouvait se planter sur de grandes séquences lorsque certaines images n'étaient pas sélectionnées.
- Siril pouvait planter lorsqu'une grande étoile était proche du bord pendant la détection des étoiles.
- L'outil Asinh avait un mauvais comportement avec le point noir pour les images monochromes et 32bits.
- Quand les requêtes VisierR ne marchent pas pour trouver un objet dans les bases, Siril utilise SIMBAD. Il y'avait un bug dans ce dernier si l'objet comporte un + dans son nom.


Bien sûr, Siril est un logiciel informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en découvrir, merci de nous [contacter](../../faq/#comment-puis-je-contacter-léquipe-de-siril-) ou d'[écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug si ce dernier n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).


Les **contributeurs** de cette version sont : Cyril Richard, Cécile Melis, Vincent Hourdin, Adrian Knagg-Baugh et Fred Denjean.

### Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
