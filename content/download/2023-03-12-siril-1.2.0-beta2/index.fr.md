---
title: Siril 1.2.0-beta2
author: Cyril Richard
date: 2023-03-12T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.2.0-beta2/
version: "1.2.0-beta2"
linux_appimage: "https://free-astro.org/download/Siril-1.2.0-beta2-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.0-beta2-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.0-beta2_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.0-beta2-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.0-beta2-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.0-beta2.tar.bz2"
---

Nous sommes ravis de vous informer que Siril 1.2.0-beta2 est maintenant disponible, seulement quelques semaines après la sortie de la beta1. De nombreux bugs ont été corrigés depuis la précédente beta, ce qui rend cette nouvelle version plus stable et fiable.

Nous vous rappelons cependant que la version beta d'un logiciel est une première version du logiciel qui est encore en cours de test et de développement. Elle est mise à disposition pour recueillir des commentaires et effectuer des tests supplémentaires avant que la version finale ne soit diffusée au public. Les versions bêta peuvent comporter des bugs ou des fonctionnalités incomplètes, et sont destinées à des fins de test. Veuillez consulter régulièrement cette page pour la mise à jour et le status des éventuels bugs trouvés dans cette beta 2.

**Liste des bugs connus**

* StarNet ne fonctionne pas en séquence sur les fichiers SER. Cela sera corrigé dans la prochaine version
* Malgré un correctif déja fait, la barre de progression de l'outil PCC bloque avant la fin. Cependant il ne s'agit que d'un bug graphique, l'algorithme est bien éxecuté. Cela sera corrigé dans la prochaine version

**Liste des bugs corrigés (depuis la beta1)**

* Correction du comportement sous Windows lors de l'appel de programmes externes (StarNet, astrometry.net)
* Correction d'un crash lors de la conversion d'une séquence en SER
* Correction de la barre de progression PCC qui ne se terminait jamais
* Correction de l'AppImage Build
* Correction d'un crash lors de l'utilisation de la déconvolution sur une séquence
* Correction de l'utilisation mutltithread pour la déconvolution (Windows et MacOS)
* Correction de l'emplacement des certificats SSL sous Windows qui empêchait les fonctions liées à SearchObject d'aboutir.
* Correction de la lecture de la clé BAYERPAT si elle était définie sur NONE
* Correction du comportement de StarNet lorsque la compression TIFF est activée
* Correction d'un crash dans synthstar (resynthétisation complète et désaturation)
* Correction de la gestion des caractères larges dans les fichiers de configuration (Windows uniquement) qui provoquait un plantage au démarrage.
* Correction d'un crash lors de la sélection de points de données dans l'onglet Tracé
* Correction d'un crash lors de l'envoi d'une commande seqplatesolve ou seqstarnet sans autre argument.
* Correction d'un crash lors de l'enregistrement global et à deux passages, si une sélection était active et que la séquence avait des tailles d'image variables.
* Correction des calculs des bordures de cadrage min et max si la séquence a des tailles d'image variables.
* Correction de la perte des métadonnées lors de l'utilisation de StarNet
* Ajout d'un avertissement lors de la lecture de fichiers SER à une image
* Détection automatique de la prise en charge du threading de libfftw

**Nouvelles fonctionnalités**

* Ajout de la prise en charge des exécutables StarNet basés sur Torch
* Commandes GHS retravaillées

## Sommaire

{{< table_of_contents >}}

## Téléchargements
Siril 1.2.0-beta2 est distribué comme d'habitude pour les 3 plateformes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://free-astro.org/index.php?title=Siril:install/fr).

## Quels sont les changements majeurs de cette version depuis la version 1.0.6

Le nombre de changements apporté est très important, probablement plus important qu'entre les versions 0.9.12 et 1.0.0. Il serait bien trop long, et je pense que personne ne les lirait, d'écrire des notes de version détaillées de chaque nouvelle fonctionnalité. Nous avons donc décidé de nous arrêter sur les plus importantes, et si possible, de renvoyer vers les tutoriels ou notre toute nouvelle documentation.

La liste complète des changements/améliorations/corrections, le Changelog en anglais, se trouve [ici](https://gitlab.com/free-astro/siril/-/raw/1.2.0-beta1/ChangeLog).

## Fin de la distribution des scripts français
Nous avons fait le choix, qui peut paraître surprenant au premier abord, de ne plus distribuer la version française des scripts qui nécessitait la création de 4 dossiers : `brutes`, `offsets`, `darks` et `flats`. Ce choix s'explique par la volonté d'homogénéiser les scripts pour tous les utilisateurs, indépendamment des langues utilisées pour les logiciels. En effet, pourquoi privilégier le français et pas les autres langages ? Pour rappel, Siril est téléchargé et traduit par de nombreux pays. De plus, l'installation des scripts en français était une spécificité de l'installeur Windows et les versions macOS et GNU/Linux ne bénéficiaient pas de cette différence.

Nous avons donc décidé de ne plus distribuer les scripts en français et l'utilisateur est désormais invité à utiliser les noms de dossiers suivants : `lights`, `biases`, `darks` et `flats`. Bien entendu, si vous le souhaitez, il est toujours possible de modifier les scripts à votre guise pour utiliser des noms de dossiers que vous aurez vous-même choisis.

## Une nouvelle documentation
Une des grandes nouveautés qui accompagne cette sortie est la présence d'une nouvelle [documentation](https://siril.rtfd.io). Cette dernière a été écrite et pensée pour correspondre au mieux à l'application et pour évoluer en même temps. Quoi de plus désagréable que de lire une documentation qui n'est pas à jour. Veuillez cependant noter que cette nouvelle documentation n'est pas encore 100% finie et que les traductions sont en cours. Il faudra donc être un peu patient pour pouvoir la lire dans sa propre langue autre que l'anglais. Les traductions s'obtiennent en dépliant le volet en bas à gauche :

{{<figure src="../2023-02-24-siril-1.2.0-beta1/rtfd.langue.png" caption="Side panel allowing to change the language and to retrieve the pdf version.">}}

Nous tenons cependant a remercier les traducteurs qui ont commencé la lourde tâche de traduction de la documentation française :
- Jacky Legendre
- Frank Soldano 
- PaskEyes 

Nous avons également mis à jour et ajouté de nombreux nouveaux tutoriels. Vous pouvez les consulter [ici](https://siril.org/fr/tutorials/).

## Traitement des étoiles

Une autre nouveauté majeure dans cette version de Siril est l'introduction d'un ensemble d'outils spécifiques au post-traitement des images. 

Tout d'abord, l'accent a été mis sur l'amélioration de la détection d'étoiles. En effet, savoir détecter des étoiles dans tout type d'image est une tâche particulièrement difficile. Il faut savoir les différencier de tout autre objet stellaire et/ou artefact d'image afin de ne pas biaiser les analyses basées sur la forme et la taille des étoiles. L'algorithme de détection a donc été complètement revu et amélioré pour augmenter sa robustesse.

De plus, il est possible de modéliser les étoiles avec le profil Moffat qui est censé mieux s'adapter aux étoiles. Ce dernier n'est pas le profil par défaut (profile Gaussien), il faut donc le spécifier dans la fenêtre PSF dynamique pour que cela soit pris en compte.

{{<figure src="../2023-02-24-siril-1.2.0-beta1/dynamic-psf.fr.png" caption="Fenêtre de dialogue de la PSF Dynamique, où beaucoup d'options ont été rajoutées.">}}

Voici un aperçu des nouveaux outils:

1. Suppression des étoiles: StarNet++ est un logiciel qui permet de supprimer les étoiles d'une image. Cela s'avère trés utile pour différencier le traitement des nébulosités et des étoiles. Pour l'utilser, vous devez d'abord télécharger la version StarNetCLI sur votre ordinateur, puis indiquer à Siril où se situe l'exécutable téléchargé. La procédure est détaillée dans la [documentation](https://siril.readthedocs.io/fr/stable/processing/stars/starnet.html).
{{<figure src="../2023-02-24-siril-1.2.0-beta1/starnet.fr.png" caption="Fenêtre de dialog StarNet.">}}

1. Recomposition des étoiles: Après avoir traité séparément les étoiles et les autres objets (nébuleuses, galaxies, comètes ...), il faut recombiner les deux images. Cela peut être fait dans l'outil PixelMath, ou bien dans l'outil de recomposition des étoiles spécialement dédié à cette tâche. Plus d'explications sont à trouver dans la [documentation](https://siril.readthedocs.io/fr/stable/processing/stars/star-recomp.html).
{{<figure src="../2023-02-24-siril-1.2.0-beta1/star-recomp.fr.png" caption="Fenêtre de dialogue de la recomposition des étoiles.">}}

1. Désaturer les étoiles : Appliquer des traitements sur des étoiles saturées (comme la déconvolution par exemple) peut créer des artefacts non désirés. En général, on essaye de ne pas trop saturer les étoiles lors de la prise de vue, mais pour les plus brillantes cela n'est pas toujours possible. Cette nouvelle version de Siril offre la possibilité de désaturer simplement, en un clic, les étoiles. La documentation se trouve [ici](https://siril.readthedocs.io/fr/stable/processing/stars/unclipped.html).

1. Resynthèse intégrale : L'outil de Resynthèse intégrale a pour but d'aider à réparer les étoiles fortement déformées en utilisant les fonctions d'ajustement des étoiles de Siril. Il peut être utile pour sauver des images qui souffrent de coma ou d'autres distorsions. Si Siril peut détecter les étoiles, il peut les réparer. Attention cependant, ici on créé de toute pièce des étoiles artificielles. Il doit donc être vu comme l'outil de la dernière chance et il faut bien en être conscient. Plus d'explications sont à trouver dans la [documentation](https://siril.readthedocs.io/fr/stable/processing/stars/resynthesis.html).

## Nouvel outil de déconvolution

Un nouvel outil de déconvolution a été implémenté dans le but de remplacer l'ancien qui était un peu trop simpliste et ne donnait pas de bons résultats dans la plupart des cas. Cette nouvelle déconvolution est bien plus complète et est dédié aux images astronomiques. La fenêtre est divisée en deux parties.

1. La partie supérieure est la partie dédiée au calcul de la PSF (le noyau qui sera utilisé pour la déconvolution). Cette dernière peut être déterminée à l'aveugle, via les étoiles présentes dans l'image ou en utilisant des paramètres définis manuellement. Le choix est large et procure une grande puissance à cet outil.
1. La partie inférieure est dédiée à la déconvolution. Deux nouveaux algorithmes ont été implémentés en plus de l'algorithme Lucy-Richardson déjà présent dans les versions précédentes. Ils sont cependant orientés pour des images lunaire/planétaires.

L'outil peut paraître complexe et difficile à utiliser et c'est pour cela que nous vous conseillons de généralement d'abord travailler sur un crop de l'image avant d'utiliser l'image entière. Aussi, la [documentation](https://siril.readthedocs.io/fr/stable/processing/deconvolution.html) explique point par point tous les réglages et boutons présents dans l'interface.

{{<figure src="../2023-02-24-siril-1.2.0-beta1/deconv-dialog.fr.png" caption="Fenêtre de dialogue de la déconvolution.">}}

## Débruitage

Un des outils qui faisait le plus défaut à Siril, et souvent réclamé, est un outil de réduction de bruit. Cette version 1.2.0 rattrape ce manque et la réduction de bruit est entièrement décrite [ici](https://siril.readthedocs.io/fr/stable/processing/denoising.html). Elle est très simple à utiliser et marche très bien sur les images monochromes. Cependant, les images couleurs issues d'un dématricage peuvent connaitre des performances en deçà.

{{<figure src="../2023-02-24-siril-1.2.0-beta1/denoise-dialog.fr.png" caption="Fenêtre de dialogue de la réduction de bruit.">}}

## Refonte de l'outil Étirement Hyperbolique Généralisé

Alors que Siril 1.0 était déjà lancé, nous avions intégré un nouvel outil originellement développé par une [équipe d'astrophotographes amateurs](https://ghsastro.co.uk/) et existant en tant que plugin dans PixInsight : l'étirement Hyperbolique Généralisé. Ce dernier permet de traiter des images avec des niveaux de bruit élevés en étirant les données tout en préservant les détails fins. L'outil offre une plus grande flexibilité par rapport à la transformation de l'histogramme classique en contenant le gonflement des étoiles. Dans cette version, nous avons complètement refait cette fonctionnalité en la rendant plus simple à utiliser à l'aide de la présence d'un histogramme dans la fenêtre. Afin d'utiliser au mieux tous les paramètres disponibles, les auteurs originaux de l'outil ont écrit un [tutoriel sur le site de Siril](../../tutorials/ghs), et une [page de documentation](https://siril.readthedocs.io/fr/stable/processing/stretching.html#generalised-hyperbolic-stretch-transformations-ghs) détaille également cette fonctionnalité.

{{<figure src="../2023-02-24-siril-1.2.0-beta1/ght-dialog.fr.png" width="100%" caption="Fenêtre de dialogue de la réduction de l'Étirement Hyperbolique Généralisé.">}}

## Refonte de l'astrométrie et de PCC

L'astrométrie a, encore une fois, été fortement améliorée. En effet, elle est au coeur de nombreux processus et de son succès dépendent de nombreux autres outils. Tout d'abord, l'amélioration de la détection d'étoiles, expliquée plus haut, a par effet de ricochet, grandement amélioré l'analyse astrométrique. De plus, pour pouvoir résoudre les images hors-ligne, il est maintenant possible d'utiliser des catalogues locaux. Enfin, pour les très grand champs, dans le cas ou Siril a encore du mal a résoudre l'image, il est maintenant possible d'utiliser une installation locale d'astrometry.net. Ces nouvelles fonctionnalités sont expliquées [ici.](https://siril.readthedocs.io/fr/stable/astrometry/platesolving.html)

Très fortement lié à l'astrométrie, l'outil d'étalonnage des couleurs par photométrie, couramment appelé PCC, a aussi été amélioré. En effet, nous avons totalement découplé la partie astrométrie et photométrie. Ce changement permet maintenant de pouvoir éxécuter PCC sur une image préalablement résolue en astrométrie via un autre logiciel (service web astrometry.net, ASTAP, ....). C'est donc un profond changement idéal pour les images très grand champ que Siril n'arrive pas à résoudre. Encore une fois, plus de détails [ici.](https://siril.readthedocs.io/fr/stable/processing/colors.html#photometric-color-calibration) 

## Amélioration de PixelMath

L'outil PixelMath a également été largement amélioré. Tout d'abord il est maintenant possible d'enregistrer des formules couramment utilisées afin de pouvoir les rééutiliser à volonté. Ceci est très pratique quand on utilise toujours les mêmes formules pour sa composition. Pour ce faire, il suffit de cliquer sur le petit bouton à droite de la formule, et cette dernière sera enregistrée dans la partie **Préréglages** en bas de la fenêtre (n'oubliez pas de l'étendre pour voir son contenu). Il est également possible de forcer la mise à l'echelle de l'image en sortie enfin d'éviter que les valeurs des pixels ne dépassent la gamme que l'on souhaite, en général [0, 1].

Enfin, l'amélioration la plus importante est probablement l'ajout de la prise en charge des fonctions de statistiques d'image dans les formules. Vous pouvez consulter la [documentation](https://siril.readthedocs.io/fr/stable/processing/pixelmath.html) pour avoir la liste complète des fonctions utilisables ainsi qu'une explication détaillée de cet outil.

{{<figure src="../2023-02-24-siril-1.2.0-beta1/pixelmath-dialog.fr.png" caption="Fenêtre du PixelMath.">}}

## Nouvel algorithme d'alignement en 2 passes

Le problème avec l'alignement global actuel, c'est que l'image de référence choisie est en général la première image. Ce comportement peut bien sûr être modifié en choisissant avec précaution une image de référence, mais ceci n'est pas possible, en automatique avec les scripts. C'est pour cela que nous avons intégré une nouvelle méthode d'alignement qui se passe en deux fois.

1. Dans un premier temps, on exécute l'alignement global en 2 passes. Il va enregistrer les informations d'alignement dans le fichier seq et choisir la meilleure image.
{{<figure src="../2023-02-24-siril-1.2.0-beta1/2pass.fr.png" caption="Alignement global en 2 passes.">}}
2. Ensuite il suffit d'appliquer l'alignement calculé avec la méthode dédiée. On peut également choisir d'appliquer des filtres pour que seules les images de bonne qualité (rondeur, FWHM etc...) ne soient exportées. On peut egalement choisir de recadrer la séquence de différentes facons (min, max, cog) en plus de la méthode conventionnelle.
{{<figure src="../2023-02-24-siril-1.2.0-beta1/applyreg.fr.png" caption="Appliquer l'alignement précédemment calculé et enregistré dans le fichier seq.">}}

Plus d'informations peuvent être trouvées [ici.](https://siril.readthedocs.io/fr/stable/preprocessing/registration.html#pass-registration)

## Ajout de nombreuses commandes

De nombreuses commandes ont été ajoutées dans Siril afin de permettre le plus possible de faire le traitement de l'image en ligne de commande, et donc, également via un script. Il est donc maitenant possible d'obtenir une image de A à Z en utilisant un script. Cependant il faut garder à l'esprit qu'il est toujours mieux de controler manuellement la partie traitement et que l'on obtient généralement de meilleurs résultats.

La liste des commandes est accessible via la documentation, à cette [page.](https://siril.readthedocs.io/fr/stable/genindex.html)

## Interprétation d'entêtes

Afin de permettre une plus grande flexibilité dans l'écriture de scripts, une nouvelle fonctionnalité a été ajoutée pour interpréter les informations d'entêtes contenues dans les fichiers FITS et de les transformer en chaines de caracteres utilisables pour nommer/retrouver des fichiers. Cela permet notamment d'utiliser des bibliotheques d'images maitres ou de nommer le resultat d'un empilement, en ajoutant par exemple, le nom du filtre utilisé ou de l'objet imagé. La syntaxe a utiliser est décrite [ici](https://siril.readthedocs.io/fr/stable/Pathparsing.html).

## Livestacking

Le [livestacking](https://siril.readthedocs.io/fr/stable/Livestack.html), ou empilement en direct, est une toute nouvelle fonctionnalité implémentée dans Siril. Elle est encore expérimentale et pourrait ne pas fonctionner correctement dans certains cas. Elle sera améliorée au fur et à mesure des retours utilisateur. Cette fonctionnalité, comme son nom l'indique, permet de définir un répertoire de travail dans lequel vont arriver des images, les unes après les autres, lors de la séance astrophoto. Siril procède à l'empilement en temps réel et peut même utiliser des Darks/Flats qui ont préalablement été réalisés et empilés.

{{<figure src="../2023-02-24-siril-1.2.0-beta1/https://siril.readthedocs.io/fr/stable/_images/ls_main_menu.png" caption="Le bouton encadré permet de démarrer la session de livestacking.">}}

## Contribuer à Siril
Bien sûr, Siril est un logiciel informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en découvrir, merci de nous [contacter](../../faq/#comment-puis-je-contacter-léquipe-de-siril-) ou d'[écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug si ce dernier n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).

Nous profitons de cette version pour souhaiter la bienvenue à Adrian Knagg-Baugh dans l'équipe de développeurs. Adrian nous a rejoint initialement pour ajouter l'étirement hyperbolique généralisé qui était présent dans la v1.0. Depuis, ses contributions ont été énormes dans le domaine du traitement (tous les outils d'étoiles, débruitage, déconvolution...) et même le travail peu passionant de chasse aux fuites de mémoire !

Nous souhaitons également la bienvenue à René de Hesselle, mainteneur MacOS d'un autre grand logiciel libre [Inkscape](https://inkscape.org/), qui a donné un véritable coup de pouce à nos constructions sur cet OS. Siril est maintenant livré pour les architectures Intel et Arm. Nous le remercions vivement de nous avoir soulagé de ce fardeau, ainsi que d'avoir rationalisé tous nos pipelines de compilation complexes.

Nous remercions enfin la formidable équipe de [pixls.us](https://discuss.pixls.us), Pat, Mica et Darix, pour toute l'aide qu'ils nous ont apportée dans la tourmente de l'administration de nos sites web.



Les **contributeurs** de cette version sont : 

- **Cyril Richard**
- **Vincent Hourdin**
- **Cécile Melis**
- **Adrian Knagg-Baugh**
- **René de Hesselle**
- Fred DJN
- Udo Baumgart
- Isaac Rogers
- Ashutosh Vaidya
- Chris Kuethe
- Frédéric Trouche
- Joan Vinyals Ylla-Catala
- Mario Haustein
- hamarituc
- luz paz
- Gianluca Arcuri
- Alexander
- Robert MORELLI
- Sébastien Rombauts
- Zachary Wu
- Frank Soldano
- Joaquin Manuel Llano Montero
- Martin Mancuska
- Martin Schoenmaker
- inkongru

## Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
