---
title: Siril 1.2.2
author: Cyril Richard
date: 2024-06-14T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.2.2/
version: "1.2.2"
linux_appimage: "https://free-astro.org/download/Siril-1.2.2-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.2-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.2_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.2-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.2-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.2.tar.bz2"
---

Nous sommes ravis de vous informer que Siril 1.2.2 est maintenant disponible. De nombreux bugs ont été corrigés par rapport à la version 1.2.1 sortie l'année dernière, et nous espérons que cette version 1.2.2 sera le plus stable possible.

**Liste des bugs corrigés (depuis la 1.2.1)**

* Suppression des échantillons de la supression de gradient après leur utilisation dans un script (#1265)
* Correction d'un problème d'analyseur de catalogue avec une déclinaison négative (moins de 1°) (#1270)
* Correction de la pondération par le nombre d'étoiles lors de l'empilement si le nombre d'étoiles est le même dans toute la séquence (#1273)
* Amélioration du contrôle du panoramique et du zoom de la souris pour permettre une utilisation d'une seule main (!638, corrige #1271)
* Ajout d'une option à la commande LINK afin de trier les résultats par date (#1115)
* Correction de la taille des pixels définie par l'astrométrie en utilisant la préférence binning_update (#1254)
* Correction d'un crash lors de l'interrogation des statistiques sur une image CFA avec une sélection plus petite qu'un carré 2x2 (#1286)
* Correction d'un crash lors de l'enregistrement d'images compressées et recadrées (#1287)
* Désactivation de l'utilisation surnuméraire d'openmp dans le dématriçage, qui pouvait conduire à un crash (#1288)
* Correction de l'erreur d'orientation du ser (#1258, #1261)
* Correction d'un crash lors de l'empilement avec rejet en cas d'utilisation d'une séquence alignée par translation (#1294)
* Correction du défilement trop rapide de la molette de la souris (#1151)
* Correction du glisser-déposer dans l'affichage des images sur macOS (#1310)
* Correction d'un bug dans le filtre rgradient (Larson Sekanina) (#1313)
* Correction d'un bug dans les étirements hyperboliques généralisés (#1314)
* Correction d'une erreur d'analyse de chemin avec savetif (#1318)
* Ajout de la gestion des lectures de pipe de commande vides (#1277)

# Sommaire

{{< table_of_contents >}}

# Téléchargements
Siril 1.2.2 est distribué comme d'habitude pour les 3 plateformes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://siril.readthedocs.io/fr/stable/installation/source.html).

# Une version de stabilisation

Encore une fois, cette version est une version de stabilisation de la série 1.2. Il n'y a donc aucune nouveauté, mais Siril devient plus stable grâce à tous les retours de la communauté. C'est pour cela qu'il est très important de nous remonter les bugs si vous en trouvez, c'est généralement le seul moyen de les voir corrigés dans la version suivante.

Nous vous remercions pour votre soutien continu et vos précieux retours. Téléchargez Siril 1.2.2 dès maintenant et profitez d'une version plus stable et fiable !

# Quoi de neuf avec Siril 1.4?

La future version 1.4 avance à pas de géant. Nous sommes encore dans le processus d'introduire de nouvelles fonctionnalités, mais nous devrions bientôt entrer dans une phase de stabilisation. Cette version représente une avancée majeure, presque une rupture avec les versions précédentes : un soin particulier a été apporté à l'ergonomie. De plus, le nombre de nouvelles fonctionnalités est très important et nous avons hâte de relâcher une version bêta pour que vous puissiez les découvrir. Restez à l'écoute, on ne sait jamais !


# Contribuer à Siril
Bien sûr, Siril est un logiciel informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en découvrir, merci de nous [contacter](../../faq/#comment-puis-je-contacter-léquipe-de-siril-) ou d'[écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug si ce dernier n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). Une [page de la documentation](https://siril.readthedocs.io/fr/stable/Issues.html) a été écrite dans le but de vous aider a reporter un problème.


# Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
