---
title: Siril 1.2.2
author: Cyril Richard
date: 2024-06-14T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /download/1.2.2/
version: "1.2.2"
linux_appimage: "https://free-astro.org/download/Siril-1.2.2-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.2-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.2_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.2-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.2-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.2.tar.bz2"
---

We're delighted to announce that Siril 1.2.2 is now available. Many bugs have been fixed compared to version 1.2.1 released last year, and we hope that version 1.2.2 will be as stable as possible.

**List of fixed bugs (since 1.2.1)**

* Removed background extraction samples after using it in script (#1265)
* Fixed catalog parser problem with negative declination (less than 1°) (#1270)
* Fixed weighting by number of stars during stacking if number of stars is the same accross the sequence (#1273)
* Improved mouse pan and zoom control to enable one-handed operation (!638, fixes #1271)
* Added an option to the LINK command in order to sort output by date (#1115)
* Fixed pixel size set by astrometry using binning_update preference (#1254)
* Fixed crash when querying stats on a CFA image with a selection smaller than a 2x2 square (#1286)
* Fixed crash when saving compressed and croped images (#1287)
* Disabled supernumerary use of openmp in demosaicing, which could lead to a crash (#1288)
* Fixed ser orientation error (#1258, #1261)
* Fixed crash during rejection stacking when using shift-registered sequence (#1294)
* Fixed mouse scrollwheel scrolling too fast (#1151)
* Fixed drag & drop in image display on macOS (#1310)
* Fixed bug in rgradient (Larson Sekanina) filter (#1313)
* Fixed bug in generalized hyperbolic stretches (#1314)
* Fixed path parsing error with savetif (#1318)
* Added handling of empty command pipe reads (closes #1277)

# Downloads

Siril 1.2.2 is distributed as usual for the three most common platforms (Windows, macOS, GNU/Linux). See the [download](../../../download) page.

But of course, since Siril is free software, it can be built on any OS from the sources, see the [installation](https://siril.readthedocs.io/stable/installation/source.html) page.

# A Stabilization Version

Once again, this version is a stabilization release of the 1.2 series. Therefore, there are no new features, but Siril becomes more stable thanks to all the community feedback. This is why it is very important to report any bugs you find, as it is generally the only way to have them fixed in the next version.

We thank you for your continued support and valuable feedback. Download Siril 1.2.2 now and enjoy a more stable and reliable version!

# What's New with Siril 1.4?

The upcoming version 1.4 is making great strides. We are still in the process of introducing new features but should be approaching a stabilization phase quite soon. It represents a major advancement, almost a break with previous versions: a lot of care has been put into ergonomics. Also, the number of new features is becoming very significant and we can't wait to release a beta version. Stay tuned, you never know!

# Contribute to Siril
Of course, Siril is a computer program developed by fallible humans, and bugs may still exist. If you think you've discovered one, please [contact](../../faq/#how-can-I-contact-siril-team-) or [write](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) a bug report if it isn't already present in the [list of known bugs](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). A [documentation page](https://siril.readthedocs.io/en/stable/Issues.html) has been written to help you report a problem.


# Donate
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.
