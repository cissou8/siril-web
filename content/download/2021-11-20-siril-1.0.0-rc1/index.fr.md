---
title: Siril 1.0.0-rc1
author: Cyril Richard
date: 2021-11-20T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
version: "1.0.0-rc1"
linux_appimage: "https://free-astro.org/download/Siril-1.0.0-rc1-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.0-rc1-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.0-rc1-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.0-rc1.tar.bz2"
---

Ceci est la première [release candidate](https://fr.wikipedia.org/wiki/Version_d%27un_logiciel#Version_admissible_ou_pre-release_ou_Release_Candidate) de la version 1.0.0. Elle est livrée avec de nombreuses améliorations et corrections de bugs par rapport à la version bêta précédente (0.99.10.1) et présente toutes les fonctionnalités de la 1.0.0. À partir de maintenant, nous nous concentrerons sur les corrections de bugs afin de sortir, dans les plus brefs délais, une version 1.0.0 aussi aboutie que possible.

Cette version marque également l'arrivée dans l'équipe de développement de Cécile (alias Cissou8) qui est l'auteure d'un grand nombre d'améliorations et qui travaille sur Windows ! Merci aussi à Florian Bénédetti pour son énorme travail sur l'intégration continue.

Nous tenons enfin à souligner l'excellent travail réalisé par les bêta-testeurs. Cela nous a permis de faire cette superbe version avec tellement de corrections de bugs : Thierry Legault, Colmic, Batbihirulau et Stéphiou. Merci!

### Téléchargements
Siril 1.0.0-rc1 est distribué pour les 3 plates-formes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://free-astro.org/index.php?title=Siril:install/fr).

## Quoi de neuf dans cette version ?

### L'astrométrie au cœur de cette version
L'astrométrie, c'est la mesure de la position des astres. En astrophotographie, cette fonctionnalité est essentiellement utilisée pour connaître la position de l'image sur la voûte céleste. Grâce à cela, Siril est depuis plusieurs versions capable de récupérer la colorimétrie d'une image en effectuant la photométrie sur chaque étoile dont l'astrométrie l'a associée à un catalogue. Cependant, une des faiblesses de Siril concernait les images grand champ, dont certaines contiennent un niveau de distorsion empêchant toute résolution astrométrique simple.

#### L'astrométrie des images grand champ
Cette nouvelle version permet de tenter une résolution astrométrique sur une partie seulement de l'image. Pour ce faire, il suffit de tracer une sélection autour d'un objet connu de l'image, de le placer au centre et de lancer l'astrométrie. La résolution est en général très rapide.

{{<figure src="wide_field.fr.png" link="wide_field.fr.png" caption="Il est maintenant possible d'effectuer l'astrométrie sur une partie de l'image en faisant une sélection autour d'un objet connu, ici M8, la nébuleuse de la Lagune." width="100%" >}}

Toujours pour les images grand champ (FOV > 5°), la résolution astrométrique se fait maintenant par défaut sur la partie centrale de l’image uniquement. Cela permet de gagner en rapidité et d’éviter de résoudre sur une partie de l’image ou les distorsions sont trop importantes et feraient échouer le processus. 

#### Conservation des infos WCS dans les opérations géométriques
Lorsqu'une résolution astrométrique est effectuée sur une image, les données sont conservées dans l'entête du fichier FITS dans un format respectant le standard World Coordinates System (WCS). Ce standard est très pratique et permet à tous les logiciels l'utilisant, de connaître la solution astrométrique de l'image. Néanmoins, lorsque l'on fait une rotation, une transformation miroir ou un recadrage, ces données doivent être mise à jour en conséquence. Les versions précédentes de Siril se contentaient alors de prévenir l'utilisateur qu'elles seraient irrémédiablement perdues. Dorénavant, elles sont mises à jour, et l'utilisateur peu recadrer une image sans crainte de perdre toutes les informations astrométriques qui pourraient s’avérer utile par la suite.

#### Conservation des infos WCS dans les opérations Annuler/Rétablir
De même, lors des opérations Annuler/Rétablir, habituellement effectuées lorsque l'on n'est pas trop content du traitement appliqué, les coordonnées WCS sont traitées comme des métadonnées de l'image. Elles peuvent donc être annulées et rétablies à volonté.

#### Une meilleure annotation
L'annotation des images a été améliorée avec l'intégration de la grille céleste, de ses coordonnées RA/Dec et d'une boussole. Tout ceci est accessible via un nouveau bouton dans la barre d’outils. 
{{<figure src="wcs-grid.png">}}
La boussole peut également être positionnée à différents endroits de l'image, et ceci est paramétrable dans les préférences de Siril (ctrl + p). Les couleurs choisies pour l'affichage de la grille correspondent aux couleurs utilisées dans le logiciel d'autoguidage [PHD2](https://openphdguiding.org/).

{{<figure src="astrometry_pref.fr.png" link="astrometry_pref.fr.png" caption="Le menu astrométrie des préférences a été étoffé. Il contient, entre autres, une liste déroulante pour la position de la boussole." width="100%" >}}
{{<figure src="astrometry_grid.png" link="astrometry_grid.png" caption="Une grille est dorénavant affichable en plus de l'annotation standard. Il est également possible d'ajouter une boussole dans un des 4 coins de l'image ou au centre." width="100%" >}}

#### Intégration du catalogue GAIA EDR3
Dans Siril, l'astrométrie se fait à partir de catalogues qui sont consultés en ligne. Le tout nouveau catalogue astrométrique [GAIA EDR3](https://www.cosmos.esa.int/web/gaia/earlydr3) a été intégré dans les requetes de recherche de cette nouvelle version et se retrouve donc au côté de l'ancienne version : GAIA DR2.

#### Catalogue utilisateur
Depuis la version 0.99.8, il est possible d'annoter manuellement des objets présents dans l'image s'ils ne se trouvent pas dans un catalogue géré par Siril. Cette nouvelle version permet d'enregistrer ces annotations manuelles dans un catalogue utilisateur afin de pouvoir les réutiliser par la suite. L'utilisateur est libre de l'afficher en le sélectionnant ou non dans les préférences (ctrl + p) et il est également possible de le purger au même endroit.

#### Le choix du snapshot
Les versions antérieures proposaient également une fonctionnalité d'instantanés afin de partager une capture de l'image en l'état. L'image est alors enregistrée dans le répertoire de travail avec un nom unique contenant l'horodatage de la capture. La version 1.0.0 donne maintenant le choix à l'utilisateur de copier l'instantané dans le presse-papier. Ainsi, un "coller" (ctrl + v) suffit à afficher l'image dans les forums ou différents réseaux sociaux.
{{<figure src="snapshot.fr.png" caption="Il est possible de choisir le type d'instantané que l'on veut : un fichier sur le disque, ou en mémoire dans le presse-papier." width="50%" >}}

### L'alignement des images
#### Amélioration de la détection d'étoiles
L’algorithme de détection d’étoiles, qui sert aussi bien à l’alignement global qu’à la résolution par l’astrométrie, a été modifié pour gagner en performance et en fiabilité. Il détecte moins de faux positifs qui pouvaient fausser les alignements et estime les profils des étoiles pour résoudre la PSF sur une boite de taille adaptée à chacune.  Comparé à la version précédente, il devrait y avoir moins souvent besoin d’adapter les paramètres de PSF dynamique pour que Siril aligne les images.
{{<figure src="stars-comparison.png" link="stars-comparison.png" caption="Comparaison de la détection d'étoile entre la vesion 0.99.10.1 à gauche et la 1.0.0-rc1 à droite." width="100%" >}}

Un effet collatéral de cette amélioration, et non des moindres, est la réduction du temps de traitement de l'alignement global. Ce gain de performance est par ailleurs assez spectaculaire pour les images monochromes comme on peut le voir dans les illustrations suivantes.
{{<figure src="PerfRC1color.png" caption="Compte rendu de performance pour une séquence d'images couleurs. La comparaison est faite en 16 et 32bits pour les versions 0.9.12, 0.99.10.1 et 1.0.0-rc1 sur les 3 types de fichier (SER, FITS, FITSEQ). (La version 0.9.12 ne permettant pas de traiter des images en 32bits, seules les performances 16bits sont reportées. De plus elle ne permettait pas non plus le travail en FITSEQ)." width="100%" >}}
{{<figure src="PerfRC1mono.png" caption="Compte rendu de performance pour une séquence d'images monochromes. La comparaison est faite en 16 et 32bits pour les versions 0.9.12, 0.99.10.1 et 1.0.0-rc1 sur les 3 types de fichier (SER, FITS, FITSEQ). (La version 0.9.12 ne permettant pas de traiter des images en 32bits, seules les performances 16bits sont reportées. De plus elle ne permettait pas non plus le travail en FITSEQ)." width="100%" >}}

#### Choix du nombre de degrés de liberté
Pour l’alignement global, il est désormais possible de choisir le nombre de degrés de liberté de la transformation qui envoie l’image à aligner sur l’image de référence. Par défaut, la transformation est une [homographie](https://en.wikipedia.org/wiki/Homography_(computer_vision)), qui admet un certain nombre de distorsions entre les 2 images. C’est un modèle "souple" qui s’adapte très bien au cas général.

Mais pour des séquences d’images dans lesquelles on s’attend à peu ou pas de distorsions, cette projection a trop de degrés de liberté et l’on peut souhaiter tester des projections plus rigides, qui entraîneront moins d’interpolations. Dans ce but, il est désormais possible de choisir une [translation](https://fr.wikipedia.org/wiki/Translation) (1 translation selon chaque axe), une [similitude](https://fr.wikipedia.org/wiki/Similitude_(g%C3%A9om%C3%A9trie)) (2 translations, 1 rotation et 1 échelle) ou une [application affine](https://fr.wikipedia.org/wiki/Application_affine).

{{<figure src="Transformations.png" caption="Selon le type choisi, la transformation qui envoie l'image à aligner sur l'image de référence est calculée sur 2, 4, 6 ou 8 degrés de liberté. Plus le nombre de degrés est élevé, plus la transformation admet de déformations entre les 2 images." width="100%" >}}

### De nouveaux outils d'analyse
#### La mesure du tilt du capteur
Un nouvel outil intègre la panoplie, déjà conséquente, des outils d'analyse de Siril. Il permet d'analyser l'image et d'indiquer à l'utilisateur la présence ou non de tilt dans la prise de vue. Ce dernier est représenté par un quadrilatère avec à chaque sommet, la valeur de la FWHM utilisée pour le calcul. L'image est divisée en 4 parties égales dans lesquelles sont calculées les FWHM. Puis la FWHM de la zone centrale est également calculée. Un capteur avec un tilt nul serait représenté par un carré avec toutes les valeurs FWHM identiques.
Il y a deux façons d'utiliser cette nouvelle fonctionnalité :
1. soit par la fenêtre de PSF dynamique (ctrl + F6), en cliquant sur le bouton comportant un quadrilatère. {{<figure src="tilt_button.png">}}
1. soit en tapant <code>tilt</code> dans la ligne de commande (<code>tilt clear</code> efface le dessin).

{{<figure src="tilt.png" link="tilt.png" caption="Résultat de la fonction tilt. Le quadrilatère représente le tilt du capteur." width="100%" >}}

<code>Stars: 347, Truncated mean[FWHM]: 3.61, Sensor tilt[FWHM]: 0.57 (16%), Off-axis aberration[FWHM]: -0.30</code>

Dans la console sont indiqués:
- le nombre d'étoiles utilisées pour la mesure
- la FWHM moyenne sur le capteur, débarrassée des valeurs aberrantes
- le tilt, exprimé comme la différence entre la meilleure et la moins bonne FWHM sur les 4 coins de l'image avec entre parenthèse le pourcentage de déviation du tilt (une valeur supérieure à 10% indique un problème de tilt)
- l'aberration, exprimée par la différence de FWHM entre les étoiles au centre et les étoiles sur les bords du capteur

Si le nombre d'étoiles détectées est faible (<200), les paramètres de détection de la PSF dynamique permettent d’améliorer en réglant le seuil/le rayon. En effet, plus le nombre d'étoiles utilisées dans le calcul est important et plus le résultat de l'analyse est fiable.

Attention : pour que le résultat ait du sens, il est préférable d’exécuter cette commande sur une image unitaire et non un résultat d'empilement. Une image pré-traitée (et tout juste dématricée pour les capteurs couleurs) est donc idéale. De plus, le quadrilatère dessiné a ses proportions exagérées, et ce, afin d’être plus visible à l'écran. Ça ne saurait correspondre exactement à la réalité.

#### Un bouton pour de la photométrie rapide
Dans la barre d’outils a été ajouté un bouton pour effectuer une photométrie rapide des étoiles. 
{{<figure src="photometry_button.png">}}
Il suffit de sélectionner ce bouton pour changer le mode de sélection de l'image, puis de cliquer sur une étoile. Le résultat de la photométrie apparaît instantanément. Un nouvel estimateur du rapport signal sur bruit a été ajouté dans les résultats de la PSF/Photométrie. Sa valeur est calculée en [dB](https://fr.wikipedia.org/wiki/D%C3%A9cibel), mais pour plus de compréhension, elle est associée à 6 niveaux de notation :
1. Excellent (RSB > 40.0dB)
1. Bon (RSB > 25.0dB)
1. Satisfaisant (RSB > 15.0dB)
1. Faible (RSB > 10.0dB)
1. Mauvais (RSB > 0.0dB)
1. N/A

Cette dernière notation n'intervient que si le calcul a échoué, pour une raison ou pour une autre.

{{<figure src="photometry_result.fr.png" link="photometry_result.fr.png" caption="Fenêtre du résultat de la photométrie." width="100%" >}}

Si la magnitude d'une étoile du champ est renseignée avec la commande `setmag`, le résultat indique cette fois la magnitude réelle de chaque étoile pointée.

{{<figure src="photometry_setmag.fr.png" link="photometry_setmag.fr.png" caption="Fenêtre du résultat de la photométrie avec la magnitude réelle." width="100%" >}}

#### Encore plus de graphiques et de tri

Pour vous aider à trier vos séquences alignées, de nouvelles fonctionnalités ont été ajoutées dans l'onglet `Graphique`. Une fois terminé un alignement de séquence (ou au chargement d'une séquence alignée), un menu déroulant vous permet de choisir plusieurs paramètres d'intérêt pour les tracer sous forme de graphiques ou pour trier vos données.

{{<figure src="Plot1.fr.png" link="Plot1.fr.png" caption="Sélectionnez un des paramètres en Y dans le menu pour en afficher les valeurs dans le graphique. La courbe violette affiche les valeurs pour chaque image dans l'ordre de la séquence. La courbe verte montre les valeurs triées par ordre décroissant de qualité. Le cercle indique la valeur de l'image de référence tandis que la croix indique la valeur de l'image ouverte dans la visualisation." width="100%" >}}

Vous pouvez également tracer un paramètre en fonction d'un autre.

{{<figure src="Plot2.fr.png" link="Plot2.fr.png" caption="Les valeurs de rondeur en fonction de la FWHM sont tracées en nuage de points. Passez la souris sur un des points pour en afficher les valeurs en X et Y ainsi que pour identifier l'image correspondante." width="100%" >}}

Cliquez sur l'un des points vous offre la possibilité d'exclure ou d'ouvrir l'image correspondante. Le paramètre choisi en Y est également reflété dans l'inspecteur de séquence, qui peut être utilisé pour trier, afficher et sélectionner/rejeter des images de la séquence.

{{<figure src="Plot3.fr.png" link="Plot3.fr.png" caption="Le paramètre sélectionné en Y est également repris dans la dernière colonne de l'inspecteur de séquence, qui permet ensuite de trier, revoir et sélectionner/exclure des images de la séquence. Pour ajouter ou retirer des images, sélectionnez les lignes correspondantes et appuyez sur la barre d'Espace." width="100%" >}}

Pour compléter les possibilités de tri et filtrage, effectuez une PSF de séquence sur une étoile.

{{<figure src="Plot4.fr.png" link="Plot4.fr.png" caption="Ce graphique montre la valeur de fond de ciel mesurée par photométrie pour chaque image. Vous pouvez maintenant facilement identifier les passages nuageux en repérant les valeurs anormalement hautes de fond de ciel." width="100%" >}}

### Pixel Math : implémentation d'une première version
Pas du tout prévu pour cette mouture, un outil de "Pixel Math" a été développé et intégré dans cette release candidate. La puissance de cet outil réside dans la capacité à interpréter des formules mathématiques pour appliquer une transformation aux images, déclarées en tant que variables.

{{<figure src="Pixel_Math.fr.png" link="Pixel_Math.fr.png" caption="Fenêtre du Pixel Math avec la zone de saisie des formules et celles prévues à la déclaration des variables et fonctions." width="100%" >}}

Puisqu'un exemple vaut bien plus que des mots, voici une petite vidéo présentant le Pixel Math intégré à Siril.

{{<youtube id="WIhaUGF28Ck" title="Un exemple d'utilisation de l'outil." >}}
Les fonctions mathématiques suivantes sont prises en charge :

addition (<code>+</code>), soustraction/negation (<code>-</code>), multiplication (<code>*</code>), division (<code>/</code>), puissance (<code>^</code>) module (<code>%</code>), inversion de pixel (<code>~</code>), <code>abs</code>, <code>acos</code>, <code>asin</code>, <code>atan</code>, <code>atan2</code>, <code>ceil</code>, <code>cos</code>, <code>cosh</code>, <code>exp</code>, <code>floor</code>, <code>ln</code>, <code>log</code> (ou <code>log10</code>), <code>log2</code>, <code>pow</code>, <code>sin</code>, <code>sinh</code>, <code>sqrt</code>, <code>tan</code>, <code>tanh</code>, <code>trunc</code>

Les fonctions suivantes sont également intégrées et fournies par Pixel Math

- <code>fac</code>  (factorielles e.g. <code>fac 5 == 120</code>)
- <code>ncr</code>  (combinaisons e.g. <code>ncr(6,2) == 15</code>)
- <code>npr</code>  (permutations e.g. <code>npr(6,2) == 30</code>)

Aussi, les constantes suivants sont disponibles :

<code>pi</code>, <code>e</code> 

### De nouvelles commandes
- <code>tilt</code> : Cette commande est expliquée plus haut. Elle permet d'afficher un dessin représentant le tilt du capteur. L'option <code>clear</code> peut être passée en argument afin d'effacer le dessin.
- <code>dir</code> : Cette commande n'existe que sur la version compatible avec Microsoft Windows et elle est l'équivalente de la commande <code>ls</code> déjà présente sous les versions UNIX. Elle permet de lister les fichiers compatibles Siril présents dans le dossier de travail.
- <code>seqcosme et seqcosme_cfa</code> : Applique la moyenne locale à un ensemble de pixels sur la séquence donnée en argument (correction cosmétique). Les coordonnées de ces pixels sont dans un fichier ASCII <code>[fichier .lst]</code> également donné en argument. Cette commande est adaptée pour corriger les pixels chauds et froids résiduels après prétraitement lorsque l'on ne peut par exemple pas appliquer de master darks. La commande est déclinée en 2 versions, une monochrome et une CFA.
- <code>boxselect</code> : Cette commande donne la possibilité de faire une sélection avec des arguments <code>x</code>, <code>y</code>, <code>width</code> et <code>height</code>, avec <code>x</code> et <code>y</code> étant les coordonnées du coin supérieur gauche, et <code>width</code> et <code>height</code>, la taille de la sélection (largeur et hauteur). Si aucun argument n'est passé et qu'une sélection est active, <code>x</code>, <code>y</code>, <code>width</code> et <code>height</code> sont écrits dans l’onglet Console.

### Reformatage de l'aide des commandes
Certaines commandes, devenant de plus en plus complexes, avaient une aide difficile à lire et à comprendre. Celles-ci ont donc été entièrement revues, corrigées et reformatées.
{{<figure src="command_helper.fr.png" link="command_helper.fr.png" caption="L'aide de la commande stack accessible en cliquant sur le bouton à droite de la ligne de commande." width="100%" >}}


### Contributeurs
Les contributeurs à cette release sont : Cyril Richard, Cécile Melis, Vincent Hourdin, Florian Benedetti, ratmilk, Anthony Hinsinger, Andreas Kempe, Benjamin A. Beasley, Steffen Schreiber et Guillaume Roguez.

### Traducteurs
Cyril Richard, Stefan Beck, Kostas Stavropoulos, Xiaolei Wu, Joaquin Manuel, Alexandre Prokoudine, Luca Benassi, Antoine Hlmn, Kenji Iohara, Marta Fernandes

### Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
