---
title: Siril 1.0.5
author: Cyril Richard
date: 2022-09-09T01:00:00+00:00
categories:
  - News
tags:
  - new release
aliases:
  - /download/1.0.5/
version: "1.0.5"
linux_appimage: "https://free-astro.org/download/Siril-1.0.5-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.5-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.5-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.5.tar.bz2"
---

Shortly after the release of version 1.0.4, last week, we are forced to make a fix and release Siril 1.0.5 much earlier than expected. Indeed, we found a bug introduced in the previous version when we had fixed a bug. Sometimes, errors compensate and don't show up, but when you fix one, the other one shows up. This is exactly what happened here. This bug is critical and causes a shift of 1 pixel in x and y after a registration of images on which there was a meridian flip. We would like to warmly thank Kristopher Setnes for finding this bug and making sure it doesn't live for long.

### Downloads
Siril 1.0.5 is distributed for the 3 most common platforms (Windows, MacOS, GNU / Linux). Check out the [downloads](../../../download) page.

But of course, as Siril is a free software, you can build from the sources, available through the [installation](https://free-astro.org/index.php?title=Siril:install) page.

### What's new
Obviously the new features are few, but not non-existent. Indeed, a new button has been added to the gradient extraction: when pressed, it allows to see the original image in order to compare before and after.
The list of corrected bugs is as follows:
- the dropper button of the hyperbolic stretching tool did not work on 16bit images
- the reading of GAIA catalogs had a problem and could cause astrometry to fail
- the alignment had an offset of 1px in x and y on the images returned after the meridian

### Siril in the Windows Store
Finally, this news would not be complete if I did not announce the great news of the official presence of Siril in the [Microsoft store](https://apps.microsoft.com/store/detail/siril/XPDM23ZQ9CCLVF). This is indeed a big improvement in terms of visibility and a kind of recognition: feel free to leave a review of the application. Of course we will continue to produce a classic installer for users who prefer it.

At the time of writing, version 1.0.4 is available for download. But when a new version of Siril is released, you have to wait a few hours before you can download it from the store, and we don't have control over that.

{{<figure src="MicrosoftStore.png" link="MicrosoftStore.png" width="100%" caption="Siril in the Microsoft Store. Feel free to leave a comment.">}}

Of course, despite all our efforts, bugs may still exist (I think this is obvious now with this unexpected release). If you think you have found one, please reach out to us through the [forum](https://discuss.pixls.us/siril). Even better, do file a bug report [there](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) if you see it is not already listed as a [known bug](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).


**Contributors** to this release are: Cyril Richard, Cécile Melis, Vincent Hourdin. We also want to thank all the beta testers and especially Fred Denjean, for his gift to find bugs.

### Donate 
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.
