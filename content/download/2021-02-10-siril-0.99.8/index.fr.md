---
title: Siril 0.99.8.1
author: Cyril Richard
date: 2021-02-13T08:30:00+00:00
categories:
  - Nouvelles
tags:
  - new release
version: "0.99.8.1"
linux_appimage: "https://free-astro.org/download/Siril-0.99.8.1-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-0.99.8.1-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-0.99.8.1-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-0.99.8.1.tar.bz2"
---

### <span style="color:red">13 Février, mise à jour importante</span>

En raison d'un crash sur certains fichiers FITS avec la version 0.99.8 (provenant généralement du système ASIAIR), un correctif a été rapidement publié sous la version 0.99.8.1. Nous encourageons tout le monde à mettre à jour le logiciel, même si Siril dit que tout est à jour.

### La version
Ceci est la troisième version bêta de la prochaine version 1.0. Elle est livrée avec de nombreuses améliorations et corrections de bugs par rapport à la version bêta précédente (0.99.6) et aussi par rapport à la version stable (0.9.12), et continue le grand refactoring commencé avec la série 0.99.

Les améliorations les plus importantes sont les nouveaux outils d'astrométrie et les améliorations de performances et de stabilité.

Nous aimerions spécialement remercier les beta-testeurs, qui ont permis, grâce à leur super travail, de faire cette version avec tant de correctifs. Merci à Cécile, Colmic, Stéphiou et Trognon !

{{< youtube id="G1ZCKQh9eKk" title="Survol des nouveautés de la version 0.99.8" >}}

### Téléchargements
En tant que version bêta de Siril 1.0, nous ne distribuons cette version qu'avec les packages pour les 3 plates-formes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../download).

Mais comme d'habitude, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page installation.

## Quoi de neuf dans cette version ?

### Nouveau site internet
Vous aurez probablement remarqué la refonte complète de notre site web : ce dernier, assez compliqué à gérer, contenait de nombreux bugs et était très lourd. Nous avons alors décidé de radicalement changer en développant un site web statique. Ce fastidieux travail a été fait par l'équipe de [pixls.us](https://pixls.us/) avec qui nous collaborons, et plus particulièrement Mica et PatDavid que nous remercions chaleureusement.

### Nouvelles fonctionnalités d'annotation d'astrométrie et de recherche d'objets
Il s'agit probablement de la fonctionnalité la plus importante introduite dans cette version tant celle-ci est maintenant au cœur de Siril. Il existe plusieurs façon de faire de l'astrométrie dans siril. Soit dans le menu "burger", Information de l'image et Résolution Astrométrique, soit dans le menu "Traitement de l'image" avec l'outil d'étalonnage des couleurs par photométrie (seulement accessible pour les images couleurs). Ces outils définissent des mots clés dans l'entête du fichier FITS (qui sont bien évidemment perdus si on enregistre le fichier dans un autre format) et qui permettent à Siril de définir en temps réel une conversion entre la position d'un pixel et ses coordonnées dans le World System Coordinates. Ceci est visible lorsque l'on déplace la souris, dans la barre d'état en dessous de l'image. Plusieurs nouvelles fonctions découlent alors de cette possibilité.
* Annotation : Lorsque l'astrométrie a réussie, mais il est également possible de la faire dans un logiciel externe, le bouton {{< figure src="astrometry_button.png" >}} est dégrisé et permet d'afficher les annotations des objets présents sur l'image. Par défaut Siril embarque les principaux catalogues, et la sélection/déselection de ceux-ci peut se faire via les préférences et l'onglet astrométrie.
{{< figure src="pref_astrometry.png" caption="Les préférences sont accessibles via le menu burger ou avec la suite de touche ctrl + P.">}}
* La fenêtre des résultat de la PSF d'un objet a été améliorée avec l'ajout d'un lien permettant d'avoir plus de détails sur l'objet étudié. {{< figure src="PSF.png" caption="Boite de dialogue d'une PSF sur une étoile.">}} Il faut bien sûr avoir une connexion internet pour pouvoir utiliser cette fonctionnalité qui va intérroger la base de données SIMBAD. Ceci peut s'avérer très pratique lors de travaux photométriques afin de savoir si une étoile que l'on souhaite utiliser comme étoile de référence est une variable ou non.
* La recherche d'objet, accessible via la combinaison de touche ctrl + "/" ou le menu popup (clic droit), permet de rechercher un objet présent dans l'image et de l'annoter si celui-ci n'est pas déjà disponible dans les catalogues fournis par défaut.

### Nouvelle fonctionnalité de capture d'image instantanée
Afin de partager son image à n'importe quel moment du traitement, avec ou sans les annotations astrométriques, et en tenant compte des modes de visualisation, une fonctionnalité d'instantané a été rajoutée. Il suffit de cliquer sur l'appareil photo situé dans la barre d'en-tête en haut du programme. L'instantané est alors enregistré dans le répertoire de travail.

### Refonte des algorithmes de conversion et d'exportation de séquence
La conversion est au centre du fonctionnement de Siril qui nécessite de devoir travailler avec tout type de format. Il faut donc un algorithme robuste, capable de prendre en charge toutes les situations possibles en fonction des formats d'entrée et de sortie. Rappelons au passage que Siril peut nativement travailler avec le format FITS (format FITS individuel ou FITS-cube) et le format SER. La partie conversion a donc été entièrement repensée afin d'optimiser la gestion des fichiers et de la mémoire. De même, la partie exportation a été revue, car bien que semblant être identique à l'outil conversion, il n'en est rien. Il est en effet possible d'exporter une séquence en prenant en compte les données d'alignement précédemment calculées.

### Nouvelle possibilité d'exclure les images de séquence à partir de l'onglet Graphique
{{< figure src="plot_remove.gif">}}
Il est maintenant possible d'interagir sur la sélection et la déselection d'images dans la séquence via l'onglet graphique. Cela permet grâce à un rapide coup d'œil d'évaluer la qualité de l'image afin de l'intégrer ou non dans le traitement. Un clic sur le point et l'image est déselectionnée. Elle n'est pas supprimée, juste exclue de la séquence. On peut bien évidemment la réintégrer à partir de la liste des images de la séquence.

### Outil de saturation des couleurs amélioré avec un facteur d'arrière-plan pour ajuster la force
{{< figure src="saturation.png" caption="L'outil saturation a été légèrement amélioré.">}}
L'outil de saturation s'est vu amélioré avec l'ajout un curseur définissant le niveau du fond de ciel. Cela permet d'augmenter la saturation de l'image sans faire remonter celle du bruit de l'image.

### Amélioration du support des films (AVI et autres)
L'utilisation de film dans Siril est maintenant noté comme obsolète. Le problème principal avec l'AVI ou d'autres conteneurs de vidéos est qu'ils sont conçus pour fonctionner avec un grand nombre de codecs et de formats de pixels, ce qui est une bonne chose quand on l'utilise pour des films de tous les jours, mais cela nécessite que les logiciels de traitements d'image pour l'astronomie gère un large panel de formats de fichiers différents. Les logiciels utilisant ces films sont souvent mal équipés pour prendre en compte les pixels à 16 bits par valeur ou certains formats de fichiers non compressés comme expliqué [ici](https://free-astro.org/index.php?title=SER/fr). Siril prévient qu'il est possible de convertir le film en une séquence SER en un seul clic.
{{< figure src="sequence_obsolete.png">}}

### Nouvelles commandes et amélioration de certaines
* Nouvelle [commande merge](https://free-astro.org/index.php?title=Siril:Commands#merge) qui fusionne des séquences
* Nouvelle [commande seqstat](https://free-astro.org/index.php?title=Siril:Commands#seqstat) qui calcule les statistiques sur les images d'une séquence
* Ajout des types de rejet dans la [commande stack](https://free-astro.org/index.php?title=Siril:Commands#stack)
* Correction de la [commande setref](https://free-astro.org/index.php?title=Siril:Commands#setref) pour fonctionner sans séquence ouverte

### Optimisations
* Réduction de la mémoire utilisée par l'alignement global
* Amélioration du temps de lecture des images FITS
* Réduction de la taille minimal de la fenêtre principale afin de s'intégrer dans les environnements à faible résolution
* Amélioration de l'adaptation des polices à différentes résolutions d'écran. En effet, il est maintenant possible de définir un facteur d'échelle pour la police utilisé dans Siril. Ceci peut s'avérer indispensable avec l'utilisation de certaines très haute résolutions. L'option est accessible via les préférences. {{< figure src="polices.png" caption="Il est possible de choisir l'échelle de la taille de la police ainsi que quelques autres fonctionnalité estétiques.">}}

### Corrections de bugs
* Correction de l'analyse de séquence de PSF (seqpsf) pour les images 32 bits et pour les séquences sans données d'alignement
* Correction des conditions de manque de mémoire sur l'alignement global et l'empilement médian ou moyen (provoquant le gel des systèmes)
* Correction des limites mémoire pour Siril 32 bits sur un système d'exploitation 64 bits (la mémoire disponible devient 2 Go) et correction de l'ouverture des gros fichiers SER sur Siril 32 bits
* Correction de l'empilement minimum ou maximum qui ne fonctionnait pas avec les images en couleur
* Correction d'un plantage lors d'une résolution astrométrique au deuxième essai et amélioration de sa vitesse
* Correction des dates des images dans un FITS cube

La liste complète des tickets (nouveautés et correctifs, avec des détails) est disponible [ici, sur gitlab](https://gitlab.com/free-astro/siril/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=beta%3A%3A0.99.8).

### Traducteurs
Cyril Richard, Stefan Beck, Kostas Stavropoulos, Xiaolei Wu, Joaquin Manuel, Alexandre Prokoudine, Luca Benassi, Antoine Hlmn, Kenji Iohara, Marta Fernandes

### Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
