---
title: Siril 1.2.3
author: Cyril Richard
date: 2024-06-19T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /download/1.2.3/
version: "1.2.3"
linux_appimage: "https://free-astro.org/download/Siril-1.2.3-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.3-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.3_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.3-3-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.3-3-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.3.tar.bz2"
---

### <span style="color:red">July 29th, release udpate for macOS packages</span>

The first bug found has not been completely corrected, and many users have reported slowness. We therefore invite all macOS users to download the new package, revision 3, which bears the build number 11207.

### <span style="color:red">July 2nd, release udpate for macOS packages</span>

A bug has been found in macOS versions preventing the use of multiple threads during calculations. Both packages have been updated to version 2 (build number 10977). We invite all our macOS users to download the new packages.

# This version

Although Siril 1.2.2 was released very recently, we have to quickly publish a new version, 1.2.3.

**Why?** The explanation is simple: several very annoying bugs have been discovered. Some affect only the macOS version, others the Windows version, and one affects all versions.

**List of fixed bugs (since 1.2.2)**

* Fixed handling wide-char characters in filenames on Windows (cfitsio rollback) (#1324)
* Fixed compression error (files were not compressed anymore) (#1328)
* Fixed internet connection in macOS version (packaging issue)

# Downloads

Siril 1.2.3 is distributed as usual for the three most common platforms (Windows, macOS, GNU/Linux). See the [download](../../../download) page.

But of course, since Siril is free software, it can be built on any OS from the sources, see the [installation](https://siril.readthedocs.io/en/stable/installation/source.html) page.

# A Stabilization Version

Once again, this version is a stabilization release of the 1.2 series. Therefore, there are no new features, but Siril becomes more stable thanks to all the community feedback. This is why it is very important to report any bugs you find, as it is generally the only way to have them fixed in the next version.

We thank you for your continued support and valuable feedback. Download Siril 1.2.3 now and enjoy a more stable and reliable version!

# What's New with Siril 1.4?

The upcoming version 1.4 is making great strides. We are still in the process of introducing new features but should be approaching a stabilization phase quite soon. It represents a major advancement, almost a break with previous versions: a lot of care has been put into ergonomics. Also, the number of new features is becoming very significant and we can't wait to release a beta version. Stay tuned, you never know!

# Contribute to Siril
Of course, Siril is a computer program developed by fallible humans, and bugs may still exist. If you think you've discovered one, please [contact](../../faq/#how-can-I-contact-siril-team-) or [write](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) a bug report if it isn't already present in the [list of known bugs](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). A [documentation page](https://siril.readthedocs.io/en/stable/Issues.html) has been written to help you report a problem.


# Donate
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.
