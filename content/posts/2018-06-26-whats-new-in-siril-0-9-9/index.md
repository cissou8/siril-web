---
title: What’s new in Siril 0.9.9
author: Argonothe
date: 2018-06-26T13:25:48+00:00
featured_image: cropped-m31.jpg
categories:
  - News
---

## This release contains some new features as well as some bug fixes:

  * Major update of the command line, with completion and documentation in the GUI, enhanced scripting capability by running commands from a file and also allowing it to be run with no X11 server running with the -s command line option
  * Added commands to stack and register a sequence
  * Image statistics, including auto-stretch parameters and stacking normalization, are now cached in the seq file for better performance
  * Global star registration now runs in parallel
  * Workflow improvement by adding demosaicing as last part of the preprocessing
  * Added a filtering method for stacking based on star roundness
  * Added an option to normalize stacked images to 16-bit with average rejection algorithm
  * All GUI save functions are now done in another thread
  * Improved histogram transformation GUI
  * Improved support of various FITS pixel formats
  * Preserve known FITS keywords in the stacked image by average method
  * Added native open and save dialogues for Windows users
  * Various Windows bug fixes in SER handling
  * Fixed wrong handling of scale variations in Drizzle case
  * Fixed 8-bit images auto-stretch display
  * Fixed BMP support
  * Fixed issues in PNG and TIFF 8-bit export
  * Fixed the “About” OS X menu
