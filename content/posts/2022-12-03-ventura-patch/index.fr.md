---
title: Correction de bug pour MacOS Ventura
author: Cyril Richard
show_author: true
date: 2022-12-03T00:00:00+00:00
featured_image: dmgpackage.png
categories:
  - Nouvelles

---

MacOS Ventura est la treizième version majeure de macOS, sortie récemment. Actuellement, Siril ne peut y être exécuté correctement car il est impossible d'utiliser la souris sur l'application. Ce bug, très gênant, est causé par des changements de bas niveau dans Ventura qui affectent GTK, la bibliothèque qui gère l'interface graphique de Siril. Et toutes les applications utilisant GTK3 sont concernées par ce bug, à savoir GIMP v2.99, RawTherapee, Inkscape, Darktable et bien d'autres...

Actuellement nous sommes en mesure de fournir une version de Siril dont une correction partielle a été effectuée sur GTK. Cette dernière permet à l'utilisateur de se servir à nouveau de la souris, mais il se peut que d'autres effets non désirés existent. Nous vous tiendrons informés de l'évolution du sujet.

En attendant, je vous invite à vous rendre sur la page [Téléchargement](../../../download) et de récupérer la version de Siril correspondant à l'architecture de votre ordinateur Apple. Ces versions sont fournies par notre nouveau mainteneur macOS, René de Hesselle.
