---
title: 'Sirilic and Sirilot: Two very useful utilities for Siril'
author: Argonothe
date: 2018-11-15T10:20:30+00:00
featured_image: /wp-content/uploads/2018/11/rvb2.jpg
categories:
  - Tips
---

## SIRILIC

(Siril Image Converter) is a software for preparing acquisition files (raw, Offset, Flat and Dark) for processing with SiriL software.

It does three things:

* Structuring the SiriL working directory into sub-folders  
* Convert Raw, Offset, Dark or Flat files into SiriL sequence  
* Automatically generate the SiriL script according to the files present and the options

## SIRILOT

The Sirilic version V0.36 provides an additional tool: Sirilot

It&#8217;s the contraction of &#8220;SiriL + Lot&#8221;. SiriLot is a siril extension that allows you to batch process multiple channel and sessions. Indeed, Sirilic is adapted to process only 1 layer but as soon as you wanted to process the 4 layers LRGB, you had to do 4 times the operation &#8220;Copy + script generation&#8221;. The author still wanted to keep Sirilic: so he created a second tool with a more batch-oriented graphical interface:

![sirilot](sirilot.jpg)


More information here: <a href="https://astroslacholet.wordpress.com/2018/10/12/utilitaire-conv2siril/" target="_blank" rel="noopener">https://gitlab.com/free-astro/sirilic</a>

