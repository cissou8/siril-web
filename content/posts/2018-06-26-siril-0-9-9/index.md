---
title: SIRIL 0.9.9
author: Argonothe
date: 2018-06-26T13:24:23+00:00
featured_image: /wp-content/uploads/2018/06/Siril_0.9.9.png
categories:
  - News
---

This release is the ninth stability release for the 0.9 branch. Its SVN revision is _2403_. It also has an SVN tag: <a class="external text" href="https://free-astro.org/svn/siril/tags/0.9.9" rel="nofollow">0.9.9</a>. Stability updates and minor improvements occur in the dedicated <a class="external text" href="https://free-astro.org/svn/siril/branches/0.9" rel="nofollow">0.9 branch</a>. This is the second version officially supported on Windows, and it even includes developments that were specially made for Windows. But please keep in mind that Siril is developed _without_ Windows and that we rely on Windows users to test and give us feedback about integration or porting problems.

This update focuses on automating the processing steps of a complete acquisition by introducing scripts, allowing for the first time Siril to be run in console and improving speed of registration and many other operations.

Leveraging the new scripting, Siril 0.9.9 has been integrated as an automatic way of processing acquisitions in the capture software <a class="external text" href="https://www.ap-i.net/ccdciel/en/start" rel="nofollow">CCDciel</a>.
