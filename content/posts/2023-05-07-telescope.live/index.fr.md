---
title: Siril et Telescope.Live lancent une nouvelle série de tutoriels
author: FreeAstro team
show_author: true
date: 2023-05-07T00:00:00+00:00
featured_image: telescopelive.jpg
categories:
  - Nouvelles

---

Nous sommes heureux de vous annoncer que Telescope Live, la plateforme en ligne 
dédiée à l'astrophotographie et à l'imagerie à distance, a créé une nouvelle 
série de tutoriels vidéo pour Siril, disponible gratuitement pour tous. Les 6 
vidéos, d'une durée totale de 39 minutes, sont spécialement destinées aux 
débutants. Elles couvrent de nombreuses opérations nécessaires pour transformer 
un ensemble de données brutes en une image magnifiquement traitée.

{{<figure src="telescopelive.jpg" link="https://telescope.live/tutorials/ultimate-astrophotography-toolkit-beginners">}}

Telescope Live utilise des télescopes de très haute qualité sous les meilleurs 
cieux des deux hémisphères et fournit un grand nombre d'images dans ses archives, 
mais à un prix abordable. Ils ont passé du temps à apprendre à utiliser Siril, 
à créer des scripts et à produire les tutoriels vidéo, les rendant disponibles 
gratuitement. Si cette série de tutoriels est gratuite et accessible à tous, 
c'est parce qu'ils partagent la même mission que nous : rendre l'astrophotographie 
accessible à tous.

En choisissant Siril pour cette tâche, ils nous confortent dans l'idée que 
Siril est un excellent outil pour les débutants, ainsi que pour les 
astrophotographes plus expérimentés, pour ceux qui veulent obtenir des résultats 
rapidement ou de manière automatisée.

Nous les remercions chaleureusement.

[Aller voir les tutoriels](https://telescope.live/tutorials/ultimate-astrophotography-toolkit-beginners)

[En savoir plus sur Telescope Live](https://telescope.live/start (essai gratuit 
d'une semaine))

Pour rappel, vous trouverez nos tutoriels détaillés, textes et images, sur
notre [site web](../../../tutorials).
