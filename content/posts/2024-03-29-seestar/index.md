---
title: New ZWO Seestar script and tutorial
author: Cyril Richard
show_author: true
date: 2024-03-29T00:00:00+00:00
featured_image: seestar.jpg
categories:
  - News

---

We are pleased to announce the release of a script and tutorial dedicated to Zwo's new All-in-One telescope, Seestar.

The tutorial explains step-by-step how to use the script, and then how to process the stacked image. The proposed script has been designed to automatically pre-process the raw images recorded by the Seestar and give the best image, automatically.

We hope it will meet your expectations. To find out how, click [here](../../../tutorials/seestar). The link to download the script is in the tutorial.
