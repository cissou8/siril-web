---
title: Introduction to Siril’s GUI
author: Argonothe
date: 2020-01-27T15:50:53+00:00
featured_image: /wp-content/uploads/2020/01/Capture-d’écran-du-2020-01-27-16-59-04-1024x576.png
categories:
  - Tips
  - Video
---

Here is a video in English that presents the new interface of Siril.

It will be followed by many others in order to present all the features of the Siril software.

**Remember** to **subscribe** to our youtube channel.
