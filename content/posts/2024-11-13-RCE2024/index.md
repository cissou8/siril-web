---
title: Siril at RCE 2024
author: Cyril Richard
show_author: true
date: 2024-11-13T00:00:00+00:00
featured_image: RCE_2024.jpg
categories:
  - News

---

We were delighted to participate in the [**Rencontres du Ciel et de l’Espace 2024**](https://www.afastronomie.fr/rencontres-ciel-espace) in Paris, at La Villette, an essential event organized by the French Astronomy Association (AFA). The event took place on **November 9, 10, and 11**, and our presentation was a great success: the room was packed, and unfortunately, not everyone could get in.

{{<figure src="pano.jpg">}}

During the presentation, we had the opportunity to showcase the **new features of Siril 1.4.0**, which is not yet released. It was an amazing moment, shared with astronomy and imaging enthusiasts.

For those who couldn’t attend, we filmed the presentation, and it’s now available on YouTube. Please note, the video is intended for French-speaking viewers, as it is entirely in French with no translation.

Thank you to everyone who came and continues to support Siril’s development!

{{< youtube 23rZ92g4Fzs >}}
