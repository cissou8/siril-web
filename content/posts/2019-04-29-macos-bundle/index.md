---
title: MacOS Bundle
author: Argonothe
date: 2019-04-29T08:14:34+00:00
featured_image: mac.png
categories:
  - News
---

Hello,

We are looking for a MacOS specialist who would be able to realize, or help us realize the MacOS Bundle.

You can contact us at the following address:

<img loading="lazy" src="Capture-du-2019-04-29-10-01-16.png" /> 

Thank you in advance for your help,

The Siril development team
