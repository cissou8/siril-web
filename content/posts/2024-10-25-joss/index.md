---
title: New Siril Publication in JOSS
author: Cyril Richard
show_author: true
date: 2024-10-25T00:00:00+00:00
featured_image: joss_publication.png
categories:
  - News

---

We are thrilled to announce the publication of a peer-reviewed article on Siril in the [Journal of Open Source Software](https://joss.theoj.org/) (JOSS). This publication is a major milestone for us, as it validates the software’s development, usability, and impact in the open-source and astronomical imaging communities.

If Siril has been part of your work, we encourage you to cite it using the following reference:

**C. Richard et al., Journal of Open Source Software, 2024, 9(102), 7242. DOI: 10.21105/joss.07242**

This citation helps support Siril’s continued development and strengthens its standing in the scientific and open-source communities. Thank you for your support!

For further details and to access the article, click [here](https://doi.org/10.21105/joss.07242).

