---
title: Nouvelle publication de Siril dans JOSS
author: Cyril Richard
show_author: true
date: 2024-10-25T00:00:00+00:00
featured_image: joss_publication.png
categories:
  - News

---

Nous sommes ravis d'annoncer la publication d'un article scientifique sur Siril dans le [Journal of Open Source Software](https://joss.theoj.org/) (JOSS). Cette publication représente une étape importante pour nous, en validant le développement, l'utilisabilité et l'impact de notre logiciel au sein des communautés de l'imagerie astronomique et du logiciel libre.

Si Siril fait partie de votre travail, nous vous encourageons à le citer en utilisant la référence suivante :

**C. Richard et al., Journal of Open Source Software, 2024, 9(102), 7242. DOI: 10.21105/joss.07242**

Cette citation contribue à soutenir le développement continu de Siril et renforce sa reconnaissance dans les communautés scientifiques et open source. Merci pour votre soutien !

Pour plus de détails et pour accéder à l'article, cliquez [ici](https://doi.org/10.21105/joss.07242).

