---
title: New OS X installer available
author: Cyril Richard
date: 2018-08-16T15:03:29+00:00
featured_image: /wp-content/uploads/2018/08/pkgimage.jpg
categories:
  - News
---

A new OS X installer, packaged by a Siril user, is now available for the 0.9.9 version. The great news of this package is that the set of DSLR scripts is now embeded as it was already done for the Windows version. Moreover, it is now possible for the user to uninstall the application very easily and properly from his computer.

Go to the [download][1] section in order to get it.

 [1]: /download
