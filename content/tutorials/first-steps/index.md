---
title: First steps -- pre-processing with scripts
author: Cyril Richard
show_author: true
featured_image: Capture-05.png
rate: "0/5"
type: page
---

This tutorial explores your first steps with Siril and its scripting capability.

Siril comes with a few basic scripts. They allow you to preprocess your images in one click!

# 1. Create the folders to hold your images types.

Create the following 4 directories named as:
 * `biases`
 * `darks`
 * `flats`
 * `lights`

# 2. Put your RAW images in the directories created in the previous step.

*Attention*: don’t mix other files, even JPEG (or other formats), with your RAW files: Siril will take all the files inside the directories as input files for processing.

# 3. Click on the *home* button and navigate to your project folder .

This is tell Siril where all your images are located.

# 4. Validate with the *Open* button of the window.

{{< figure src="Capture-02.png" caption="Headerbar of Siril" >}}

Siril will validate your working directory.

# 5. Click on the *Scripts* button and select the script of your choice.

The main script that covers basic needs is named `OSC_preprocessing`.

If you don’t see the button, do the follwoing:
1. Go to your preferences (`ctrl` + `P`)
2. Remove all script paths (as shown in the following screenshot).

  {{< figure src="Capture-03.png" caption="Preferences dialog box. Script paths can be added here." >}}

3. Validate your changes.
4. Close and restart Siril.

   You are now ready to run a script.

If you need more scripts:
1. Click on the *hamburger* menu and on “Get Scripts”.

   Script files are text files with `ssf` extension.

2. Download a script.

   You can download any script you want, put it in any folder. You just have to indicate to Siril the path where it needs to search for it. Do it in Preferences, in the same tab as seen in the illustration above.

   {{< figure src="Capture-04.png" caption="Main menu of Siril." >}}

Depending on the number of RAW images and the power of your computer, Siril can take some time to process your images. At the end of the script, you should have a window like that:

   {{< figure src="Capture-05.png" caption="Script is running." >}}

Your images have been calibrated, aligned (registered) and stacked. Congratulations! It is now time to process the result in Siril.

You can open it by clicking on the *Open* button (upper left), your image will be named *result_xxx.fits* (xxx représente le temps total d'exposition en secondes).

   {{< figure src="Capture-06.png" caption="The stacked image in the file tree." >}}

It's normal that your image looks dark, you should choose the "AutoStretch" mode (lower part of the window) to get a brighter view of it.
