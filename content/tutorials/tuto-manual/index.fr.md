---
title: Pré-traitement manuel
author: Dominique Dhoosche
show_author: true
featured_image: 12-background_extraction.fr.png
rate: "2/5"
type: page
---

## Introduction
Dans ce tutoriel, nous allons étudier le process de pré-traitement d'une image astro réalisée avec un appareil photo numérique (APN), mais le traitement d'images provenant de caméra astronomiques est quasiment identique.
Bien sûr, Siril est doté de [scripts](../tuto-scripts/), qui réalisent ce process de façon automatique. Mais, [dans un certain nombre de cas](../../faq/#y-a-t-il-un-inconvénient-à-utiliser-des-scripts-), il vous faudra effectuer un traitement manuel : 
 - Vous voulez comprendre ce que font les scripts : le meilleur moyen est en effet de faire un pré-traitement manuel !
 - Vous voulez peaufiner le pré-traitement : les scripts prennent en effet des valeurs par défaut, qu'il est possible d'optimiser
 - Vos images contiennent des gradients, difficiles à enlever au traitement. Il est très intéressant de les enlever sur les images brutes : le gradient est plus simple et plus facile à éliminer.
 - Il vous manque des flats : bien sûr, c'est formellement déconseillé, mais les séances de prises de vues ne se déroulent pas toujours comme on le souhaiterait ! 
 - Et plein d'autres raisons... qui vous appartiennent :-)

### Fichiers utilisés
Pour ce tutoriel, nous allons considérer que vous possèdez des images de l'objet, des offsets, des flats et des darks. 

### Rangement des fichiers
Il est préférable de ranger ses fichiers dans différents sous-dossiers. Nous allons nous calquer sur les sous-dossiers utilisés dans les scripts : `brutes` pour les images, `offsets`, `darks` et `flats` pour les images outils.
NB: Depuis la version 1.2.0, les repertoires utilisés par les scripts ont été unifiés sous leurs dénomination anglaise, soit `lights`, `biases`, `darks` et `flats` respectivement.

## Principe de pré-traitement
Le calcul des images est fait par la formule suivent :
$$
\frac{\text{image}-{\text{dark}}-\text{offset}}{\text{flat}}
$$
Par principe, les offsets sont contenus dans toutes les images que nous avons réalisées : les brutes bien sûr, mais aussi dans les darks et les flats. Il faudra donc enlever les offsets des darks et des flats pour utiliser ces images-outils. Mais analysons cette formule.
$$
\frac{\text{image}-{\text{dark}}-\text{offset}}{\text{flat}} = \frac{\text{image}-({\text{dark}}+\text{offset})}{\text{flat}}
$$
Nous constatons qu'il suffit de soustraire les darks **NON TRAITÉS** (qui contiennent donc également les offsets) pour retirer en une fois le dark **ET** les offsets : gain de travail et de temps !

Pour pré-traiter les images, le mode opératoire proposé est le suivant :
1. Définir le dossier de travail.
1. Convertir les fichiers.
1. Créer l’offset (ou bias) maître.
1. Créer le flat maître, en soustrayant l'offset maître.
1. Créer le dark maître, sans soustraire l'offset maître.
1. Calibrer les images, avec flat et dark maîtres.
1. Traiter les gradients.
1. Aligner les images.
1. Empiler les images.

## Définir le dossier de travail
{{<figure src="03-working_files.fr.png" link="03-working_files.fr.png" caption="Définir le dossier de travail." >}}
1. Cliquer sur l’icône `Maison`
1. Rechercher le dossier où se trouvent les images, puis créer un dossier `travail` (les scripts crééent un dossier `process`)
1. Double clic sur le dossier `travail` pour entrer dedans
1. Valider

Toutes les images seront désormais enregistrées dans ce dossier

## Convertir les fichiers
{{<figure src="04-file_conversion.fr.png" link="04-file_conversion.fr.png" caption="Convertir ses fichiers" >}}
Siril travaille avec des fichiers au format FITS (Flexible Image Transport System). Il faut donc procéder à une conversion de vos fichiers raw. Pour les gens possédant des images FITS provenant de caméras dédiées, La conversion n'est pas nécessaire mais un renommage si. Il est aussi possible de créer une séquence sans copier les fichiers mais en créant des liens symboliques. Une explication pour le faire sous Windows est donnée [ici](../tuto-scripts/#important-avant-de-commencer-a-nametuto-1a).
1. Cliquer sur l’onglet `Conversion`
1. Cliquer sur  `+ Ajouter` 
1. Rechercher le dossier ou se trouvent les images à convertir (par défaut, on est dans le dossier de travail)
1. Sélectionner les images à convertir
1. Ajouter
1. Donner un nom générique aux images à convertir (`brutes`, `flats`, ...). Siril ajoute un nombre derrière ce nom pour différencier les images, et cela permet la création des séquences.
1. Cliquer sur `Convertir`

Siril convertit les images en format `.fit` dans le dossier par défaut. Il faut ensuite procéder à la même manipulation avec les brutes, les flats, les offsets et les darks, en leur donnant à chaque fois un nom générique évocateur.

## Les séquences
Un mot sur les séquences, très importantes dans Siril. Une séquence est une suite d'images `cohérentes` : les images, les darks, les offsets, etc. Ces séquences sont listées dans des petits fichiers dont l'extension est `.seq`. Ainsi, lorsque vous convertissez des images d’offset en leur donnant comme `nom de séquence` : `Offset`, les images converties portent les noms `Offset_00001`, `Offset_00002`, `Offset_00003`, etc. Le nom de la séquence correspondante est `Offset` et est contenue dans le fichier `Offset.seq` que nous conseillons de ne jamais éditer à la main.

Il est tout à fait possible d'avoir plusieurs séquences dans le même dossier. Cependant, pensez bien qu'il faut **TOUJOURS** choisir la séquence de travail avant de procéder à des traitements. Le plus souvent, par défaut, Siril charge la dernière séquence calculée, mais veuillez garder ça en tête !

Dans l'onglet `Séquences`, le bouton `Chercher séquence` analyse votre dossier et recherche les séquences : 
{{<figure src="05-sequences.fr.png" caption="Chercher les séquences" >}}
Vous pouvez forcer Siril à recalculer toutes les séquences du dossier de travail, en cochant la case `Forcer à recalculer le fichier .seq`. Attention cependant cela efface toutes les données (normalisation, alignement, ...) qui ont été préalablement calculées.

## Les offsets
{{<figure src="06-biases_processing.fr.png" caption="On traite les offsets" >}}
1. Cliquer sur l’onglet `Séquences`, puis choisir la séquence des offsets.
1. Cliquer sur l’onglet `Empilement`.
1. Régler les paramètres comme ci-dessus.
1. Cliquer sur `Débute l’empilement`.

Le calcul démarre, Siril affiche la console sur laquelle on peut suivre les opérations effectuées.

Une fois le calcul terminé, vérifiez que le taux de rejet se trouve dans une fourchette 0.1 à 0.5%. Ces valeurs sont complètement arbitraires et permettent à l'utilisateur débutant de se faire une idée. Il n'est pas utile de vouloir à tout prix obtenir une valeur inférieur à 0.5% : une valeur de 1% peut tout aussi bien s'avérer bonne. Gardez seulement à l'esprit qu'il faut vérifier ce taux de rejet, et si celui-ci possède une valeur aberrante, reprendre l’empilement en changeant les valeurs des `Sigmas bas/haut`. 
{{<figure src="07-sigma.fr.png" caption="Vérification des taux de rejets" >}}
**ATTENTION** : quand on augmente un facteur sigma, on abaisse le taux de rejet !

A la fin du calcul, Siril affiche l’image empilée. Elle est enregistrée ici sous le nom Offset_stacked.fit

# Les flats
## Pré-traitement des flats
{{<figure src="08-flats_processing.fr.png" caption="On traite les flats" >}}
1. Cliquer sur l’onglet `Séquences`, choisir la séquence des flats.
1. Cliquer sur l’onglet `Pré-traitement`.
1. Régler les paramètres comme ci-dessus.
1. Cliquer sur `Pré-traiter`.

Siril soustrait les offsets des flats et créée autant de fichiers flats traités en ajoutant le préfixe `pp_` aux noms des fichiers. Vous pouvez bien sûr changer le préfixe (`pp_` = pre-process en anglais).

**REMARQUE**: Nous recommandons de cocher le bouton "Egaliser CFA" pour les images couleurs afin de preserver l'équilibre des cannaux original.

## Création du flat maître
{{<figure src="09-flats_staking.fr.png" caption="On empile les flats" >}}
1. Cliquer sur l’onglet `Séquences`, puis choisir la séquence des flats prétraités (normalement, elle est déjà chargée à cette étape).
1. Cliquer sur l’onglet `Empilement`.
1. Régler les paramètres comme ci-dessus.
1. Cliquer sur `Débute l’empilement`.

Siril empile les flats, et créée le fichier dont on a définit le nom.
Comme pour les offsets, vérifier que le taux de rejet se trouve dans une fourchette 0.1 à 0.5 %. Si les valeurs sont aberrantes, reprendre l’empilement en changeant les valeurs des `Sigmas bas/haut`. Quand on augmente un facteur sigma, on abaisse le taux de rejet !

# Les darks
{{<figure src="10-darks_staking.fr.png" caption="On empile les darks" >}}
Nous l'avons vu plus haut, nous n'enlèverons pas les offsets des darks. Procédons directement à l'empilement.
1. Cliquer sur l’onglet `Séquences`, puis choisir la séquence des darks.
1. Cliquer sur l’onglet `Empilement`.
1. Régler les paramètres comme ci-dessus.
1. Cliquer sur `Débute l’empilement`.

Siril empile les darks, et créée le fichier dont on a préalablement définit le nom (ici `Dark_stacked.fit`, mais vous pouvez lui donner le nom que vous voulez).

Comme pour les offsets, vérifier que le taux de rejet se trouve dans une fourchette 0.1 à 0.5 %. Si les valeurs sont aberrantes, reprendre l’empilement en changeant les valeurs des `Sigmas bas/haut`. Quand on augmente un facteur sigma, on abaisse le taux de rejet !

# Les images

## Pré-traitement des images
{{<figure src="11-lights_processing.fr.png" caption="On pré-traite les images" >}}
1. Cliquer sur l’onglet `Séquences`, puis choisir la séquence des images brutes.
1. Cliquer sur l’onglet `Pré-traitement`.
1. Régler les paramètres comme ci-dessus.
1. Cliquez sur `Estimer` pour connaître le nombre de pixels corrigés par la correction cosmétique. Si l’un des chiffre passe en rouge, changer la valeur de sigma. Si la valeur reste rouge malgré les changements de valeur sigma, alors vous pouvez décocher la correction cosmétique correspondante (pixels chauds ou froids).
1. Cliquer sur `Dématricer avant sauvegarde` pour enregistrer les images en couleur.
1. Cliquer sur `Pré-traiter`.

Siril traite les images avec les darks (et les offsets qu’ils contiennent) et les flats et créée autant de fichiers images traitées, avec le préfixe `pp_` (que vous pouvez changer).

## Retrait de gradient
__La methode de retrait du gradient ci-dessous a été rendue obsolète par la version 1.2.0 de Siril et l'introduction d'un nouvel algorithme de retrait de gradient bien plus performant. Cet outil est detaillé dans le tutoriel [gradient](../../tutorials/gradient/) adéquate.__

Si vos images font apparaitre un gradient, généralement dû à la pollution lumineuse ou à la Lune, il est fort probable que ce gradient évolue au fil de votre session et donc que l'aspect change sur vos images, surtout si vous avez imagé pendant plusieurs heures. Il est alors largement préférable de supprimer ce gradient directement sur les images brutes. En effet l'empilement rendrait ce gradient beaucoup plus complexe à éliminer plus tard.
{{<figure src="12-background_extraction.fr.png" link="12-background_extraction.fr.png" caption="On retire le gradient" >}}
1. Menu `Traitement de l'image`, `Extraction du gradient...`.
1. Une fenêtre s'ouvre. Nous allons poser des points de mesure sur toute l'image. Siril calcule le niveau de fond de ciel sous ces points, et en déduit une interpolation polynomiale du gradient : un ciel synthétique. 
La première chose à régler est l'ordre de l'interpolation. Ici, sur un traitement d'une séqunce d'images brutes, on utilise en général un ordre 1 car comme nous l'avons dit, la forme du gradient est plus simple sur les images unitaires. 
On donne ensuite le nombre de points à positionner sur une ligne horizontale. Ici, j'ai réduit la valeur proposée à 10. J'ai également limité la tolérance, pour que Siril ne positionne pas trop de points près de la nébuleuse. On clique ensuite sur `Générer`. 
Siril positionne alors les points de calcul (les carrés verts) : on peut en supprimer (clic droit sur un carré) ou en ajouter (clic gauche dans l'image ou on veut l'ajouter). S'agissant de gradient de pollution lumineuse, le type de correction à choisir est `Soustractive`.
On n'oublie pas de cocher ensuite `Appliquer à la séquence`, et ici aussi, on peut choisir un préfixe de sortie pour la nouvelle séquence d'images qui est créée.
1. Cliquer sur `Appliquer`. Siril recalcule alors une nouvelle séquence d'image débarassée du gradient. Il sera peut-être nécessaire de refaire une extraction de gradient lors du traitement, mais ce dernier sera alors nettement plus simple à réaliser.

## Alignement des images
{{<figure src="13-registration.fr.png" caption="On pré-traite les images" >}}
1. Cliquer sur l’onglet `Séquences`, puis choisir la séquence des images brutes prétraitées.
1. Cliquer sur l’onglet `Alignement`.
1. Régler les paramètres comme ci-dessus.
1. Cliquer sur `Aligner`.

Siril aligne les images et créée autant de fichiers images alignées, avec le préfixe `r_` (que vous pouvez changer).

__NB: Depuis Siril 1.2.0, l'opération d'alignement a grandement évolué. Il est désormais possible de réaliser cette opération en 2 étapes: tout d'abord par le choix de la méthode d'alignement (comme ci dessus), puis par l'application de l'alignement. Cela permet de dissocier les opérations de calcul d'alignement, des opéartions de modifications des images.__
{{<figure src="13.5-registration.fr.png" caption="Alignement en 2 passes, puis Application" >}}

## Tri des images
Il est temps de vérifier la qualité de vos images, et éliminer les plus mauvaises.
{{<figure src="14-plot.fr.png" caption="Vérification des images" >}}
1. Comme d'habitude, on contrôle la séquence sur laquelle on travaille (si vous venez d'aligner, inutile de vérifier, Siril aura choisi la séquence alignée)
1. Dans l'onglet `Graphique`, Siril indique les FWHM des images de la séquence chargée. Pour éliminer de la séquence les plus désastreuses, on fait un clic droit sur un image à forte FWHM, et on valide "Exclure image XX".
1. En cliquant sur cette icône, on affiche la liste des images de la séquence, et on voit que les images éliminées sont décochées. Vous pouvez travailler sur le graphique ou dans le tableau. 
{{<figure src="15-list.fr.png" caption="Vérification des images" >}}
Une fois votre choix fait, vous pouvez fermer la liste des images.

__Depuis Siril 1.1.0, l'outil de tri des images a évolué. Il est possible de paramétrer à volonté les axes X et Y du graph.__
{{<figure src="14.5-plot.fr.png" caption="Paramétrage des axes" >}}
Le paramètre de tri (colonne de droite) de la liste des images est mofifié d'aprés le paramètre défini En Y. Le processus de tri est identique à ce qui a été décrit précédement.

## Empilement des images
{{<figure src="16-stacking_lights.fr.png" caption="On empile les images" >}}
1. Cliquer sur l’onglet `Séquences`. Et choisir la séquence des images alignées.
1. Cliquer sur l’onglet `Empilement`.
1. Régler les paramètres comme ci-dessus. On voit que Siril a bien enlevé les images éliminées lors de la phase précédente : on ne traite qu'une partie de la séquence.
1. Cliquer sur `Débute l’empilement`.
1. Il est aussi possible, avant d'empiler, de rajouter un critère de sélection des images : par fwhm, ou rondeur.

Siril empile les images choisies, et créée le fichier dont on a définit le nom.
Comme précédemment, vérifier que le taux de rejet se trouve dans une fourchette 0.1 à 0.5 %. Si les valeurs sont aberrantes, reprendre l’empilement en changeant les valeurs des `Sigmas bas/haut`. Quand on augmente un facteur sigma, on abaisse le taux de rejet !

__Depuis Siril 1.2.0, ce dernier onglet a également évolué.__
{{<figure src="16.5-stacking_lights.fr.png" caption="Nouvel onglet d'empilement" >}}
L'option ``Egalisation RVB`` vous permettra d'avoir une image déjà équilibrée en sortie d'empilement. Cela évite la dominante verte bien connue causée par le filtre de Bayer en imagerie couleur.

# Et ensuite ?
C'est la fin du pré-traitement avec une image propre, mais encore linéaire. Ne vous étonnez pas si elle est encore toute noire, c'est normal ! Il reste à [réaliser le traitement](../tuto-scripts/#on-va-maintenant-récupérer-limage-résultante-de-lempilement), mais là on déborde de ce tuto. :-) 
