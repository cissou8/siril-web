---
title: Intégration Starnet++
author: Adrian Knagg-Baugh
show_author: true
featured_image: PMwindow.png
rate: "2/5"
type: page
---

Ce tutoriel vise à présenter l'utilisation de la nouvelle intégration de Starnet++ disponible depuis Siril 1.2.0.
Cet outil répond aux besoins des utilisateurs de pouvoir manipuler des images provenant de Starnet++ avec Siril, notamment Pixel Math, l'outil de recombinaison d'étoiles et les outils d'étoiles synthétiques.

{{< table_of_contents >}}

# Nouvelle intégration Siril
Dans les versions de Siril antérieures à la 1.2.0, la fonctionnalité Starnet++ n'était pas intégrée. Un script python était disponible et couvert ici dans un précédent tutoriel (supprimé depuis), mais il impliquait de multiples dépendances supplémentaires, alors que la nouvelle interface intégrée dans la version 1.2.0 n'a pas de dépendances supplémentaires, sauf qu'il faut que Siril soit compilé avec le support de libtiff (c'est la valeur par défaut, et si vous utilisez des paquets, votre distribution l'aura presque certainement construit de cette façon) et une installation fonctionnelle de Starnet++. Les versions v1 et v2 sont toutes deux supportées.

# Processus Starnet++
Les images issues du prétraitement Siril sont généralement au format standard FITS/32bit (bien que dans certaines circonstances, elles puissent également être au format FITS/16bit).
Cependant, les images utilisables par Starnet++ doivent être au format TIFF/16bit. Elles doivent donc être converties.

De même, les images de sortie de Starnet++ sont au format TIFF/16bit. Si vous voulez bénéficier de la précision de calcul de Siril, vous devez les convertir en FITS/32bit.
Ce format est d'ailleurs le seul supporté par l'outil PixelMath de Siril.
La nouvelle interface Starnet++ gère automatiquement toutes les conversions de formats d'images nécessaires. À moins que la préférence "Forcer 16 bits" ne soit activée, elle convertira la sortie en 32 bits afin d'être prête pour des calculs de précision accrue lors de traitements ultérieurs.

Les mots clés `FILTER` de l'entête de l'image FITS de sortie sont réglés sur Starless et StarMask pour les fichiers de sortie respectifs.

# Quand supprimer les étoiles
Starnet++ préfère largement que la fonction de transfert des tons moyens (MTF) soit appliquée à son entrée - je suppose que c'est ainsi que les données sur lesquelles il a été formé ont été préparées. Pour cette raison **il est fortement recommandé d'utiliser Starnet++ avant d'effectuer tout étirement personnalisé, et de cocher l'option "Pre-stretch Linear Image "**.

_Il existe une exception détaillée sur le [site Web de Starnet++](https://www.starnetastro.com/tips-tricks/) : si vous avez des difficultés à faire disparaître des parties de nébuleuses avec les étoiles, vous pouvez obtenir de meilleurs résultats en appliquant progressivement un étirement MTF (histogramme) différent et en désactivant l'option de pré-étirement dans la boîte de dialogue de Starnet++. Notez que dans ce cas, si vous avez d'autres traitements linéaires à effectuer, vous devrez noter les valeurs des curseurs MTF lo, mid et hi et appliquer manuellement l'étirement inverse en utilisant la commande de console Siril `invmtf`._

Le site Web de Starnet++ indique également que si vous traitez des images en couleur, il est préférable de supprimer les étoiles de l'image en couleur plutôt que de diviser l'image en canaux et de supprimer les étoiles de chacun d'eux. Il est donc recommandé, si l'on traite des images mono, de les composer en couleurs RVB avant d'exécuter Starnet++.

# Interface utilisateur Starnet++ et options

L'interface utilisateur de Starnet++ est présentée ci-dessous. Notez que le contrôle de saisie du pas n'est affiché que lorsque `Utiliser un pas personnalisé` est coché : il n'est pas coché par défaut.

{{<figure src="dialog.png" link="dialog.png" caption="Interface graphique de Starnet++">}}

Starnet++ peut également être appelé depuis la console ou les scripts en utilisant la syntaxe de commande suivante :

```
starnet [-stretch]
        [-upscale]
        [-stride=value]
        [-nostarmask]
```
Les options fonctionnent comme suit :

- `-stride` Starnet++ a une option pour définir le pas utilisé pour diviser l'image en morceaux pour le traitement. Réduire le pas augmente considérablement le temps de traitement et l'auteur de Starnet++ recommande fortement de le laisser par défaut. Néanmoins, l'option est disponible.
- `-upscale` Il y a une option pour augmenter l'échelle de l'image d'entrée d'un facteur 2. Ceci est fourni suite à des conseils sur le site web de Starnet++ indiquant que l'augmentation de l'échelle peut aider si des problèmes sont rencontrés en supprimant les étoiles qui couvrent un très petit nombre de pixels. L'option est désactivée par défaut.
- L'option par défaut est de générer un masque d'étoiles. Il s'agit simplement de la différence entre l'image d'entrée et la sortie sans étoiles, mais elle est extrêmement utile dans le cadre d'un flux de travail de réduction des étoiles où vous souhaitez étirer le fond de ciel séparément des étoiles, mais ajouter les étoiles plus tard. Si un masque d'étoile n'est pas nécessaire, la case peut être décochée dans l'interface graphique, ou l'option `-nostarmask` passée à la commande.
- Siril offre un autre outil, `Star Recomposition`, pour simplifier le processus d'étirement des images sans étoile et avec masque d'étoile indépendamment. L'interface graphique de Starnet++ fournit une option pour lancer cet outil à la fin du processus Starnet++. L'activation de cette option permet de générer automatiquement un masque d'étoile. Il n'y a pas d'option de commande correspondante car il s'agit d'un outil purement graphique.

# Les fichiers de sortie 
A la fin du processus, l'image sans étoile est sauvegardée dans le répertoire de travail avec le même nom que le fichier original, avec le préfixe _starless ajouté. L'image sans étoile est chargée comme image de travail courante dans Siril. Le masque d'étoile est enregistré dans le répertoire de travail actuel avec le préfixe _starmask ajouté au nom du fichier original.

{{<figure src="starless.png" link="starless.png" caption="Exemple du fichier de sortie starless correspondant">}}

{{<figure src="starmask.png" link="starmask.png" caption="Exemple de fichier de sortie avec masque d'étoile correspondant">}}

# Recombinaison d'étoiles
Introduit dans la version 1.2.0 en même temps que l'intégration de Starnet++, l'outil de recombinaison d'étoiles permet de fusionner deux images distinctes, chacune ayant des étirements différents appliqués. En théorie, il peut être utilisé pour fusionner deux images quelconques, mais l'objectif premier de la conception était de prendre en charge les flux de travail impliquant le traitement séparé de la couche sans étoile et du masque d'étoile.

L'outil peut être chargé de deux façons : il est accessible par le menu Traitement d'image, et la boîte de dialogue Starnet++ comporte également une option permettant de le lancer directement à la fin de Starnet++.

{{<figure src="recombination.png" link="recombination.png" caption="L'interface utilisateur de l'outil de recombinaison des étoiles">}}

Le dialogue se présente sous la forme de deux colonnes, chacune comportant un ensemble de commandes d'étirement hyperbolique généralisé (EHG) et un affichage de mini histogrammes. Les mini-histogrammes affichent également un graphique de la fonction de transfert de chaque étirement, et peuvent être réduits en cliquant sur la flèche à côté de leur titre. Les histogrammes peuvent être basculés entre les modes linéaire et logarithmique et obéissent à la préférence globale de l'interface utilisateur pour le mode d'histogramme par défaut. L'outil de recombinaison ne permet pas d'étirer les canaux séparément en raison des contraintes d'espace dans l'interface utilisateur. Si cela est souhaité, les canaux peuvent toujours être étirés séparément en utilisant l'interface principale de l'outil d'étirement hyperbolique généralisé accessible par le menu `Traitement de l'image`, puis en chargeant le résultat étiré dans l'outil de recombinaison des étoiles.

{{<figure src="setting-stretches.png" link="setting-stretches.png" caption="Applying stretch settings">}}

Comme l'étendue des contrôles de l'étirement hyperbolique généralisé est assez étendue, certains ont été cachés par défaut. Ils peuvent être affichés en cliquant sur le bouton `Avancé`. Les valeurs par défaut ont été sélectionnées de manière à ce que d'excellents résultats puissent être obtenus à l'aide d'un ensemble simple de commandes. Les détails de tous les contrôles peuvent être trouvés dans la page principale du tutoriel sur l'étirement hyperbolique généralisé. _(à venir !)_

{{<figure src="advanced.png" link="advanced.png" caption="Showing all settings">}}

Il existe deux contrôles supplémentaires au bas de la boîte de dialogue qui affectent l'équilibre général de la sortie entre les deux images d'entrée.
- Le premier est l'outil crossfader. Par analogie avec l'outil crossfader d'une table de mixage, cet outil définit simplement un mélange linéaire des deux images d'entrée. Tout à gauche signifie que seule l'image de gauche contribuera à l'image de sortie, tout à droite signifie que seule l'image de droite contribuera à l'image de sortie, et centré signifie que chaque image d'entrée aura une pondération égale dans la sortie.
- Le deuxième outil est l'outil d'étirement final. En général, l'utilisateur peut définir un étirement pour l'arrière-plan, puis utiliser le crossfader pour augmenter la contribution des étoiles - cependant, ce faisant, la luminosité de l'arrière-plan diminue. Il est possible de remédier à ce problème en appliquant un étirement final léger à l'ensemble de l'image pour renforcer les détails de l'arrière-plan tout en empêchant l'éclatement des étoiles brillantes.

# Qu'en est-il de PixelMath ?
L'outil [PixelMath](../pixelmath) récemment introduit dans Siril permet de recombiner les images sans étoile et avec masque d'étoile d'une manière différente. Les deux fichiers peuvent être chargés dans PixelMath et combinés en utilisant la les opérations standard de PixelMath de Siril. Contrairement à l'outil de recombinaison des étoiles, PixelMath offre la possibilité d'être scriptée en utilisant le jeu de commandes de Siril. Les détails complets de PixelMath peuvent être trouvés dans la page principale du tutoriel PixelMath.

# Mes étoiles sont terribles, puis-je sauvegarder mon image ?
Bien sûr ! Siril propose des [outils d'étoiles synthétiques](../synthetic-stars) qui peuvent vous aider. Si vos étoiles sont mauvaises mais que, pour une raison quelconque, il est important de sauvegarder l'image plutôt que de revoir la cible, les outils d'étoiles synthétiques peuvent être utilisés conjointement avec Starnet++ pour les remplacer. Starnet++ est utilisé pour produire une image sans étoiles, et l'outil d'étoiles synthétiques est utilisé pour générer un masque d'étoiles synthétique en utilisant les paramètres des étoiles réellement mesurés à partir de votre image. Le masque d'étoiles synthétique peut ensuite être recombiné avec l'image sans étoiles à l'aide de l'outil de recombinaison des étoiles ou de PixelMath, comme décrit ci-dessus.





C'est à vous de jouer ! !!
