---
title: Transformations d'Étirement Hyperboliques Généralisées (GHS)
author: David Payne & Mike Cranfield
show_author: true
featured_image: PMwindow.png
rate: "3/5"
type: page
---

Après avoir intégré vos données, l'étirement est essentiel pour faire sortir votre image de l'obscurité et appliquer le contraste nécessaire pour révéler les détails. Alors que Siril dispose d'outils d'étirement plus simples, GHS offre une plus grande liberté pour révéler et mettre en évidence exactement ce que vous voulez mettre en avant, dans n'importe quelle image astronomique.

{{< table_of_contents >}}

# Comment les paramètres GHS (curseurs) modifient l'image et son histogramme

Ce tutorial sera plus compréhensible si parallèlement à la lecture, vous chargez une image que vous avez traité, ou toute image déjà étirée que vous pouvez lire dans Siril. Il n'est pas nécessaire qu'elle soit monochrome, mais cela facilite le suivi des modifications de l'histogramme. Il n'est pas nécessaire qu'elle soit sans étoiles ou même une image astronomique - mais elle doit contenir au moins une luminosité de niveau moyen. Voici mon image, qui représente le canal de luminance traité de NGC2264 - l'amas de l'arbre de Noël (sauf, bien sûr, sans l'amas d'étoiles lui-même).

{{<figure src="NGC2264.png" link="NGC2264.png" caption="" >}}

Une fois votre image chargée, allez-y et sélectionnez Image `Traitement -> Transformations d'étirement hyperboliques généralisées` dans le menu déroulant et Siril affichera l'histogramme associé à l'image avec les commandes GHS sur le côté droit :

{{<figure src="GHS_window.png" link="GHS_window.png" caption="" >}}

L'histogramme montre la répartition de la luminosité des pixels dans l'image, avec les pixels les plus sombres à gauche et les plus clairs à droite. La hauteur de l'histogramme représente le nombre de pixels d'une luminosité donnée. L'utilisation de l'histogramme conjointement avec l'image vous offrira le meilleur guide d'utilisation du GHS, donc si vous avez besoin d'aide pour comprendre les histogrammes, veuillez vous référer à la mine d'informations qui existe en ligne.
Je recommande fortement, pour régler une image déjà non linéaire, de sélectionner la case "Échelle logarithmique", afin que les zones de luminosité avec moins de pixels soient plus facilement visibles. Voici ma vue à l'échelle logarithmique de la même image :

{{<figure src="GHS_window2.png" link="GHS_window2.png" caption="" >}}

La magie de la vue logarithmique de l'histogramme, c'est qu'elle peut vous guider dans vos ajustements d'étirement.
 
À mon avis, les meilleures images ont cette forme de ligne presque droite du sommet (point le plus haut) vers la droite et c'est le genre de forme que nous devrions cibler avec nos étirements. La plupart des bosses ou des vallées de cette pente indiquent où le contraste doit être redistribué (ajouté pour les bosses et supprimé des vallées). L'endroit où se trouve le pic détermine la luminosité de l'arrière-plan et sa largeur détermine la quantité de caractéristiques sombres apparentes (y compris le bruit de fond, malheureusement). En général, l'histogramme doit couvrir la majeure partie de la plage de luminosité pour profiter de toute la plage de niveaux de luminosité de votre support d'affichage. Si les pixels sont trop étirés, ils se regrouperont sur le côté droit de l'histogramme, provoquant une saturation des pixels et un "gonflement des étoiles". Dans ce cas, j'ai volontairement gardé un espace sur le côté droit pour accueillir les étoiles qui seront ajoutées plus tard à cette image. À l'extrême gauche se trouvent les pixels les plus sombres, passant des pixels les plus sombres avec peu ou pas de signal (juste du bruit) avec un signal croissant jusqu'au pic de l'histogramme.

Si vous remarquez une ligne rouge traversant l'histogramme en diagonale du bas à gauche vers le haut à droite, il s'agit d'un tracé de la transformation réelle déterminée par les cinq principaux paramètres d'entrée du GHS. La ligne diagonale représente la transformation "sans changement" ou transformation identité. Lorsque l'outil est démarré, le `Facteur d'étirement (ln (D + 1))` ou simplement `D` en abrégé, est défini sur 0, ce qui ne représente aucun étirement et donc aucun changement de l'image ou de l'histogramme. L'importance de cette ligne diagonale en tant que guide deviendra évidente dans un instant, mais pour les besoins de ce didacticiel, veuillez garder vos yeux fixés sur l'affichage de l'image, l'histogramme et les lignes/courbes rouges qui se transforment simultanément.

Au départ, je voudrais que vous déplaciez d'abord le curseur `Point de symétrie (SP)` à environ 0.5 (pas besoin d'être précis). Cela peut également être fait en cliquant sur le tracé de l'histogramme à mi-chemin. Ensuite, j'aimerais que vous déplaciez graduellement le curseur du haut, `D` jusqu'à environ 2. Tout d'un coup, vous verrez qu'il se passe beaucoup de choses ! - avec la courbe de transformation rouge, l'histogramme et l'image elle-même. Je vous encourage à déplacer `D` d'avant en arrière et à jouer avec pour avoir une idée de ce qu'il fait aux trois.

Voici mon histogramme et mon image avec un `D` de `2` et `SP=0.5` appliqué :

{{<figure src="GHS_window3.png" clink="GHS_window3.png" aption="" >}}

{{<figure src="NGC2264_2.png" link="NGC2264_2.png" caption="" >}}

Après avoir joué un peu avec `D`, nous pouvons avoir une idée de ce que fait la transformation.

L'image montre que nous avons injecté beaucoup de contraste dans les tons moyens tout en éclaircissant les clairs et en assombrissant les sombres - et nous l'avons clairement exagéré dans l'image. Vous pouvez voir sur l'histogramme que sur le côté gauche, l'histogramme a été décalé assez loin vers la gauche, tandis que sur le côté droit, l'histogramme a été décalé assez loin vers la droite. Il y a maintenant beaucoup moins de pixels au milieu, représentant les tons moyens - nous pouvons donc relier la forme de l'histogramme à l'effet sur l'image. En effet, l'histogramme (vue logarithmique) est devenu une vallée à large base qui, ce qui n'est généralement pas esthétiquement attrayant car il représente un contraste trop dur.

Il est également utile de voir ce que la transformation (courbe rouge) nous dit. La clé pour comprendre la courbe de transformation est de la relier à la ligne d'identité que j'ai soulignée plus tôt. Là où la courbe de transformation se situe au-dessus de la ligne d'identité, les pixels sont éclaircis et l'histogramme décalé vers la droite. Là où la courbe de transformation se situe sous la ligne d'identité, les pixels sont atténués et l'histogramme décalé vers la gauche. La pente ou la pente de la transformation nous indique où nous mettons le contraste. Encore une fois, en reliant cela à la ligne d'identité - le contraste est ajouté dans les tons moyens où la transformation est plus raide que la ligne d'identité, et supprimé. Sur le côté gauche et le côté droit, la ligne de transformation est moins profonde ou plus plate que la ligne d'identité et le contraste est en fait supprimé de ces zones. La courbe globale elle-même représente une forme en S, que vous pouvez avoir l'habitude de faire pour ajouter du contraste aux tons moyens de votre image.

Une fois que vous vous êtes installé sur un `D` d'environ 2 (en laissant `SP` autour de 0.5), vous trouverez peut-être utile de changer le `Type d'étirement` en `Transformée hyperbolique généralisée inverse` et de faire une pause pour examiner les résultats. Voici les miens :

{{<figure src="NGC2264_D2.png" link="NGC2264_D2.png" caption="" >}}

{{<figure src="GHS_window4.png" link="GHS_window4.png" caption="" >}}

Vous pouvez voir que nous avons fait l'inverse avec la transformation inversée (maintenant une forme en S inversé) en ajoutant du contraste sur les extrémités droites et gauches, en éclaircissant les basses lumières, en atténuant les hautes lumières et en comprimant l'histogramme au milieu. L'histogramme est un peu bombé à droite du pic, ce qui donne à l'image elle-même un aspect assez plat et un besoin urgent de plus de contraste au milieu.

Revenez à `Étirement hyperbolique généralisée` et regardons ce curseur `Point de symétrie (SP)` tout en laissant le paramètre `D` à `2` pour le moment. Le paramètre `SP` contrôle en fait le niveau de luminosité auquel le contraste maximum sera ajouté. Jusqu'à présent, nous avons défini cela comme étant le milieu de l'histogramme, ou les tons moyens de l'image. Faites glisser `SP` vers la gauche ou vers la droite pour voir comment la courbe de transformation rouge change et ce que cela fait à la fois à l'histogramme et à l'image.

En général, là où `SP` se trouve, la transformation supprimera les bosses et commencera à créer des vallées en ajoutant du contraste, et loin de `SP`, la transformation aplatira toutes les vallées et créera potentiellement des bosses en supprimant le contraste. La taille de l'effet dépendra du niveau de `D`.

Le processus d'affinement de mon étirement, pour moi, devient un processus de recherche de bosses et de vallées locales dans mon image non linéaire et de faire de l'histogramme logarithme une ligne plus droite à droite du pic de mon histogramme. Dans le cas de notre exemple actuel. Je remarque une petite bosse à base large juste à droite du pic de mon histogramme. Je peux placer `SP` (où le contraste maximal sera ajouté) juste au-dessus du pic et ajuster `D` jusqu'à ce que mon histogramme soit plus droit - comme indiqué ici :

{{<figure src="GHS_window5.png" link="GHS_window5.png" caption="" >}}

Et voici mon aperçu de l'image.

{{<figure src="NGC2264_3.png" link="NGC2264_3.png" caption="" >}}

Maintenant, j'aime personnellement les tons moyens de cette image plus que l'original, et je suis d'accord qu'il y a un meilleur contraste dans les tons moyens, mais les brillants sont devenus un peu trop brillants, tandis que du côté sombre, je ne peux pas vraiment voir le même niveau de détails que l'original.

C'est à ce stade que nous pouvons explorer et utiliser les deux paramètres suivants de la transformation, à savoir le `point de protection de l'ombre (LP)` et le `point de protection des hautes lumières (HP)`. Ces deux paramètres fonctionnent essentiellement de la même manière, mais à partir de côtés opposés de l'histogramme.

Pour voir comment cela fonctionne, réinitialisez votre image à un `SP = 0.5` et `D = 2` et, dans un premier temps, augmentez `LP` de son `0` initial vers `SP` (notez que `LP` doit être `<= SP`). Vous verrez les foncés se rééclaircir vers leur valeur initiale. En même temps, vous pouvez voir la transformation revenir vers la ligne d'identité. Au point où la transformation se trouve juste au-dessus de la ligne d'identité, vous avez laissé tous les pixels inférieurs à `LP` inchangés ! Cependant, vous pouvez continuer et éclaircir les pixels les plus sombres même au-dessus de leur niveau initial si vous le souhaitez.

`HP` fait le contraire et son niveau initial se situe à `1.0`. Déplacez `HP` vers le bas pour assombrir les hautes lumières en protégeant le contraste dans le haut de la gamme. En pratique, j'utilise presque toujours un certain niveau de `HP` afin d'empêcher les parties les plus brillantes de la nébulosité, des noyaux galactiques et des étoiles elles-mêmes de saturer. À l'aide des curseurs, jouez avec la transformation et notez les changements dans l'image et l'histogramme. Essayez d'ajuster `LP` et/ou `HP` pour placer une partie de la transformation juste au-dessus de la ligne d'identité et observez que cela laisse ces pixels inchangés.

Pour notre exemple, j'ai maintenant utilisé `LP` et `HP` pour réduire (protéger) l'assombrissement des basses lumières et l'éclaircissement des hautes lumières, tout en ajoutant juste assez d'extension pour que mon histogramme soit plus rectiligne.

{{<figure src="GHS_window6.png" link="GHS_window6.png" caption="" >}}

{{<figure src="NGC2264_4.png" link="NGC2264_4.png" caption="" >}}

Maintenant, il y a un dernier paramètre important à discuter, et c'est `l'intensité d'étirement local` ou `b`.

Le paramètre `b` essentiel détermine la concentration ou le foyer de l'ajout de contraste autour de `SP`. Pour illustrer cela, appuyez sur le bouton de réinitialisation en bas à droite de la fenêtre de transformation - cela ramènera tous les paramètres à `0`, sauf HP, qui a sa valeur par défaut à 1. Encore une fois, réglez `D` sur environ `2`, et `SP` quelque part dans le milieu de l'histogramme.

Vous devriez voir la nature de la transformation SGH changer au fur et à mesure que b augmente de 0 à 1 et jusqu'à un maximum de 15. A `b=0`, la transformation enlève le contraste de l'endroit le plus éloigné de `SP` et le met dans le voisinage de `SP`. Lorsque `b` est augmenté au-delà de `b=1`, les zones les plus éloignées sont moins touchées et le contraste de la transformation est ajouté directement au point `SP` - au détriment des distances modérées de `SP`. En fait, l'ajout de contraste est concentré ou focalisé sur le point `SP`.

L'essentiel de l'utilisation de `b` dans la finalisation des étirements est que `b` doit être orienté vers la "largeur" de la région de l'histogramme que vous souhaitez modifier. Par exemple, pour se débarrasser d'un pic ou d'une bosse étroite dans l'histogramme, il faut utiliser un facteur `b` important pour concentrer l'étirement et pour les bosses larges, il faut utiliser un facteur `b` plus faible. En général, j'utilise un facteur `b` compris entre 2 et 6 pour ajuster l'étirement initial. Dans la pratique, il se peut que vous deviez également ajuster `D` de manière itérative, et même `SP` quelque peu au fur et à mesure que b est modifié pour trouver l'endroit qui donne le meilleur résultat. Notez que nous devons éviter de créer une vallée profonde ou une "bifurcation" de l'histogramme, ce qui n'est généralement pas beau à voir.

Bien que l'utilisation de `b` puisse être quelque peu obscure pour l'utilisateur novice du GHS, elle viendra au fur et à mesure que vous l'essayerez. Certains changements spectaculaires dans la nature de votre image peuvent être réalisés grâce à sa mise en œuvre. Par exemple, en utilisant un étirement avec un grand `b` et un `SP` réglé sur le pic de l'histogramme représentant l'arrière-plan de l'image, des détails de nébulosité faible ou de FIN peuvent être révélés, comme l'étirement suivant sur l'exemple que j'ai utilisé.

{{<figure src="GHS_window7.png" link="GHS_window7.png" caption="" >}}

{{<figure src="NGC2264_5.png" link="NGC2264_5.png" caption="" >}}

Maintenant, est-ce que ce rendu est "meilleur" que le précédent ? Il est certainement différent par nature. De cette manière, la conception de l'image finale, par le biais d'ajustements des cinq paramètres du GHS, peut vous permettre de mettre en évidence les observations scientifiques que vous souhaitez ou de faire ressortir votre côté artistique.

Vous noterez que les valeurs négatives de `b` sont également autorisées dans le cadre de la transformation. Lorsqu'elles sont négatives, les équations du GHS deviennent logarithmiques par nature et un `b=-1.4` reproduit en fait fidèlement les résultats obtenus en utilisant un étirement "arcsinh". Un `b` négatif est idéal lorsqu'une augmentation ou une diminution de la luminosité globale est souhaitée sans changer radicalement la distribution du contraste dans l'image. En outre, un `b` négatif fonctionne bien pour ajuster les niveaux de saturation des couleurs.

Les équations de la transformation GHS et ses propriétés mathématiques signifient que les étirements peuvent être appliqués de nombreuses fois pour effectuer des ajustements illimités aux images sans ajouter de bruit ou d'artefacts aux données, du moins dans les limites de la précision numérique offerte par l'image. Il n'y a pas d'écrêtage des données. Si vous n'aimez pas ce que vous avez fait la dernière fois, vous pouvez l'annuler, inverser l'étirement ou effectuer encore plus d'étirements pour réparer.

## Effectuer des étirements initiaux à l'aide de GHS
Le premier défi de l'étirement d'une image linéaire nouvellement empilée et principalement sombre est que vous ne savez pas vraiment ce que vous avez jusqu'à ce que l'étirement soit effectué. Par exemple, voici la forme linéaire de l'image que je vais utiliser pour démontrer l'étirement primaire.

{{<figure src="NGC2264_6.png" link="NGC2264_6.png" caption="" >}}

Je suis sûr que vous ne le reconnaîtrez pas, mais il est basé sur les mêmes données que l'exemple précédent, sauf que cet ensemble de données est antérieur à la suppression des étoiles - pour rendre l'étirement plus difficile, car souvent il n'est ni possible, ni souhaitable de supprimer les objets les plus brillants de l'image tels que les étoiles ou les noyaux galactiques. Les points brillants que vous voyez sur l'image sont en fait les noyaux des étoiles les plus brillantes de l'image.

Dans la vue linéaire de l'histogramme, vous pouvez voir que l'écrasante majorité des pixels sont loin à gauche de l'histogramme entier - c'est pourquoi l'image linéaire ne peut pas être vue. C'est là que se trouve la quasi-totalité des données, c'est-à-dire la nébulosité, le fond et le bruit. La vue logarithmique, cependant, montre qu'il y a quelques pixels clés, représentant les étoiles à travers l'histogramme et ce sont ces pixels que l'on peut distinguer dans l'image linéaire.

{{<figure src="GHS_window8.png" link="GHS_window8.png" caption="" >}}
{{<figure src="GHS_window9.png" link="GHS_window9.png" caption="" >}}

D'une manière ou d'une autre, nous devons éclaircir et ajouter beaucoup de contraste (étaler) le pic de l'histogramme dans la vue linéaire, sans entasser tous les pixels de l'étoile contre le côté droit de l'histogramme, de sorte qu'ils ne soient plus que des taches blanches. Cela doit être fait de manière non linéaire (en langage mathématique, cela signifie "courbe"), en utilisant une transformation courbe pour y parvenir.

La toute première opération que vous faites actuellement est probablement une simple transformation d'histogramme (non linéaire) pour mieux voir ce que vous avez comme données. Voici la vue zoomée - non linéaire - de la fonction automatique de transformation d'histogramme que le module propose pour l'étirement. Le zoom sur le côté gauche de l'histogramme est effectué pour mieux voir ce qui se passe près de cette partie importante de l'histogramme linéaire.

{{<figure src="GHS_window10.png" link="GHS_window10.png" caption="" >}}

L'application de la transformation automatique de l'histogramme est en fait un processus en deux étapes :

1. L'histogramme est renormalisé via un étirement linéaire et les parties sur les espaces à gauche des données et à droite de l'histogramme sont coupées pour augmenter le contraste avec l'image.
2. Un étirement non linéaire (harmonique) est appliqué (équivalent d'un étirement GHS avec `SP = 0` et `b = 1`) avec une quantité d'étirement (équivalent GHS D) définie sur la valeur qui place le nouveau pic d'histogramme à environ un quart du chemin à travers le nouvel histogramme.

Voici l'aperçu résultant et la vue logarithmique agrandie de l'histogramme.

{{<figure src="NGC2264_7.png" link="NGC2264_7.png" caption="" >}}

{{<figure src="GHS_window11.png" link="GHS_window11.png" caption="" >}}

C'est un travail d'étirement raisonnable. Il y a une certaine perte de définition des étoiles et un léger gonflement des étoiles dans l'image, comme en témoignent les pixels qui commencent à se rassembler sur la droite. De même, les parties les plus sombres de la nébulosité sont en grande partie perdues. Je pense qu'avec un peu de pratique et quelques étapes supplémentaires, mais similaires, GHS peut obtenir un bien meilleur résultat. Revenons donc à l'image linéaire et je vais montrer la recette modifiée pour les étirements initiaux.

1. Réglez `SP` et `b` : Ouvrez l'outil GHS et faites un zoom sur le côté gauche de l'histogramme - vue linéaire, zoom avant jusqu'à 100x. Réglez d'abord `SP`. Ce que vous voulez, c'est que `SP` se trouve dans le pic visible de l'histogramme lui-même, mais juste à gauche du point le plus élevé. Rappelez-vous que c'est le point auquel GHS donnera le contraste maximum (plutôt qu'à 0 comme avec le processus HT-auto). La meilleure façon de procéder est de cliquer sur le tracé de l'histogramme lui-même, ou d'utiliser le bouton "pipette" pour cliquer sur une partie sombre de l'image. `SP` doit être une très petite valeur, mais pas 0. Ensuite, à l'aide du curseur, fixez une valeur initiale de `b` à 15. Cela rendra la transformation super harmonique et concentrera la plus grande quantité de contraste à `SP` - juste là où nous le voulons. Vous ne verrez pas encore de changement dans l'image, car la valeur d'étirement, `D`, est toujours fixée à 0.

{{<figure src="GHS_window12.png" link="GHS_window12.png" caption="" >}}

2. Appliquez `D` et ajustez : Faites un zoom arrière sur l'histogramme, et vérifiez la vue logarithmique de l'histogramme. Une fois que vous pouvez clairement voir à la fois l'histogramme et l'aperçu de l'image, gardez l'œil sur les deux tout en utilisant le curseur pour augmenter `D`. Continuez à augmenter `D` jusqu'à ce que vous puissiez juste distinguer les détails de l'arrière-plan. et vérifiez que l'un des éléments suivants se produit - notez que rien de mauvais n'arrive à votre image/données, juste qu'il y a des ajustements qui rendront votre image meilleure. L'une des beautés de GHS est qu'il est possible de réparer ou d'améliorer à peu près tout avec d'autres valeurs.

   + L'arrière-plan, probablement avec du bruit, devient trop lumineux, tout comme le sujet, ou bien vous laissez la plus faible nébulosité derrière vous en arrière-plan. Cela indique que vous devez réajuster `SP` - dans le premier cas, vous voudrez augmenter `SP` et dans le second, le diminuer. Cela peut être fait par la saisie manuelle dans la zone de texte `SP` ou l'ajusteur fin +/-, mais je trouve que c'est mieux de le faire en cliquant sur l'image elle-même avec l'outil pipette. L'idéal est de placer `SP` là où se trouvent les éléments les plus sombres que vous voulez voir, mais au-dessus du niveau de bruit ou des éléments que vous voulez cacher. Je vous recommande, même si votre image semble correcte, d'explorer un peu plus `SP`.

   + Vous commencez à bifurquer (créer deux pics proéminents) lorsque `D` est augmenté. C'est une indication que vous ajoutez trop de contraste, peut-être au mauvais endroit. Si c'est le cas, vous devez réduire `b` et `D` à un niveau inférieur, juste à l'endroit où la bifurcation se produit. Cela atténuera à nouveau l'image et vous devrez peut-être faire plusieurs étirements "initiaux" (ce qui est ma norme de toute façon). L'étape suivante consiste à ajuster `SP`. La bifurcation peut se produire si la valeur `SP` est trop élevée ou trop basse, ou même si elle est juste correcte, mais il est préférable de vérifier. Si l'étirement ne peut pas être amélioré en ajustant `SP`, alors laissez `D` et `b` au niveau réduit.

   + Les pixels commencent à former un grand pic secondaire sur la droite de l'histogramme (le début du gonflement et de la définition des étoiles dans les objets les plus brillants). Si cela se produit, alors `D` doit être réduit. Vous pouvez également appliquer un `HP < 1`, ce qui aidera, mais je préfère réduire `D` et traiter le problème avec un étirement ultérieur.

   + Grain / bruit excessif dans les éléments sombres. Normalement, je fais un ajustement du point noir comme deuxième étape de l'étirement "initial", mais si l'écart entre `0` et le côté gauche de l'image est trop important pour commencer, vous pouvez faire l'étape 2 avant cette étape 1.

   + Tout semble correct. Vous avez peut-être la chance de pouvoir vous passer d'un seul étirement "initial". Néanmoins, vous pouvez souhaiter ne pas amener l'image à la luminosité maximale souhaitée à ce stade - êtes-vous sûr de ne pas manquer quelque chose ? Ma recommandation de base est de faire en sorte que le processus d'étirement initial soit au moins une combinaison de deux grandes étapes. Je m'arrêterais donc là où vous pouvez voir la majeure partie de votre sujet, même s'il est considérablement plus sombre que vous ne le souhaiteriez.

Voici mon résultat après l'étape 1, tant au niveau de l'histogramme que de l'image de prévisualisation. Il est évident qu'un peu plus d'étirement doit être fait dans ce cas, ce sont les étoiles (sous 1C), ci-dessus) qui ont limité la quantité que je pouvais augmenter l'étirement.

{{<figure src="GHS_window13.png" link="GHS_window13.png" caption="" >}}

{{<figure src="NGC2264_8.png" link="NGC2264_8.png" caption="" >}}

3. Exécutez l'étirement
4. Définir et appliquer un décalage linéaire du point noir

C'est généralement après un étirement initial que j'aime employer un étirement linéaire pour réinitialiser le point noir, car je n'ai pas à gérer un histogramme zoomé. Il suffit de changer le `type d'étirement` en `étirement linéaire` et vous verrez qu'une seule entrée est nécessaire - le point noir que vous désirez. Avec l'histogramme réglé sur logarithmique, je clique sur le tracé de l'histogramme à gauche, mais aussi près que possible du début de la montée de l'histogramme. Ne vous souciez pas trop d'être extrêmement proche, mais si vous empiétez sur les données elles-mêmes, vous allez écrêter et perdre certaines de vos données durement gagnées - mais peut-être que c'est juste du bruit ou que vous recherchez un effet. En pratique, je n'essaie jamais d'écrêter les données originales. Voici l'histogramme de mon aperçu.

{{<figure src="GHS_window14.png" link="GHS_window14.png" caption="" >}}

Notez que cet exercice va redimensionner quelque peu votre image, mais ne vous en préoccupez pas pour l'instant. Lorsque vous êtes satisfait, cliquez sur Appliquer.

5. Appliquer la deuxième transformation GHS. Il s'agit essentiellement d'une répétition des étapes 1 à 3. Cependant, dans ce cas, il n'est pas nécessaire de zoomer sur l'histogramme initial, et il y a beaucoup plus de latitude pour l'ajustement des paramètres. Une fois de plus, faites attention aux mêmes choses que celles notées au point 2) ci-dessus en utilisant les touches
   + `D` pour ajuster la quantité d'étirement
   + Plus haut `b` pour étaler le pic de l'histogramme et laisser de la définition dans les étoiles
   + `SP` généralement dans le pic de l'histogramme, mais variable pour ajuster la luminosité relative des composants du sujet.
   + `LP` et `HP` pour protéger davantage les points faibles et les points forts (étoiles, noyaux galactiques, bords lumineux des colonnes, etc.) et contrôler la luminosité générale de l'image.

Voici l'aperçu et l'histogramme de ma deuxième application de GHS

{{<figure src="GHS_window15.png" link="GHS_window15.png" caption="" >}}

Lorsque vous êtes prêt, n'oubliez pas d'appliquer l'étirement. Vous constaterez peut-être que vous devez répéter l'étape 5 une ou deux fois de plus et c'est très bien. Une fois terminé, vous pouvez procéder à d'autres processus non linéaires, entrecoupés d'ajustements d'étirement de suivi.

{{<figure src="NGC2264_9.png" link="NGC2264_9.png" caption="" >}}

Je crois qu'avec GHS, vous pouvez finalement obtenir une meilleure distribution de contraste qu'avec d'autres méthodes, sans corrompre les données elles-mêmes. Lorsqu'il s'agit de nébulosité, vous constaterez peut-être que la suppression des étoiles n'est pas nécessaire. De toute évidence, cette image a toujours besoin d'une redistribution du contraste à l'aide d'étirements GHS itératifs, mais c'est un excellent point de départ pour le traitement non linéaire.

# Gérer la couleur

L'étirement standard d'une image couleur se déroule de la même manière qu'un étirement monochrome, sauf qu'il y a initialement trois histogrammes représentés dans le tracé d'histogramme - bleu, vert et rouge - un pour chaque canal. Cela peut être intimidant au début, mais si vous êtes familier avec l'étirement monochrome, tout ce que vous faites est d'étirer "trois images monochromes" en même temps - seulement maintenant nous pouvons non seulement nous préoccuper du contraste et de la luminosité, mais aussi de la relation entre les trois histogrammes des canaux de couleur eux-mêmes. Heureusement, nous pouvons généralement procéder avec une approche très simple de cette relation et nous nous baserons sur cela, une fois que vous comprendrez comment cette relation peut être manipulée. Vous verrez, à la fin, que ce que l'étirement des couleurs offre est un autre degré de liberté avec lequel montrer la science et exprimer votre créativité artistique en même temps.

## Extensions de couleurs de base et améliorées

Pour illustrer l'étirement de base des couleurs, nous utiliserons une image couleur RVB du même sujet que nos images monochromes ci-dessus, mais représentant cette fois trois canaux pris à travers des filtres rouge, vert et bleu qui sont placés dans des canaux et étalonnés en couleur. Une telle image peut également provenir d'un appareil photo qui utilise des filtres pour les trois couleurs en même temps, ce qui permet d'obtenir le même résultat. 

Après la combinaison des couleurs, cette image a été étalonnée (à l'aide de l'étalonnage photométrique des couleurs, dans ce cas) sous forme linéaire pour obtenir une couleur neutre à l'arrière-plan et une couleur blanche presque moyenne pour les étoiles. Dans l'étalonnage photométrique des couleurs, les couleurs des étoiles sont en fait étalonnées et ajustées pour correspondre aux couleurs d'étoiles connues obtenues par analyse photométrique.

{{<figure src="colour_GHS_window.png" link="colour_GHS_window.png" caption="" >}}

Dans cette vue initiale de l'histogramme de notre image couleur RVB calibrée - montrée en GHS et zoomée sur l'extrême gauche de l'histogramme car cette image est encore linéaire. Il est important de noter que dans cette image calibrée, les histogrammes se chevauchent tous et que les pics de l'histogramme se trouvent tous à peu près au même endroit - c'est le résultat du calibrage photométrique. La principale différence entre les canaux de couleur semble être leur largeur.

Je note que dans ce cas, il y a initialement un grand écart (par rapport à l'histogramme lui-même) sur la gauche de l'histogramme. Pour compenser, nous allons basculer l'affichage de l'histogramme en mode logarithmique et définir un point noir pour réduire cet écart, tout en ne coupant que quelques pixels.

Vous constaterez sans doute que nous avons maintenant trois histogrammes de canaux de couleur de prévisualisation pour aller avec les trois de l'image couleur originale.

{{<figure src="colour_GHS_window1.png" link="colour_GHS_window.png1" caption="" >}}

Une fois la réinitialisation du point noir effectuée, nous pouvons procéder de la même manière pour utiliser GHS afin d'étirer l'image linéaire, comme cela a été illustré dans l'exemple monochrome. En général, nous voulons que `SP` soit placé dans l'histogramme des trois couleurs, mais à gauche de tous les pics - mais encore une fois, vous pouvez toujours voir à quoi ressemble votre image si vous ne le faites pas. 

Voici la version dézoomée de l'aperçu de l'histogramme en utilisant un grand `b` (pour un étirement initial) et suffisamment de `D` pour pouvoir voir la majeure partie de l'image.

{{<figure src="colour_GHS_window2.png" link="colour_GHS_window2.png" caption="" >}}

L'image résultante après le premier étirement nécessitera encore quelques ajustements d'étirement pour mieux montrer la nébulosité, mais elle montre la définition des étoiles. Cependant, avant de procéder à la correction, faisons une pause pour examiner les couleurs qui ressortent de l'image.

{{<figure src="colour_DSS.png" link="colour_DSS.png" caption="" >}}

Les parties de l'image qui ont été le plus éclaircies, les étoiles elles-mêmes, semblent avoir été quelque peu décolorées. C'est un phénomène courant lorsque le même étirement se transforme en histogrammes de couleurs différentes très proches les uns des autres, comme cela a été le cas ici.

Heureusement, il existe quelques alternatives à cette méthodologie. La première alternative consiste à utiliser la méthodologie que vous avez peut-être déjà vue employée par le module `Transformation asinh`. Cette méthode consiste à étirer la luminance de l'image, puis à appliquer la quantité d'étirement résultante de la luminance à chacun des canaux de couleur. L'effet de cette nouvelle méthode est d'étirer la saturation de la couleur en même temps que la luminance et peut être invoquée en sélectionnant `Luminance pondérée uniforme` à partir des commandes de transformation GHS. Vous pouvez voir comment cela a ajusté l'histogramme et fourni plus de couleur dans les étoiles :

{{<figure src="colour_GHS_window3.png" link="colour_GHS_window3.png" caption="" >}}
{{<figure src="colour_DSS2.png" link="colour_DSS2.png" caption="" >}}

Cela permet de rétablir la saturation des couleurs des étoiles ou d'autres zones lumineuses de l'image. Un résultat légèrement différent peut être obtenu en sélectionnant `Modèle d'étirement des couleurs -> Luminance simulant la vision humaine`.
 
Si le résultat de l'une ou l'autre sélection est trop important, envisagez de subdiviser votre étirement en deux plus petits, l'un utilisant la luminance pondérée et l'autre l'étirements par canaux indépendants. Si ce n'est pas suffisant, laissez le modèle d'étirement de couleur sélectionné à l'une des luminances pondérées pour l'étirement suivant.

{{<figure src="colour_GHS_window4.png" link="colour_GHS_window4.png" caption="" >}}
{{<figure src="colour_DSS3.png" link="colour_DSS3.png" caption="" >}}

Vous vous demandez peut-être "où est passée toute la nébulosité ?". Vous noterez que cette cible est principalement une nébuleuse d'émission, qui n'apparaît pas facilement dans l'imagerie à large bande, rouge-vert-bleu. Ce que vous voyez, c'est une nébuleuse de réflexion ainsi qu'une représentation précise de la teinte des étoiles.

Si vous n'aimez pas particulièrement les options de pondération de la luminance présentées ici, la saturation des couleurs peut être étirée séparément dans le module `Saturation des couleurs` de Siril. L'une ou l'autre de ces méthodes, ou une combinaison de celles-ci, peut être utilisée pour obtenir le résultat souhaité.

Les étirements suivant devront tenir compte des saturations et des teintes de couleur en plus de la distribution du contraste et de la luminosité. Cela peut être beaucoup, même pour un utilisateur expérimenté. De plus, certains processus linéaires et non linéaires peuvent facilement créer des artefacts de couleur. Pour ces raisons, il est courant d'extraire la luminance des images RVB à large bande ou à bande étroite avant l'étirement mais après l'étalonnage des couleurs ou la luminance de prise de vue avec son propre filtre. Cela permet de traiter la luminance séparément des images couleur. L'objectif est d'obtenir un contraste et une luminosité corrects dans l'image de luminance, et de concentrer la saturation et la teinte avec l'image couleur. En règle générale, dans la phase non linéaire, lorsque la majeure partie du traitement est terminée, les images de luminance et de couleur peuvent être réunies à l'aide du processus de `Composition RVB`.

## Calibrage manuel des couleurs

Les images couleur à bande étroite nécessitent aussi généralement un étalonnage des couleurs, tout comme les images RVB à large bande, bien que l'étalonnage photométrique soit beaucoup moins souvent employé. Puisque nous avons de toute façon affaire à de "fausses" couleurs, nous avons tendance à utiliser la couleur soit pour aider à révéler les informations contenues dans les données, soit à des fins esthétiques. Souvent, lorsque l'on utilise les filtres Ha, [OIII] ou O, et [SII] ou S, on a recours à la populaire "palette de Hubble", qui est une palette de couleurs assez vaguement définie où S est assigné au rouge, Ha au vert, et O au bleu, ce qui implique une manipulation de la teinte pour supprimer une partie ou la totalité de la couleur verte, bien que les principes de calibrage et d'étirement des couleurs démontrés ici soient essentiellement les mêmes. 

Quoi qu'il en soit, en général, on s'attend à ce que la couleur de fond, qu'elle soit à bande étroite ou à large bande, soit neutre et que, bien qu'une couleur puisse être dominante, les trois couleurs s'efforcent d'équilibrer l'image. Pour obtenir cette condition, les histogrammes des pixels rouges, verts et bleus doivent se chevaucher largement. Malheureusement, c'est rarement le cas lorsque les données à bande étroite sont d'abord combinées en une image RVB linéaire, comme on peut le voir dans la vue auto-étirée de l'image linéaire (avec les couleurs liées). Dans ce cas, c'est le bleu qui domine l'image, le vert ne s'exprimant que dans les hautes lumières, tandis que le rouge est presque absent.

Je m'arrêterai ici pour dire que ce problème peut être partiellement corrigé en effectuant une "extraction de gradient", mais dans certains cas, la nébulosité apparaît presque partout et je voudrais faire attention à ne pas supprimer un signal que je pense seulement être du fond de ciel. Je peux aussi avoir un meilleur aperçu de ce à quoi les couleurs devraient ressembler, en déliant les couleurs dans l'auto-ajustement, mais cela ne corrige que la vue et non l'image elle-même - d'ailleurs je veux garder l'image linéaire pour le moment. Je recommande vivement, lorsque vous manipulez les couleurs en mode linéaire, de laisser l'étirement automatique sur le mode lié aux couleurs, car cela donnera la meilleure indication de ce que nous faisons réellement aux couleurs. 

{{<figure src="colour_DSS4.png" link="colour_DSS4.png" caption="" >}}

Les observations de couleur sur la vue auto-ajustée se reflètent également dans l'histogramme de l'image (vue linéaire, zoomée sur le côté gauche).

{{<figure src="colour_GHS_window5.png" link="colour_GHS_window5.png" caption="" >}}

Je pense que vous pouvez voir que l'histogramme du bleu est à l'extrême droite, et chevauche à peine le vert et le rouge - c'est pourquoi l'image est si dominée par le bleu. Le vert est cependant l'histogramme le plus large, ce qui suggère qu'il contient le plus de signal parmi les trois couleurs.

Pour obtenir un équilibre des couleurs dans les zones d'arrière-plan et les zones plus sombres de l'image, nous aimerions que tous les histogrammes se chevauchent, en particulier dans la région située à gauche du pic de l'histogramme (représentant l'arrière-plan et les zones plus sombres). La première étape consiste à choisir un des histogrammes (ce n'est pas si important, mais je vais choisir celui du milieu - le vert ici - pour montrer comment nous pouvons déplacer l'histogramme bleu vers la gauche et le rouge vers la droite, mais vous pouvez le faire comme vous le souhaitez). Heureusement, la transformation dans Siril peut être effectuée sur une seule couleur à la fois, et nous allons l'utiliser pour réaliser notre équilibre des couleurs.

Tout d'abord, nous allons déplacer le rouge vers la droite. Pour ce faire, je choisis généralement un niveau `SP` à gauche de l'histogramme rouge en cliquant sur l'histogramme lui-même - ou bien vous pouvez le laisser à `0` (le plus facile au début). Désélectionnez le bleu et le vert en cliquant sur le cercle coloré - le fond du cercle s'estompe lorsque la couleur est désélectionnée. Avec un `b` modeste (réglé autour de 1 à 2), je commence à augmenter `D`. Vous verrez l'histogramme rouge se déplacer vers la droite. (Si vous voyez plus que le rouge "bouger", vous avez probablement encore les autres couleurs sélectionnées). À ce stade, vous souhaitez en fait étirer excessivement le rouge - bien au-delà de l'emplacement cible qui chevauche le vert, comme on peut le voir dans l'aperçu de l'histogramme ci-dessous :

{{<figure src="colour_GHS_window6.png" link="colour_GHS_window6.png" caption="" >}}

Comme vous pouvez l'imaginer, cela rend l'image auto-ajusté (liée aux couleurs) très rouge. J'ai délibérément dépassé l'objectif ici, car il est préférable d'effectuer un réglage fin d'une autre manière. Pour décaler le rouge vers ma cible de chevauchement vert, vous devez utiliser le curseur `HP` - si vous vous en souvenez, abaisser la valeur `HP` linéarise l'étirement en pixels à droite de `HP` et assombrit l'image globale. Nous profiterons de cette atténuation de l'image pour repousser le rouge vers la gauche - j'utilise d'abord le curseur, mais à mesure que l'histogramme rouge se rapproche du chevauchement vert, je commence à utiliser les boutons +/- pour effectuer les derniers réglages de `HP`. Je peux même recourir à la saisie de chiffres si je veux être très précis.

Prenez note de la valeur `HP` extrêmement très faible (mais toujours supérieure à `SP`) que j'ai utilisée pour chevaucher l'histogramme vert avec le rouge.

{{<figure src="colour_GHS_window7.png" link="colour_GHS_window7.png" caption="" >}}

Lorsque vous avez aligné le rouge avec le vert, n'oubliez pas d'appliquer votre premier étirement de calibrage de couleur. Juste pour que ce soit clair, nous avons en effet déplacé l'histogramme rouge vers la droite pour chevaucher l'histogramme vert, que nous avions plutôt arbitrairement choisi comme référence. Vous pouvez voir que j'ai concentré le placement de l'histogramme rouge afin qu'il corresponde au vert à gauche du pic de l'histogramme.

{{<figure src="colour_GHS_window8.png" link="colour_GHS_window8.png" caption="" >}}

Maintenant, nous tournons notre attention vers le bleu, qui se trouve toujours à droite du rouge et du vert. Sélectionnez le cercle de couleur bleu (avec le vert et le rouge désélectionnés) et cette fois, réglez `SP` sur 1. À ce réglage de SP, l'image sera assombrie lorsque `D` est appliqué. Encore une fois, réglez `b` autour de 1 à 2 et augmentez lentement `D` et vous devriez voir l'histogramme bleu se déplacer vers la gauche.

{{<figure src="colour_GHS_window9.png" link="colour_GHS_window9.png" caption="" >}}

Ensuite, augmentez `D` pour décaler l'histogramme bleu vers la gauche. Une fois de plus, je dépasse délibérément mon objectif, mais cette fois à gauche de l'histogramme vert. Pour effectuer ce réglage fin, je me tourne en fait vers le cursuer `LP` pour obtenir ma superposition précise. Au fur et à mesure que le `LP` augmente, le bleu s'éclaircit et je m'arrête lorsqu'il atteint la cible.

{{<figure src="colour_GHS_window10.png" link="colour_GHS_window10.png" caption="" >}}

Si vous avez laissé la vue de l'image sur auto-ajustement avec les couleurs liées, vous verrez si vous avez obtenu une meilleure balance des couleurs.

{{<figure src="colour_GHS_window11.png" link="colour_GHS_window11.png" caption="" >}}

Dans notre cas, nous pouvons voir que nous avons obtenu une couleur neutre dans le fond du ciel. L'image est encore dominée par le vert - mais cela se produit le plus souvent lorsque l'on utilise la palette de couleurs SHO, le Ha fournissant le plus grand signal. Cependant, nous avons vu que la teinte verte varie sur l'image - influencée principalement par les rouges. (Remarque : si vous utilisez un filtre double ou triple bande étroite avec une caméra couleur, vous verrez probablement le rouge dominer dans un nuage moléculaire ou une nébulosité, car la ligne spectrale alpha de l'hydrogène est enregistrée dans le canal rouge, plutôt que d'être attribuée au vert).

À ce stade, un sous-produit très important peut être extrait lorsque vous avez équilibré les couleurs de cette manière - et il s'agit du canal de luminance de l'image. En utilisant le module "Extraction" de Siril, et en choisissant l'option CIE L*a*b, vous pouvez extraire le canal L pour l'utiliser dans un traitement supplémentaire - pour être recombiné avec l'image RVB plus tard. Certains choisissent d'utiliser le canal Ha comme luminance, mais je préfère cette méthode car il contient également des informations provenant des autres filtres à bande étroite. En fait, les images monochromes présentées plus tôt étaient basées sur une luminance extraite de ce processus.

À ce stade, vous pouvez également extraire les étoiles de l'image, car cela rendra l'étirement des étoiles et de la nébulosité indépendants les uns des autres. Cela peut offrir une flexibilité supplémentaire pour faire ressortir la nébulosité sans trop éclaircir les étoiles, comme nous l'avons fait avec l'image monochrome ci-dessus. Cependant, GHS est particulièrement bon pour conserver la forme des étoiles - en les gardant petites, donc pour ce tutoriel, j'ai décidé de les laisser.

Une fois que vous avez obtenu votre équilibre des couleurs, vous pouvez procéder à l'étirement de votre image en non-linéaire. (Techniquement, nous avons déjà rendu l'image légèrement non linéaire - mais à moins que ce ne soit scientifiquement/quantitativement important pour vous, je ne le dirai à personne - les autres processus linéaires fonctionneront toujours très bien).

L'étirement vers le non-linéaire doit être fait (au moins au début) avec toutes les couleurs sélectionnées et en employant la même manière que nous l'avons fait pour l'image monochrome. Lorsque les couleurs deviennent un peu confuses, je me concentre sur la couleur dominante et j'effectue les étirements comme si le "vert", dans ce cas, était la couleur monochrome ou dominante.

Voici le résultat après étirement avec les trois couleurs sélectionnées - on pourrait faire mieux, mais vous pouvez voir le rouge et le bleu s'exprimer dans certaines hautes lumières et dans l'ombrage de la couleur verte dominante. Cependant, si la représentation du vert est bonne dans l'image, le bleu et le rouge pourraient être davantage renforcés.

{{<figure src="colour_DSS5.png" link="colour_DSS5.png" caption="" >}}

On peut voir dans l'histogramme de cette image désormais non linéaire que le rouge (S) et le bleu (O) sont en retard dans la partie nébulosité de l'image - dans les tons moyens. Pour y parvenir, nous voulons finaliser notre étirement en nous concentrant sur le rouge et le bleu, tout en laissant le vert seul - en ajustant la couleur dans l'image non linéaire.

{{<figure src="colour_GHS_window12.png" link="colour_GHS_window12.png" caption="" >}}

## Réglage des couleurs non linéaire

Il existe de nombreuses façons d'ajuster la couleur dans Siril - vous pouvez ajuster directement la teinte, l'étirement de la saturation, etc., mais j'ai découvert que les transformations généralisées de l'histogramme ont un rôle à jouer. La possibilité d'étirer les couleurs indépendamment est un bon moyen d'y parvenir. L'astuce consiste à exagérer les couleurs les plus faibles (ou à diminuer les couleurs dominantes) pour apporter plus de variété de couleurs dans l'image.

A titre d'exemple, j'ai d'abord éteint le vert, et étiré le rouge uniquement pour apporter plus de rouge dans les étoiles et montrer plus de rouge dans la nébulosité. En jouant avec les paramètres, vous comprendrez comment modifier un histogramme de couleur individuel, de la même manière que nous avons manipulé les images monochromes plus tôt. Ici, j'ai utilisé un bas `b` (`b` négatif) parce que je veux un large éclaircissement de l'histogramme rouge, en me concentrant sur les tons moyens - en utilisant un `SP` de 0.2 dans ce cas. De plus, `HP` a été fortement utilisé pour protéger les étoiles des gonflements et de devenir trop rouges.

{{<figure src="colour_GHS_window13.png" link="colour_GHS_window13.png" caption="" >}}

J'ai utilisé un grand `b` pour concentrer le contraste ajoute dans les tons moyens pour le rouge et le bleu et placé `SP` à l'endroit d'où je voulais que les tons moyens proviennent - cliquer sur l'image est un bon moyen de le faire. Enfin, un peu de `LP` a été utilisé pour protéger les basses lumières et maintenir l'équilibre des couleurs, ainsi qu'une faible valeur de `HP` pour protéger les hautes lumières. Tout cela en surveillant à la fois l'histogramme et l'image de prévisualisation elle-même. Il peut arriver que vous créiez des bosses inutiles dans l'histogramme de la couleur que vous ajustez, ou que la couleur soit déséquilibrée. Vous pouvez toujours réparer de telles occurrences avec un étirement ultérieur.

Notez qu'en raison des propriétés mathématiques des transformées hyperboliques généralisées, elles n'ajoutent pas de bruit et ne créent pas d'artefacts irréparables en cas d'applications multiples - du moins pas jusqu'à la précision numérique de l'ordinateur. Cependant, si vous vous perdez, il est souvent utile de revenir à un endroit précédemment sauvegardé, ou même de revenir au début de votre traitement - en commençant par un empruntant un nouveau chemin. Avec de la pratique, vous aurez bientôt une idée des paramètres et de la façon dont ils contrôlent à la fois les histogrammes et l'image. 

Une fois que le rouge est arrivé à un point où je sentais qu'il contribuait davantage à l'image, je me suis tourné vers le bleu pour effectuer une deuxième série d'étirements. La plupart des étirements font intervenir les cinq paramètres/curseurs d'entrée pour contrôler les ajustements de l'image.

Une deuxième série d'étirements a ensuite été effectuée avec uniquement le bleu. Comme illustré dans la transformation et l'histogramme ci-dessous, le bleu a été exagéré. Vous pouvez voir qu'une fois de plus, les cinq paramètres d'entrée/curseurs ont été utilisés pour contrôler les ajustements de couleur.

Voici l'histogramme avec lequel j'ai terminé. J'ai rapproché les histogrammes du bleu et du rouge, sans toutefois les aligner complètement sur le vert.

{{<figure src="colour_GHS_window14.png" link="colour_GHS_window14.png" caption="" >}}

Cela se reflète dans l'image résultante qui montre maintenant une variation de teinte considérable à travers la nébulosité et à l'intérieur des étoiles.

{{<figure src="colour_DSS6.png" link="colour_DSS6.png" caption="" >}}

Tous ces ajustements de couleurs sont une question de goût, et de l'utilisation finale de votre image. Vous pouvez continuer à effectuer des étirements de couleur sélectifs dans n'importe quelle combinaison jusqu'à ce que vous obteniez les effets que vous désirez. Deux mises en garde s'imposent toutefois. La première est qu'il est probable que vous étirez (éclaircissez) les signaux les plus faibles plus que les signaux les plus forts. Cela peut signifier que le bruit associé aux canaux les plus faibles peut être exagéré. C'est le bruit des données qui, en fin de compte, limitera le degré d'étirement de la couleur ou de l'image dans son ensemble.

Une deuxième mise en garde concerne les images à bande étroite et plus particulièrement la palette Hubble. Comme vous pouvez le constater, nous n'avons pas supprimé la dominante verte qui persiste dans l'image. Dans la plupart des cas, vous souhaiterez peut-être vous débarrasser davantage, voire complètement, de cette dominante. En outre, un ton magenta (causé par un manque de vert) est entré dans les étoiles, ce qui n'est généralement pas souhaité. La bonne nouvelle est qu'il existe d'excellents moyens de traiter ces problèmes de SHO dans Siril.

La façon la plus simple (mais pas nécessairement la meilleure) de traiter les verts de votre image Hubble est d'utiliser les processus `Extraction` et `Composition RVB`, comme indiqué dans les étapes suivantes :

1. Si vous avez traité un canal de luminance (L) séparé, notez son emplacement. Si ce n'est pas le cas, extrayez un canal de luminance en utilisant l'option `Extraction -> Séparer les canaux` avec l'option `CIE - L*a*b` - le canal L vous intéresse.

2. Effectuez ou répétez `Extraction -> Séparer canaux` en utilisant l'option RVB. Cela créera des représentations monochromes de vos canaux de couleur.

3. Utilisez la fonction `Image Composition` pour reconstituer les canaux de luminance, rouge, vert et bleu. Le vert peut être supprimé de l'image en sélectionnant le bouton `Finaliser la balance des couleurs` et en réduisant la contribution du vert. Comme nous avons utilisé un canal de luminance, la luminance (luminosité) sera préservée.

Voici des versions de l'image composée avec 100%, 85% et 70% laissés en place.

{{<figure src="colour_DSS7.png" link="colour_DSS7.png" caption="" >}}
{{<figure src="colour_DSS8.png" link="colour_DSS8.png" caption="" >}}
{{<figure src="colour_DSS9.png" link="colour_DSS9.png" caption="" >}}

Par essais successifs, vous pouvez laisser du vert dedans ou dehors, selon vos goûts. Ce processus est mieux exécuté sans étoile, car nous modifions également la couleur de l'étoile avec la nébulosité. De plus, vous trouverez probablement du magenta rampant dans l'image (en particulier dans les étoiles).

Le magenta peut être supprimé d'une image en inversant d'abord l'image à l'aide de la `Transformation négative` qui fera apparaître le magenta en vert. En appliquant le `Supprimer le bruit vert`, (j'aime l'option `Neutre Maximal` avec `Préserver la luminosité` sélectionnée), sur l'image inversée, puis en réinversant à nouveau en utilisant le processus `Transformation négative`, vous aurez supprimé tout ou partie de le magenta de l'image et obtenez une "image de la palette Hubble" avec des teintes que vous connaissez mieux.

Voici la version 65% verte après suppression du magenta :

{{<figure src="colour_DSS10.png" link="colour_DSS10.png" caption="" >}}

Vous pouvez en fait décider que vous ne voulez pas du tout de vert dans l'image, au-delà de ce qui est nécessaire pour l'équilibre des couleurs. Cela peut également être fait grâce à une combinaison des techniques ci-dessus en utilisant les applications `RVB Composition` et `Supprimer le bruit vert`, mais maintenant nous nous aventurons trop loin du cadre de ce didacticiel.

{{<figure src="colour_DSS11.png" link="colour_DSS11.png" caption="" >}}

Une fois que vous avez ajusté la couleur de l'image, il est presque toujours utile d'ajuster l'image, encore une fois avec GHS, tout en utilisant toutes les couleurs sélectionnées pour affiner le contraste et la luminosité.

# Suggestions supplémentaires

En dehors de l'étirement, il existe de nombreux processus supplémentaires dans Siril qui peuvent améliorer vos résultats bien plus que le GHS seul. Dans ce tutoriel, je n'ai pas appliqué la suppression du gradient, la réduction du bruit, la déconvolution, les processus d'ondelettes, l'accentuation, l'ajustement supplémentaire de la teinte, ou d'autres techniques de traitement d'image dans l'écurie toujours croissante de processus et de scripts natifs et complémentaires de Siril. Il est certain que les images que j'ai montrées ici peuvent être améliorées avec un peu plus d'attention et de traitement.

Une technique souvent utilisée consiste à traiter séparément le canal de luminance (obtenu soit par extraction à partir de images de filtres de luminance RVB ou monochromes) et à le recombiner vers la fin du flux de travail. Avec les étirements GHS, en plus d'autres traitements, on peut alors concentrer son travail sur l'apport de contraste, de luminosité et de détails sur l'image de luminance. L'étirement du RVB devrait se concentrer sur la teinte et la saturation des couleurs. Enfin, en les recombinant, vous obtiendrez le meilleur des deux mondes.

Une autre technique utilisée par beaucoup consiste à séparer (ou extraire) les étoiles avant de les étirer. Bien que le GHS soit très habile pour protéger les étoiles tout en faisant ressortir la nébulosité et les détails faibles, il est beaucoup plus facile de se concentrer sur les caractéristiques faibles tout en étirant l'image sans étoile et sans se soucier des étoiles, tout en concentrant votre étirement des étoiles pour leur donner le contraste, la luminosité et la taille que vous désirez. Cette création d'une image sans étoile et d'une image étoilée sert également à aider d'autres traitements en plus du SGH, en évitant de nombreux artefacts d'étoiles qui viennent avec de nombreux traitements. 

Lorsque l'on étire des caractéristiques peu lumineuses telles que les IFN, la nébulosité peu lumineuse ou les "atmosphères" galactiques, on se trouve souvent en concurrence avec le bruit de fond et il peut être tentant d'abuser de la réduction du bruit. D'après mon expérience, alors que GHS peut afficher des caractéristiques très faibles, les retirer du bruit de fond peut nécessiter un temps d'intégration supplémentaire (plus de trames ou des expositions plus longues) pour améliorer le rapport signal/bruit.

D'un point de vue positif, il est facile de passer à côté d'éléments de faible intensité dont vous n'avez peut-être même pas conscience de l'existence dans vos données. Je vous recommande vivement d'effectuer quelques étirements "exploratoires" au début de vos images empilées en utilisant GHS - en vous concentrant non pas sur l'amélioration de l'image, mais sur l'observation de ce qui se trouve au niveau et à la gauche des pics de l'histogramme linéaire. Une fois que vous savez ce qui s'y trouve, vous pouvez décider de le faire ressortir, de le laisser dans le fond du ciel ou de faire ce qui vous semble le mieux adapté à votre image. 

J'espère que ce tutoriel vous aidera et que vous trouverez du plaisir dans le traitement de vos images.

Bon ciel,
Dave Payne

Un grand merci à Mike Cranfield, Adrian Knagg-Baugh, Cyril Richard et au reste de l'équipe Siril pour avoir apporté GHS à Siril.
