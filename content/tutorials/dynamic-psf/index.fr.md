---
title: Analyse d'une image
author: Cyril Richard
show_author: true
featured_image: global_psf.png
rate: "2/5"
type: page
---

Dans ce tutoriel, nous allons analyser une image grace à ses étoiles. Qu'est ce qu'une FWHM ? En quoi est-ce révélant de la qualité de la photo ?

# L'outil de PSF Dynamique
La PSF Dynamique est un outil implémenté depuis très longtemps dans Siril. Bien qu'étant utile dans de nombreuses situations, telle que l'alignement stellaire global, son utilisation reste obscure pour la majorité des utilisateurs.
{{< figure src="dpsf-tool-fr.png" caption="La fenêtre de la PSF Dynamique lorsqu'une detection d'étoiles a été appliquée." >}}
C'est un outil puissant, facile d'utilisation, qui permet d'en apprendre plus sur la qualité de son image. Pour y accéder il faut passer par le menu "burger", puis `Information de l'image` et `PSF Dynamique`.

## Un peu de théorie
La **fonction d'étalement du point** (**Point Spread Function** ou [PSF](https://fr.wikipedia.org/wiki/Fonction_d%27%C3%A9talement_du_point) en anglais), est une fonction mathématique qui décrit la réponse d'un système d'imagerie à une source ponctuelle. En théorie, du fait de leur très grande distance, les étoiles sont des objects parfaitement ponctuels. En théorie seulement, car avant de parvenir jusqu'à notre rétine la lumière traverse un milieu qui va la faire interagir : l'atmosphère terrestre, le verre de nos lunettes astro (ou bien le miroir des telescopes) et les capteurs photo. Ces derniers vont alors avoir tendance à élargir ce point, d'où le nom de cette fonction. Pour faire simple, plus l'optique utilisée sera performante et l'atmosphère "chilienne", plus les étoiles seront fines et piquées. À l'inverse, utiliser un cul de bouteille en pointant l'horizon donnera de grosses étoiles baveuses.

### Fonctions du modèle PSF
Dans Siril, la fonction PSF est ajustée à partir d'un "profil". Il est proposé au choix un profil de type Gaussien ou de type Moffat. *In fine* cela permet de calculer des paramètres importants comme la FWHM, la rondeur, la magnitude, etc...
- Ca: Canal sur lequel la PSF a été appliquée
- B: Moyenne de fond de ciel local.
- A: Amplitude la PSF ajustée (valeur maximale)
- (x<sub>0</sub>, y<sub>0</sub>): Coordonnées du centroïde en unités de pixels.
- (FWHM<sub>x</sub>, FWHM<sub>y</sub>): La FWHM sur les axes X et Y en coordonnées PSF.
- Mag: Magnitude relative de l'étoile.
- r: Rondeur de l'étoile, r=1 signifie une étoile parfaitement ronde.
- Angle: Angle en degrés de rotation de l'axe X des coordonnées PSF.
- RMSE: Erreur quadratique moyenne de l'ajustement.

### FWHM, un critère de selection
Soit une courbe gaussienne de dimension 1, comme représentée sur l'image suivante. La FWHM (de l'anglais *full width at half maximum*) représente la largeur de la courbe à mi-hauteur.
{{< figure src="fwhm.png" caption="Largeur à mi-hauteur (FWHM)." >}}
Dans le cas d'une étoile, la gaussienne est de dimension 2 et la FWHM s'exprime alors selon deux axes, x et y, soit en pixel, soit en seconde d'arc (si la résolution de l'image est connue). En quelque sorte, la FWHM quantifie l'étalement de l'étoile rapport au point.

### La rondeur, un critère supplémentaire
La rondeur d'une étoile est définie comme étant le rapport de la FWHM selon y sur celle selon l'axe x. De fait, une rondeur de 1 représente une étoile parfaitement ronde alors qu'une rondeur de 0.5 présente une étoile deux fois plus allongée sur l'axe x. C'est donc un critère qui, associé à la FWHM (comme proposé pendant l'étape de l'empilement), permet d'affiner la sélection des images.

{{< figure src="Star_roundness_fr.png" >}}

## Utilisation de l'outil
Il existe deux façons d'utiliser l'outil de PSF dynamique de Siril.
1. Étoile par étoile, en les sélectionnant soigneusement. En effet, une sélection autour de l'étoile puis un clic droit et `Pointer l'étoile` permet d'ajouter l'étoile dans l'outil de psf dynamique. L'avantage est ici que chaque étoile ajoutée est choisie par l'utilisateur.
{{< figure src="pick_a_star-fr.png" caption="Un pointé manuel peut s'avérer de meilleur qualité pour analyser son image car l'utilisateur sélectionne lui même le nombre et les étoiles utilisées." >}}
2. Globalement, en cliquant sur le bouton de détection d'étoiles. Ici, Siril cherche automatiquement tout ce qui s'apparente à une étoile. Or, souvent, comme on peut le voir dans l'exemple ci dessous, l'algorithme peut se tromper sur un objet brillant. Bien que cela n'ait aucun impact sur l'alignement stellaire, cela peut un peu fausser les résultats de l'analyse. Jouer sur la valeur du seuil de détection (en l'augmentant dans ce cas) peut s'avérer salutaire.
{{< figure src="global_psf-fr.png" caption="Détection globale des étoiles telle que faite en interne par l'outil d'alignement stellaire. On voit ici que pour des raisons de rapidité de calcul, l'ajustement sur la fonction gaussienne ne tient pas compte de l'angle de la fonction, contairement aux résultats du cas par cas. Cela n'a que très peu d'impact sur le résultat final." >}}

En regardant de plus prés, on peut s'appercevoir que:
- Les étoiles s'affichent sur l'image sous forme d'ellipses, dont l'élongation est en rapport avec la rondeur de l'étoile.
- Les étoiles entourées en mauve sont les étoiles ayant été détectées comme saturées.
{{<figure src="closer-view.png" >}}

Plutot que de calculer les caractéristiques de __toutes__ les étoiles d'une image, il est aussi possible de limiter le choix suivant 2 critères:
- l'amplitude max, A. Par défaut dans la gamme [0,1] (valeur normalisée des pixels), la ``Plage d'amplitude`` peut etre ajustée.
- la rondeur max, r. Là aussi, la valeur par défaut est fixée à 1, mais la ``Plage de rondeur`` peut être ajustée.
Cela a aussi pour effet de limiter le temps de calcul si le champ est particulièrement peuplé.

Une fois les étoiles chargées dans la fenêtre, il est possible de les trier par ordre croissant ou décroissant en fonction de n'importe quels paramètres. Cette liste est également exportable au format `csv` lorsque l'on clique sur le bouton correspondant dans la barre d'outil.

Si vous souhaitez effacer la liste d'étoiles en cous d'affichage, il vous suffira de taper la commande ``clearstar`` dans la ligne de comande. 

Un point interressant: si vous sélectionnez une étoile dans la liste, celle-ci sera automatiquement pointée par 2 axes orthogonaux de couleur bleue dans l'image. Et inversement, lorsque vous cliquez sur une étoile détectée dans l'image, elle sera mise en surbrillance dans la liste.

Enfin, et c'est là un des intérêts de l'outil, il est possible d'obtenir une analyse moyenne des étoiles sélectionnées de l'image.
{{<figure src="psf-summary-fr.png" >}}

### Cas concret
Lorsque l'alignement stellaire global échoue, il peut être intéressant d'en comprendre les causes et d'essayer de remédier au problème. Bien souvent il suffit de jouer avec les paramètres de détection tels que le seuil et la rondeur des étoiles. A chaque changement de paramètres on veillera à vérifier les effets en cliquant sur le bouton de détection.
{{< figure src="star_detect.png" >}}
Une fois que ceux-ci semblent parfaitement adaptés à l'image, il faut relancer l'alignement global. Ce dernier utilisera alors les nouveaux paramètres.

Voici un exemple particulier d'image ou les paramètres par défaut ne permettent pas de faire un alignement global : une seule étoile est détectée. Cependant, on voir facilement en mode autoajustement que plusieurs étoiles sont présentes dans l'image, et même en nombre suffisant pour permettre une alignement.
{{<figure src="global_issue1-fr.png" link="global_issue1-fr.png" >}}
La première modification que l'on est tenté d'essayer est de baisser le niveau de seuil, afin d'aller chercher des étoiles en limite de bruit. Cependant, on voit dans notre exemple que l'amélioration est très faible : 3 étoiles détectées, contre 1 précédemment. On pourrait alors penser qu'il est nécessaire de baisser encore le niveau du seuil, mais si on regarde bien les étoiles detectées, on s'aperçoit que celles-ci sont faibles et de petites tailles.
{{<figure src="global_issue2-fr.png" link="global_issue2-fr.png" >}}
Le paramètre qu'il faut changer ici c'est le rayon de la boite de detection. Ce dernier est en effet trop petit pour detecter les étoiles de l'image. L'image ci-dessous le prouve, avec un rayon dont la taille a été doublée.
{{<figure src="global_issue3-fr.png" link="global_issue3-fr.png" >}}

Bien sûr, il est aussi possible de modifier les paramétres comme la ``Convergence`` ou le ``Type de profile`` (Moffat vs gaussien).
