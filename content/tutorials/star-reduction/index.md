---
title: Automated Star Reduction Script
author: Rich Stevenson
show_author: true
featured_image: Capture-01.png
rate: "1/5"
type: page
---

This tutorial will show you how to install and use a script that automates your star reduction workflow within Siril. With this script, you can streamline the process of reducing the size of your stars in your astrophotography images and get stunning results in seconds!

*Attention*: The script requires `Siril1.2.0-beta1` or newer, and `StarNet++ CLI` installed and configured within Siril.

This tutorial is also available on my YouTube channel Deep Space Astro:

{{< youtube Na6GzKozpCI >}}

#  Download the Star Reduction script
The script can be downloaded using this [link]([DSA]%20Star%20Reduction-MTF.ssf) (right click and save as...).

Just follow along the instructions given [in the documentation](https://siril.readthedocs.io/en/stable/Scripts.html#adding-custom-scripts-folders) to store the script in your custom scripts folder and refresh the scripts list.

You're now ready to run the script.

# Open the image for star reduction

Open the image in Siril that you want to reduce the stars in. The image should already be stretched and processed.

For best results the image should be in `.fit` format. However, since Siril's implementation of StarNet converts the images, I've had success running the star reduction script against other formats as well such as tif and jpg.

# Run the star reduction script

Click on the Scripts menu and then click `[DSA] Star Reduction-MTF`.

{{< figure src="Capture-03.png" caption="Run the Star Reduction script." >}}

The process can take a few minutes to complete and you can watch the progress in the console screen. Once the script is finished, you'll be presented with your image with the stars reduced.

# Increase the reduction of the stars

You can further reduce the stars in one of the two following ways.

1. Simply run the script a second time.
2. Modify the script values as shown in the next step.

# Modify the values in the script before executing

1. Open the `[DSA] Star Reduction-MTF.ssf` in any text editor.
2. Decrease the two highlighted values shown below. For example, change them from 0.20 to 0.10. They should both be set to the same value.

{{< figure src="Capture-04.png" caption="Run the Star Reduction script." >}}

3. Save the script
