---
title: Traitement des images du ZWO Seestar
author: Cyril Richard
show_author: true
featured_image: seestar.jpg
rate: "0/5"
type: page
---

Ce tutoriel explore le prétraitement et le traitement des images obtenues à partir de 
ZWO Seestar. Le prétraitement sera effectué à l'aide d'un script dédié.

{{< table_of_contents >}}

# Prétraitement
## 1. Téléchargez le script de prétraitement de vos images

Nous avons créé un script adapté au prétraitement de votre seestar. Cela signifie que ce script s'attend à avoir plusieurs images qu'il empilera. Dans la 
version actuelle de Siril, vous devez la télécharger manuellement en suivant ce 
[lien](https://gitlab.com/free-astro/siril-scripts/-/raw/main/preprocessing/Seestar_Preprocessing.ssf?ref_type=heads&inline=false).
Placez le fichier téléchargé (`Seestar_Preprocessing.ssf`) à l'endroit de votre 
choix, puis dans les préférences de Siril, dans l'onglet script, entrez le dossier 
que vous avez choisi. Vous devez saisir le chemin d'accès complet dans la case 1 
de l'image ci-dessous !

Cliquez ensuite sur le bouton 2 pour recharger les dossiers de scripts et faire apparaître le script dans le menu `Scripts`.

{{<figure src="scripts_preferences.png" caption="Les préférences de Siril dans l'onglet script">}}
{{<figure src="script_menu.png" caption="Le script est maintenant disponible dans le menu">}}

## 2. Créez le dossier qui contiendra vos images

Le seestar ne donne accès qu'aux images brutes, et pas au darks/flats/offsets. 
La calibration n'est donc pas possible. Il suffit de créer un seul répertoire
dans le dossier où sont stockées vos images.

Créer le répertoire suivant nommé comme :
 * `lights`
 
{{<figure src="cwd.png" caption="Structure des dossiers telle qu'elle devrait être définie.">}}

## 3. Placez vos images brutes dans le répertoire créé à l'étape précédente

**Attention** : ne mélangez pas d'autres fichiers, même JPEG (ou d'autres formats), 
avec les fichiers brutes : Siril prend tous les fichiers contenus dans le 
répertoire comme fichiers d'entrée pour traitement.

## 4. Cliquez sur le bouton *maison* et naviguez jusqu'au dossier de votre projet

Il s'agit d'indiquer à Siril où se trouvent toutes vos images. Sélectionnez le répertoire à la racine 
du dossier `lights`, pas à l'intérieur de `lights` lui-même et valider avec le bouton *Ouvrir* de la fenêtre de dialogue ouverte.

{{<figure src="Capture-02.fr.png" caption= "Barre d'entête du Siril" >}}

Siril validera votre répertoire de travail, qui sera indiqué dans la barre de 
titre de l'application. Vérifiez qu'il s'agit bien du bon chemin.

## 5. Cliquez sur le bouton *Scripts* et sélectionnez le script de votre choix

Le script qui couvre les besoins de Seestar est nommé `Seestar_Preprocessing`. Cliquez dessus
et attendez que le prétraitement soit terminé.

## 6. Charger l'image empilée

L'image empilée est normalement située à côté de votre dossier `lights` et du dossier `process` nouvellement créé. Elle doit être nommée en fonction de l'objet photographié et du nombre d'images empilées, comme par exemple : `NGC_2175_268x20sec_T7°C_2024-03-07.fit`.
Cliquez sur le bouton `Ouvrir` et chargez l'image.

L'image affichée est très sombre, mais c'est normal. Passez en mode de visualisation `Auto-ajustement` pour voir l'image.
Veuillez noter qu'il s'agit uniquement d'un mode de visualisation et qu'il ne modifie pas votre image.

{{<figure src="visualisation.fr.png" caption="Menu de visualisation" >}}

# Traitement

Une fois le prétraitement terminé, il est temps de traiter l'image. Vous avez besoin de :
- recadrer si nécessaire
- supprimer le gradient
- étalonnage les couleurs
- étirer l'histogramme
- enregistrer l'image

Bien sûr, vous pouvez faire beaucoup plus, mais si vous êtes sur cette page, c'est que vous êtes un débutant et nous allons rester simples.

## 1. Recadrer l'image si nécessaire
Le suivi des phares est loin d'être parfait. Il en résulte une forte rotation des champs 
d'une image à l'autre. Il est donc conseillé de recadrer l'image si les 
Les bords ne sont pas jolis. Si ce n'est pas nécessaire, passez à l'étape 3.

Il suffit de faire une sélection avec le clic gauche de la souris, puis de faire un clic droit pour 
fait apparaître un menu contextuel et choisir `Recadrer`.

## 2. Extraction du gradient
Cette étape permet de supprimer le gradient éventuellement présent dans votre image. Un tutoriel détaillant l'ensemble du processus a été rédigé [ici](../gradient).

## 3. Étalonnage des couleurs
Siril propose deux façons de récupérer les couleurs de votre image.
Ici, "récupérer" signifie rééquilibrer les canaux RVB pour se rapprocher le plus possible des vraies couleurs de l'objet photographié.

La meilleure façon de récupérer les couleurs est de comparer la couleur des étoiles dans l'image avec leur couleur dans les catalogues, afin d'obtenir la couleur la plus naturelle de façon automatique et non subjective. Cette opération est réalisée à l'aide de l'outil PCC (Étalonnage des couleurs par photométrie).

Pour fonctionner, l'outil a besoin de connaître la position exacte des étoiles. Dans la série 1.2, Siril permet l'astrométrie avant l'étalonnage des couleurs dans le même outil.
Les images Seestar ont toutes les informations nécessaires dans l'en-tête du fichier pour que l'astrométrie fonctionne correctement (il n'est pas nécessaire de chercher le nom de l'objet et de cliquer sur `Rechercher`). Il suffit d'appuyer sur le bouton `OK` pour effectuer l'astrométrie, puis la photométrie, automatiquement.

{{<figure src="PCC.fr.png" caption="Photometric Color Calibration" >}}

## 4. Étirer l'histogramme
Il existe 3 outils pour étirer l'algorithme dans Siril.
- La meilleure façon d'étirer l'histogramme est d'utiliser l'outil [d'étirement hyperbolique généralisé](../ghs). Avant d'étirer l'histogramme, il est généralement conseillé de revenir à la visualisation linéaire, afin de voir à l'écran à quoi ressemble réellement l'image.
- L'outil le plus simple est la transformation de l'histogramme. Il ne donne cependant pas les meilleurs résultats. Il est expliqué en détail dans ce [tutoriel](../tuto-scripts/#on-va-réaliser-un-petit-asinh-sur-limage).

## 5. Sauvegarder l'image
Vous pouvez enregistrer votre image à tout moment. Toutefois, si vous souhaitez l'enregistrer dans un format lisible par d'autres logiciels, cliquez sur le bouton indiqué dans l'illustration ci-dessous.
{{<figure src="save.fr.png" caption="Sauvegarde d'une image" >}}
