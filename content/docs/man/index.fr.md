---
title: "Pages du manuel Siril"
author: Cyril Richard
---

## Nom
       siril - outil de traitement d'images pour l'astronomie et autres

## Synopsis
       Siril GUI
        siril [options] [file]             Démmarrer Siril.

       Siril CLI
        siril-cli  [-i  conf_file]  [-f]  [-v]  [-c]  [-h]  [-p]  [-d  working_directory]  
       [-s  script_file] [-r input_pipe_path] [-w output_pipe_path] [image_file_to_open]


## Description
Siril est un outil de traitement d'image spécialement conçu pour la réduction du bruit et l'amélioration du rapport signal / bruit d'une image à partir de plusieurs captures, comme l'exige l'astronomie. Siril peut aligner, empiler et améliorer les images de divers formats de fichiers, même des séquences d'images (films et fichiers SER). Il utilise OpenMP, prend en charge toutes les caméras prises en charge par libraw et effectue ses calculs en 32 bits virgule flottantes.

## Options
       -i     Démarre Siril avec le fichier de configuration dont le chemin est
              indiqué dans conf_file

       -f     (ou  --format) Imprime tous les formats d'entrée d'image pris en
              charge, en fonction des bibliothèques détectées au moment de la
              compilation

       -v     (ou --version) Imprime le nom et la version du programme et quitte
       
       -c     (ou --copyright) Affiche le copyright et quitte

       -h     (ou --help) Affiche l'aide

       -p     Démarre Siril sans l'interface utilisateur graphique et utilise les
              canaux nommés pour accepter les commandes et imprimer les journaux
              et les informations d'état. Sur les systèmes POSIX, les tubes nommés
              sont créés dans /tmp/siril_commands.in et /tmp/siril_commands.out
              
       -r     (ou --inpipe) Fournit un chemin alternatif pour le tuyau d'entrée 
              qui reçoit les commandes, si -p est fourni. Le tuyau peut être créé 
              par un programme externe avec la commande mkfifo(1)
              
       -w     (ou --outpipe) Fournit un chemin alternatif pour le tuyau de sortie 
              qui imprime les journaux et les mises à jour d'état, si -p est 
              fourni. Le tuyau peut être créé par un programme externe avec la 
              commande mkfifo(1)

       -d     (ou --directory) Définition de l'argument dans le dossier de travail

       -s     (ou  --script) Démarre Siril sans l'interface utilisateur graphique
              et exécute un script à la place. Les scripts sont des fichiers texte
              contenant une liste de commandes à exécuter séquentiellement. Dans
              ces fichiers, les lignes commençant par un # sont considérées comme
              des commentaires.

       image_file_to_open
              Ouvrir un fichier image ou séquence juste après le démarrage

## Files
       ~/.config/siril/siril.cfg
              Préférences de l'utilisateur. Remplacé par l'option -i.
       ~/.siril/siril.css
              La feuille de style utilisée pour changer les couleurs de l'interface
              utilisateur graphique. Ceci est utile pour la personnalisation ou si
              un thème GTK est incompatible avec certains éléments colorés de Siril.

## Links
* Website avec nouvelles, tutoriels, FAQ et plus encore : https://siril.org
* Forum: https://discuss.pixls.us/siril
* Documentation: https://siril.readthedocs.io/fr
* Code et rapports de bugs: https://gitlab.com/free-astro/siril/issues

## Authors
* [Vincent Hourdin](debian-siril@free-astro.org)
* [Cyril Richard](cyril@free-astro.org)
* [Cécile Melis](cissou8@gmail.com)
* [Adrian Knagg-Baugh](aje.baugh@gmail.com)

## Copyright
* Copyright © 2021 Free Software Foundation, Inc.  Licence GPLv3+ : GNU GPL version 3 ou ultérieure <https://gnu.org/licenses/gpl.html>.
Il s'agit d'un logiciel libre : vous êtes libre de le modifier et de le redistribuer.  Il n'y a AUCUNE GARANTIE, dans les limites autorisées par la loi.
