---
title: Sirilic
author: M27trognondepomme
type: page
featured_image: sirilic-fr-01.png
TOC: true
---

## Introduction
### La genèse

En juin 2018, avec la version 0.9.9 de Siril est apparue une nouvelle fonctionnalité: les scripts. Ils ont permis d'enchaîner les processus. Pour mes besoins personnels et en m'inspirant du premier script, j'ai écrit un premier programme en python qui permettait de générer des scripts avec des options (seuils, *etc* ...). J'ai présenté le petit outil à mon club d'astronomie: *@LaLoucheDuNord* a été immédiatement emballé et je l'ai partagé sur le web. Après quelques améliorations, j'ai posté sur [Webastro](https://www.webastro.net/forums/topic/167072-sirilic-exconv2siril/) en août 2018 une première version sous le nom **Conv2Siril** qui devint plus tard **Sirilic**. J'ai choisi  ce nom car c'est  l'acronyme de "**Siril**'s **I**nteractif **C**ompanion" et qu'en français, **Sirilic** a une consonance proche du mot  *cyrillique*. Et après de nombreux échanges sur le forum Webastro et de nombreuses améliorations, la version 1.0 de **Sirilic** est sortie en juin 2019.

### Qu'est ce que Sirilic ?
Il s'agit d'un outil logiciel graphique interactif permettant d'automatiser le prétraitement des images astronomiques à l'aide du logiciel **Siril**. Il crée les fichiers maîtres (`Offset`,`Flat`, `Dark` et `Dark-Flat`) associés à une séquence d'images, calibre chaque image brute, les aligne et les empile. L'amélioration par rapport à Siril est qu'il ne nécessite pas un ensemble prédéfini de fichiers d'entrée, et prend également en charge le traitement de plusieurs sessions ou les acquisitions de filtres. Il affiche graphiquement le processus actuel pour chaque couche.

### Pourquoi Sirilic ?
Au départ, **Sirilic** a été créé pour construire facilement des scripts personnalisés en quelques clics et quelles que soient les sources d'images (*APN*, *CCD mono*, *etc* ...) et les options choisies. Pour aider les débutants, un affichage graphique a été ajouté. Il montre les opérations de script basées sur les options cochées.

### Que fait Sirilic exactement ?
Il fait principalement 4 choses :

- Organiser le répertoire de travail **Siril** en le structurant en sous-dossiers
- Copier ou lier les fichiers `Brute`, `Offset`, `Dark` ou `Flat` dans les sous-dossiers
- Générer un script **Siril** adapté aux fichiers présents et aux options choisies
- Lancer les traitements sur les images: **Siril** est lancé en mode script en tâche de fond

Il traite indifféremment les images issues d'APN, de camera CCD / CMOS couleur et monochrome.

Avec une camera CCD mono, il est capable gérer les couches suivantes :
```
L, R, V, B, Halpha, SII, OIII, Hbeta
```
Il peut traiter plusieurs objets à la fois et gère le multi-session par couche, c'est-à-dire il pré-traite chaque séance séparément puis les regroupe dans un seul dossier, aligne et empile l'ensemble des images. Ce que **DSS** faisait sur une couche, **Sirilic** le fait sur plusieurs couches et objets à la fois.

### Ce que ne fait pas Sirilic
Il ne fait pas les traitements finaux :

- Assemblage des couches
- Correction colorimétrique
- Suppression du gradient

Il ne traite pas les images encapsulées dans un fichier vidéo (`avi`). Sirilic reste un outil générique couvrant la plupart des cas d’emploi. Pour des cas spécifiques, il faut passer directement sous **Siril**.

### Conseil d'utilisation pour débutants
Avant d'utiliser **Sirilic**, il est nécessaire de comprendre les opérations de base du pré-traitement des images astronomiques. Effectuer toutes les étapes du traitement d'image au moins une fois dans **Siril** est probablement une bonne idée.

### Le logiciel
**Sirilic** est écrit en **python** et utilise l'interface graphique **wxwidget**. La principale difficulté de **Sirilic** a été de construire une interface lisible et d'offrir en même temps le maximum d'options. Il fonctionne sur les OS suivants : GNUL/Linux / Mac-OS / Windows 10. 

Il est développé sous licence **LGPL-v3** et est hébergé dans le groupe free-astro sur [Gitlab](https://gitlab.com/free-astro/sirilic).

{{< figure src="sirilic-fr-01.png" >}}

## Installation

###  Téléchargement 
La dernière version est disponible ici : https://gitlab.com/free-astro/sirilic/-/releases

### Conditions préalables
Sur linux, il faut avoir installé le logiciel **python**, de préférence une version >=3.5 (idéalement la 3.7). A noter que la version python 2.7 (qui n'est plus maintenu depuis janvier 2020), n'est pas supportée par Sirilic. 

Il faut aussi installer le package graphique **wxpython**.

Sous **Debian** ou **Ubuntu**:

```bash
$ sudo apt install python3
$ sudo apt install python3-wxgtk4.0
$ sudo apt install python-is-python3 
```

Sous **Arch Linux**

```bash
$ sudo pacman -S wxpython 
```

Sous Windows, aucun prérequis n'est nécessaire car le logiciel est livré en tant que package autonome.

### Installation Linux
Voici un exemple sur 2 distributions GNU/Linux :

#### Sous Debian ou Ubuntu
```bash
$ sudo apt install ./python3-sirilic_1.13.0-1_all.deb
```

#### Sous Arch Linux
```bash
$ sudo pip install sirilic-1.13.0-py3-none-any.whl 
```

Pour démarrer **Sirilic**, cliquer sur l'icône `Sirilic` dans le sous-menu `education`. Alternativement, à partir de la ligne de commande, le nom de la commande est `sirilic`.

#### À partir des sources
Sous Linux, télécharger les sources depuis le dépôt git en le clonant :

```bash
$ git clone https://gitlab.com/free-astro/sirilic.git
```

Le dossier **Sirilic** contient les fichiers suivants:

{{< figure src="sirilic-fr-02b.png" >}}

Il est nécessaire de rendre le fichier `sirilic.pyw` exécutable ou le lancer en ligne de commande dans un terminal :

soit

```bash
$ python sirilic.pyw
```
ou
```bash
$ python3 sirilic.pyw
```

Vous pouvez également associer l’extension `\*.pyw` au programme python (`/usr/bin/python`) dans votre navigateur de fichier. Alors, un simple ou double clic sur le fichier démarrera l’application.

### Installation Windows
Télécharger la dernière version et dézipper l’archive.

Attention, l’archive a un mot de passe car certains antivirus n’aiment pas les exécutables dans une archive zip. Le mot de passe est donc: `sirilic` . Lancer en double cliquant sur l’exécutable Sirilic.exe :

{{< figure src="sirilic-fr-02.png" >}}

## Guide d'utilisation

### Les premiers pas avec Sirilic
Au premier lancement de **Sirilic**, l’interface se présente de la façon suivante:

{{< figure src="sirilic-fr-03.png" caption="Première exécution de sirilic">}}

Elle se décompose en 3 zones, 1 menu et 1 barre de status:

- zone 1 : liste des images du projets
- zone 2 : arborescence du projet dans le répertoire de travail de **Siril**
- zone 3 : onglets de visualisation du processus, d’ajout des fichiers et des paramètres de traitement.

Des infos bulles apparaissent et donnent une aide succincte sur la plupart des champs de saisie ou des boutons.

Avant de lancer le premier traitement, il est nécessaire de configurer le logiciel en définissant ses préférences :

{{< figure src="sirilic-fr-04.png" caption="Configuration de sirilic">}}

Les principaux champs à compléter sont :

- Le dossier de travail de `Siril`
- La localisation de l’exécutable `Siril.exe` (`siril-cli` sur d'autres OS)
- Option `nettoyage`: elle détruit les fichiers `\*.fit` des sous-répertoires (`-flats`, `-darks`, `-offsets`, `-images`) avant de faire la copie (il est conseillé de laisser l’option cochée, ça évite des effets de bord si on fait des essais consécutifs dans le même dossier).
- Option `lien symbolique` : **Sirilic** offre la possibilité de ne plus copier les images du dossier de travail mais d’utiliser des liens symboliques. Cela permet de gagner en rapidité et en occupation disque par rapport à la copie des fichiers. 
A noter que pour utiliser les liens sous **Windows10**, il est nécessaire de cocher le mode développeur de Windows : `Paramètres → Mise à jour et sécurité → Zone développeur`
Les liens symboliques ne fonctionnent pas sur les partitions FAT32.
Pour **Windows 7 & 8**, le mode dévéloppeur n'existe pas. Il faut lancer **Sirilic** en mode ***Administrateur***.
- Option `retour dans le dossier de travail` permet de revenir dans le dossier de travail à la fin du script.
- Option `Après avoir exécuté le script, lancer Siril` permet lancer **Siril** en fin du script.
- Option `Mode des séquences` : Elle permet de choisir comment Siril gère ces séquences de fichiers 
  - Une seule image par FITS : une séquence est composée de fichiers individuels FITS
  - Plusieurs images par FITS : une séquence est composée d'un fichier FITS qui intègre toutes les images
  - SER : une séquence est composée d'un fichier SER qui intègre toutes les images
- Les options `CPU` et `Ratio mémoire` permettent de forcer les paramètres par défaut de **Siril**. Une valeur à zéro signifie que l’option n’est pas prise en compte.

### Création d'un projet
{{< figure src="progress_Sirilic-fr-1.png" >}}

Pour illustrer la procédure, j’ai pris les images de M31 issues d’un appareil photo numérique (EOS1200D). La première étape consiste donc à créer un projet. Pour cela, cliquer sur le menu `Fichier` puis `Nouveau` pour faire apparaître la boîte de dialogue suivante :

{{< figure src="sirilic-fr-05.png" caption="Création d'un nouveau projet">}}

Renseigner le nom de l’objet photographié, le nom de la session et cocher la ou les couches à traiter puis valider la boite de dialogue. Une ligne va apparaître dans la zone en haut à gauche. Son status est marqué `non-initialisé`.

{{< figure src="sirilic-fr-06.png" caption="Projet créé">}}

A noter que des objets ou couches supplémentaires peuvent être ajoutés ou édités via le menu `projet`.

{{< figure src="sirilic-fr-07.png" caption="Ajout d'un objet ou d'une couche">}}

### Ajout des images, flats, darks à chaque couche
{{< figure src="progress_Sirilic-fr-2.png" >}}

Sélectionner la ligne nommée `M31`, puis cliquer l’onglet `Fichiers` de la zone de droite de l’interface. Il faut ensuite renseigner avec la liste des fichiers (`Image`, `Offset`, `Dark`, `Flat`, `Dark-Flat`) de chaque couche du projet.

Il existe différentes façon d’ajouter les fichiers :

- en cliquant sur le bouton `ADD` 
- en utilisant son gestionnaire de fichier en utilisant la technique `Drag’n Drop`
- en éditant une ligne et en mettant un pattern (ex : `chemin_des_fichiers/L\*.fit` )

{{< figure src="sirilic-fr-08.png" caption="Ajout des fichiers">}}

Note : La détection des fichiers maîtres est automatique. En effet, si l'on ne met qu’un seul fichier dans `Offset`, alors Sirilic considère que c’est un offset maître (idem pour `Dark`, `Flat` et `DarkFlat` ).

### Configuration de chaque couche du projet
{{< figure src="progress_Sirilic-fr-3.png" >}}

L’étape suivante consiste maintenant à sélectionner l’onglet `Propriétés` de la zone à droite de l’interface. Cela permet de choisir diverses options et paramètres pour chaque type d’image (`Luminance`, `Dark`, `Flat` et `DarkFlat`) :

- Type d’empilement,
- Seuils,
- Type de normalisation,
- Retrait de l’offset ou pas,
- etc ...

Les options correspondent aux paramètres des commandes de Siril. Pour avoir de plus amples informations, il faut se référer à la documentation des [commandes de Siril](https://siril.readthedocs.io/fr/stable/Commands.html).

Note : L’option `copie de la librairie par Siril` est pour une utilisation très spécifique. Dans la plupart des cas, elle sera décochée. Elle est à cocher si et seulement si la librairie master n’existe pas au lancement du script mais sera créée par la couche précédente. Alors Siril recopie la librairie dans le dossier master de la couche courante.

{{< figure src="sirilic-fr-09.png" caption="Définition des options de traitement">}}

Le premier onglet nommé `processus` a juste pour objet de visualiser graphiquement le traitement appliquée à la couche sélectionnée. Cela permet au néophyte de comprendre les traitements que va effectuer le script sur la couche en question.

{{< figure src="sirilic-fr-10.png" caption="Visualisaiton du processus">}}

### Paramètres globaux du projet
{{< figure src="progress_Sirilic-fr-4.png" >}}

La dernière étape avant de lancer le script consiste à remplir les propriétés globales du projet. 

Il faut sélectionner le menu `Projet → Editions des propriétés` pour y accéder.

{{< figure src="sirilic-fr-15.png" >}}

Dans notre exemple (*APN*), le paramètre `égalise (CFA)` est coché. 

Sirilic peut traiter des images du même objet prises sur plusieurs nuits dans les mêmes conditions : avec à peu près le même cadrage, les mêmes dimensions d'image, le même filtre utilisé le cas échéant, mais comment la caméra a été déplacée entre les deux nuits les flats doivent être refaits, éventuellement la température est différente et un ensemble de nouveaux darks doit également être fait ... L'option `Multi-session` n'a aucun effet sur le traitement lorsqu'il n'y a qu'une seule session. Le tableau suivant explique l'effet de la combinaison de `Multi-Session` et `Empilement intermédiaire`.

{{< figure src="sirilic-fr-16.png" >}}

Avant de poursuivre, Il est intéressant de sauvegarder le projet en avec `Fichier` et `Sauvegarder`.

### Structuration des dossiers et copie des images
{{< figure src="progress_Sirilic-fr-5.png" >}}

Le projet étant configuré, il faut maintenant :

- Construire l’arborescence des répertoires dans le dossier de travail
- Nettoyer les fichiers d’un précédent traitement avant la copie
- Copier les fichiers ou créer des liens sur les images

Cliquer sur l’étape 1 dans le menu `Actions`.

{{< figure src="sirilic-fr-11.png" caption="Construction des dossiers et copie des images">}}

Le déroulement des opérations est affiché dans la fenêtre de log :

{{< figure src="sirilic-fr-12.png" >}}

### Traitement des images
{{< figure src="progress_Sirilic-fr-6.png" >}}

C’est l’étape finale : il faut cliquer sur l’étape 2 du menu `Actions`.

{{< figure src="sirilic-fr-13.png" caption="Exécution du script Siril">}}

Le déroulement du traitement des images par Siril s’affiche dans la fenêtre de log. Il se fait en plusieurs étapes :

- Création d'un script `sirilic-part1.ssf` dans le sous-dossier `script` du dossier de travail
- Lancement du script `sirilic-part.ssf` :
  - Il crée les fichiers maîtres et pré-traite toutes les images. 
  - Dans le cas d’une couche avec une seule session, il aligne et empile.
  - Pour les couches ayant plusieurs sessions:
    - Sirilic regroupe toutes les sessions d’une couche en une seule (Merge)
    - Il aligne et empile les sessions fusionnées. 
- Pour une caméra CCD mono, il aligne les couches L, R, V, B, Ha... Ainsi elle sont prêtes à être combinées sous Siril pour former la photo couleure finale.
- Le résultat du traitement est stocké directement dans le sous-dossier `objet` (dans notre cas, c’est `M31`) à la racine du dossier de travail de **Siril**.

{{< figure src="sirilic-fr-14.png" caption="Affichage de la séquence de traitement de Siril">}}

Si l'option `Après avoir exécuté le script, lancer Siril` est cochée dans les préférences, alors **Siril** est lancé automatiquement en fin de script.

{{< figure src="sirilic-fr-17.png" caption="Dernière étape : le lancement de Siril">}}

### Le mode Multi-session
Le fonctionnement est assez simple , il suffit de cocher le mode `multi-session` dans les options du projet et de créer une couche avec plusieurs sessions. La fusion des couches se fait automatiquement avant l’alignement des images. 

Par exemple, le projet NGC281 ci-dessous a 2 sessions pour ses couches L et Ha.

{{< figure src="sirilic-fr-18.png" caption="Exemple de multi-session">}}

Le dossier `Group` de la couche Ha (ou L) contient la fusion des images des 2 sessions `S01` et `S02`.

{{< figure src="sirilic-fr-19.png" caption="Arborescence d'une multi-session">}}
