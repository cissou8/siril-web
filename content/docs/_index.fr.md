---
title: "Docs"
author: Cyril Richard
menu: "main"
weight: 4
---

## Documentation
### Siril
* [Documentation en ligne](https://siril.readthedocs.io/fr/stable/)
* [Documentation PDF](https://siril.readthedocs.io/_/downloads/fr/stable/pdf/)
* [Documentation HTMLzip](https://siril.readthedocs.io/_/downloads/fr/stable/htmlzip/)

### Outils externes
* Le compagnon de Siril : [Sirilic](sirilic)

## Commandes de Siril
* [Liste des commandes (version officielle)](https://siril.readthedocs.io/fr/stable/Commands.html)
* [Liste des commandes (version de développement)](https://siril.readthedocs.io/fr/latest/Commands.html)

## Pages du manuel Siril
* [siril](man)
