---
title: "A propos"
author: Argonothe
date: 2018-06-26T14:38:41+00:00
menu: "main"
weight: 1
---

### Siril est un outil de traitement d&#8217;images astronomiques.

Il est spécialement conçu pour la réduction du bruit et l&#8217;amélioration du rapport signal/bruit d&#8217;une image issue de plusieurs captures, comme l&#8217;exige l&#8217;astronomie.

Siril peut aligner automatiquement ou manuellement, empiler et améliorer des images provenant de différents formats de fichiers, même des fichiers de séquences d&#8217;images (films et fichiers SER).

Les contributeurs [sont les bienvenus](https://gitlab.com/free-astro/siril/-/blob/master/CONTRIBUTING.md).

Le langage de programmation est le C, avec des parties en C++. Le développement principal est fait avec les versions les plus récentes des bibliothèques partagées sous GNU/Linux.

Si vous utilisez Siril dans votre travail, veuillez citer ce logiciel en utilisant les informations suivantes :</br>
C. Richard *et al.*, Journal of Open Source Software, 2024, 9(102), 7242. [DOI : 10.21105/joss.07242](https://doi.org/10.21105/joss.07242).

<span class="__dimensions_badge_embed__" data-doi="10.21105/joss.07242"></span><script async src="https://badge.dimensions.ai/badge.js" charset="utf-8"></script>
