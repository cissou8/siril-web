---
title: Soutenez-nous
author: Cyril Richard
type: page
date: 2020-09-06T11:09:49+00:00
menu: "main"
weight: 10
---

<div id='thank-you' class='mb-3 shadow border border-primary p-2 rounded-3 d-none'>
  <h2 class='mt-0'>Merci d'avoir téléchargé Siril! 🎉</h2>

Nous travaillons dur pour vous fournir cet outil génial et si vous souhaitez nous aider dans nos efforts, pensez à faire un don. 
Nous apprécions grandement tout soutien !
</div>


Développer des logiciels est amusant, mais cela prend aussi presque tout notre temps libre. Si vous aimez Siril et souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un don du montant de votre choix.

### PayPal

Nous utilisons Paypal pour [recevoir les donations][] :

<img class='mb-2' src="/images/PP QR Code.png" alt="PayPal QR Code" width="142"/>

<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="business" value="paypal@free-astro.org" />
<input type="hidden" name="currency_code" value="EUR" />
<input type="image" src="/images/btn_donate_pp_142x27.webp" border="0" name="submit" title="PayPal - Le moyen le plus sûr et le plus simple de payer en ligne !" alt="Faire un don avec le bouton PayPal" />
</form>

### Liberapay

[Faites un don à Siril avec Liberapay](https://liberapay.com/Siril). Liberapay est une plateforme gérée par une organisation à but non lucratif permettant un financement participatif mensuel (sur abonnement). Voir la page Wikipedia à [Liberapay](https://fr.wikipedia.org/wiki/Liberapay).

### Dogecoin

Dogecoin (DOGE) est une crypto-monnaie créée par les développeurs Billy Markus et Jackson Palmer, qui ont décidé de créer un système de paiement instantané, amusant et exempt de frais bancaires traditionnels. Pour plus de détails, voir la page Wikipedia du [Dogecoin](https://fr.wikipedia.org/wiki/Dogecoin).

Addresse Dogecoin : `D9LBbHKphb9aiJ22r8yeKijT4igj1Bd3Ri`
{{<figure src="/images/icons/dogecoin.svg" width="125">}}

[recevoir les donations]: https://www.paypal.com/donate?business=paypal%40free-astro.org&item_name=Siril+and+the+free-astro+team&currency_code=EUR


<script type='text/javascript'>
const searchParams = new URLSearchParams(window.location.search);

if( searchParams.has('dl') && searchParams.get('dl') == 1 ){
  document.getElementById('thank-you').classList.remove( 'd-none' );
}
</script>
