---
title: FAQ
author: Vincent Hourdin
type: "faq"
date: 2020-12-19T11:09:49+00:00
menu: "main"
weight: 4
---


# Questions d'utilisation

## Je n'ai pas de flats, comment puis-je utiliser des scripts ?

Les scripts officiels sont conçus pour traiter les images de la meilleure façon possible, ce qui suppose que vous ayez fait des darks, flats et offsets. Si vous avez des fichiers différents ou pas tous les DOFs, vous devrez soit modifier le script (voir [ci-dessous](#comment-créer-un-script-)), soit en trouver un qui répond à vos besoins dans la [page dédiée](https://siril.readthedocs.io/fr/stable/Scripts.html#getting-more-scripts) ou ailleurs.

## J'ai pris en photo un objet lumineux avec différents temps de pose, comment puis-je faire avec Siril ?

Pour certains objets très lumineux, comme M42, M31 ou bien aussi M13, on peut être tenté de faire plusieurs temps de pose. Ceci permet d'obtenir un jeu d'image, celui à l'exposition la plus courte, où les pixels les plus lumineux ne sont pas saturés. Bien qu'on peut être tenté de tout empiler ensemble une fois nos images débarassées des Darks, Flats et Offsets, ceci est en fait une très mauvaise idée. En effet, pour que l'algorithme d'empilement donne un résultat optimal il faut un jeu d'images cohérent. Et même si l'algorithme de normalisation va mettre au même niveau toutes les images, des pixels saturés vont être comparés à des pixels non saturés et ceci ne peux pas donner de bons résultats. Nous conseillons donc d'empiler chaque jeu à temps de pose constant (voir comment traiter plusieurs sessions [here](#multisession)), puis, à la fin, de fusionner les images avec des calques dans un logiciel tel que GIMP.

## Comment traiter plusieurs sessions ?

<a name="multisession"></a>Si vous avez photographié un objet en plusieurs sessions (nuits) et que vous avez dû déplacer la caméra entre elles, vous aurez un ensemble d'images flats pour chaque nuit, éventuellement un ensemble de darks si la température a changé de manière significative.

La bonne façon de procéder est:
1. Calibrez toutes les sessions indépendamment avec les fichiers maîtres
2. Alignez toutes les images prétraitées ensemble. Pour cela, vous devez créer une nouvelle séquence contenant tous les fichiers, ce qui peut être fait de plusieurs manières:
    * A l'étape de conversion, spécifiez un index de départ. Pour la deuxième session par exemple, utilisez un index de départ supérieur au nombre d'images de la première séquence, et pour la troisième supérieur à la somme des deux précédentes, et ainsi de suite. Ensuite, vous pouvez simplement mettre tous les fichiers dans le même répertoire et ils seront détectés comme une séquence s'ils ont le même préfixe dans leur nom.
    * Après le prétraitement, vous pouvez renommer les images pour ne pas s'écraser les unes les autres, avec un utilitaire ou un script shell.

Si cela semble trop compliqué, toute la procédure peut être effectuée automatiquement avec [sirilic](../docs/sirilic) après une brève configuration!

## Dans quel ordre utiliser les fonctions de traitement d'image ?

Suite à l'empilement, l'image peut être améliorée, et Siril fournit de nombreux outils pour le faire. Mais l'ordre d'utilisation n'est pas forcément évident, ni forcément le même pour tout le monde, voici quand même une liste ordonnée typique des opérations possibles :
1. **Recadrage de l'image**. des bordures de couleur différentes apparaissent lors du traitement, et faussent les statistiques utilisées par plusieurs outils
2. **Suppression du gradient de fond de ciel**. On peut aussi le supprimer automatiquement sur la séquence d'images brutes, c'est plus efficace si l'acquisition est longue et que le gradient varie beaucoup.
3. **Étalonnage des couleurs**, manuel ou par calibration photométrique des étoiles. Dans la 0.99.8, les annotations deviennent disponibles après résolution astrométrique.
4. **Déconvolution** de l'image
5. **Transformation de l'histogramme**. Avec la transformation asinh ou l'outil transformation d'histogramme ou les deux. CLAHE convient aussi pour les galaxies.
6. **Suppression du surplus vert**
7. Réglage de la **saturation des couleurs**
8. **Rotation de l'image** si besoin. Si l'image était à l'envers, la résolution astrométrique l'a remise dans le bon sens.
9. **Sauvegarde au format Web**

## Y a-t-il un inconvénient à utiliser des scripts ?

Oui. Les scripts automatisent le traitement en faisant ce qui fonctionne généralement bien, mais dans la vraie vie, vous devrez peut-être modifier quelques paramètres, vérifier que tout se passe bien à chaque étape, rechercher des messages d'avertissement et d'erreur. Quelques traitements courants à ajouter à un script sont : la suppression du gradient image par image, la modification des paramètres de détection des étoiles, la suppression de certains fichiers non nécessaires et le rejet de certaines mauvaises images du résultat.

Ce dernier peut être facilement ajouté au script lors de la commande d'empilement finale avec -filter-fwhm=75% par exemple pour conserver 75% des images les plus fines. Voir les arguments possibles dans la [référence des commandes](https://siril.readthedocs.io/fr/stable/Commands.html#stack).

Sinon, vous pouvez simplement exécuter l'empilement manuellement après l'exécution du script. Réglez le répertoire de travail sur le répertoire qui contient la séquence alignée (dans le dossier `process` si créée par un script officiel), allez dans l'onglet séquences, cliquez sur *rechercher les séquences*, sélectionnez la séquence alignée, en général préfixée par `r_pp_`, et allez dans l'onglet d'empilement. Vous y verrez de nombreuses options, y compris la possibilité d'ajouter plusieurs filtres.

## Pourriez-vous ajouter des commandes pour supprimer des fichiers ou des séquences ?
Réponse courte : Non.

Réponse détaillée : le rôle de Siril n'est pas de nettoyer votre système d'exploitation. C'est une grande responsabilité de supprimer des fichiers en masse en mode non supervisé (via des scripts) et nous préférerions que vous traitiez cette question avec des outils dédiés, tels que bash ou Python ([pySiril](../tutorials/pysiril) par exemple). Ensuite, avec les scripts par défaut, les fichiers intermédiaires sont tous stockés dans le répertoire `process`, qui peut être facilement supprimé. Enfin, il existe une raison importante pour laquelle nous voulons conserver tous les fichiers intermédiaires sur le disque : on peut re-traiter les images à chaque étape sans tout refaire à partir de zéro. Il s'agit donc d'une force, et non une faiblesse.

## Qu'est-ce que Sirilic ?

[Sirilic](../docs/sirilic) est un programme externe qui fournit une interface utilisateur graphique dans laquelle tous les fichiers d'entrée peuvent être documentés et qui exécute Siril pour obtenir le(s) résultat(s) souhaité(s). Les fichiers d'entrée peuvent être de différents types de séquences ou de formats de fichiers, peuvent provenir de capteurs monochromes ou couleur, peuvent provenir de différentes sessions, en utilisant différents filtres. Il pilote Siril pour faire tout le prétraitement et l'empilement en fonction de toutes les informations d'entrée.

## Comment créer un script ?

Les scripts sont des fichiers texte lus par Siril ligne par ligne. Chaque ligne contenant du texte doit être une commande conforme à la [référence des commandes](https://siril.readthedocs.io/en/stable/genindex.html). La meilleure façon d'en écrire un est en général de copier un script existant et de le modifier.

Étant donné que les scripts fournis par Siril sont généralement installés dans un répertoire protégé (comme les *Program files* sur Windows ou /usr ailleurs), il vaut mieux copier le fichier dans le répertoire utilisateur. On peut indiquer à Siril où chercher les scripts dans les préférences. Voir la procédure dans la [documentation des scripts](https://siril.readthedocs.io/fr/stable/Scripts.html).

Regardez attentivement les logs dans la fenêtre console pour vérifier que votre script s'exécute correctement. Si une séquence n'est pas trouvée, il est possible qu'il manque une commande `cd` (change directory) ou que vous ayez changé le nom des séquences en modifiant la façon dont elles sont traitées et vous devez en tenir compte.

## Ma caméra n'est pas refroidie, qu'est-ce que cela change ?

Il est parfaitement normal de ne pas utiliser de caméras thermostabilisées pour l'astronomie tant que le bruit thermique (également appelé courant d'obscurité) est pris en compte dans le prétraitement. Le script officiel utilise un master dark pour calibrer les images d'entrée, en considérant que toutes les images brutes et les darks ont été prises à la même température et au même temps de pose. Avec un APN et autres caméras d'astronomie non refroidies, ce n'est pas le cas, en particulier pour les longues poses (> 30sec).

La bonne façon de traiter ces images est d'utiliser *l'optimisation des darks* dans Siril, qui met à l'échelle le niveau des darks au niveau du bruit additif dans l'image, simulant un master dark fait à une température différente:
1. Vous devrez calibrer les darks, avec une image d'offset maître ou un [offset synthétique](../tutorials/synthetic-biases/). Cette opération est en général la même que pour la calibration des flats.
1. Créez un master dark calibré : pré-traitez les darks en leur soustrayant l'offset maître (ou la valeur), créez le master dark avec la séquence résultante.
1. Modifiez les scripts pour ajouter l'option `-opt` à la commande `calibrate` (voir la [référence de commande](https://siril.readthedocs.io/en/stable/Commands.html#calibrate)) , ou cochez la case *Optimisation* à côté du champ dark maître dans l'onglet de prétraitement si vous traitez manuellement.
1. Assurez-vous d'utiliser le dark maître calibré au lieu du dark normal si vous modifiez un script.
1. Prétraitez vos images avec l'offset (ou la valeur) et le dark maître, sinon l'optimisation ne pourra pas être effectuée.

## Comment traiter des images couleur faites avec un filtre (dual) narrowband ?
Ces filtres deviennent de plus en plus populaires chez les possesseurs de capteurs couleur et Siril intègre bien évidemment un process spécifique pour traiter ces images.

* Pour un traitement par script, des scripts spécifiques sont présents dans les dernières versions de Siril. Sélectionnez `Couleur_Extraction_Ha` ou `Couleur_Extraction_HaOIII` selon que vous utilisez un filtre Halpha ou Halpha/OIII.

* Pour un traitement manuel, il faudra adapter les étapes suivantes:
1. A l'étape de prétraitement, décochez la case `Dématricer avant sauvegarde`.
1. Une fois la séquence prétraitée chargée, allez dans le menu `Traitement de l'image`, `Extraction`, `Séparer les canaux CFA`. Dans le menu déroulant, choisissez la méthode selon le type de filtre. N'oubliez pas de cocher `Appliquer à la séquence` avant de cliquer sur `Appliquer`. A la fin de cette étape, vous obtiendrez 2 séquences avec les préfixes `Ha_` et `OIII_` pour un filtre dualband (une seule si vous avez un filtre Halpha).
1. Pour la suite du traitement, vous devrez aligner et empiler ces deux nouvelles séquences. Différence par rapport au traitement habituel, vous devrez cocher `Drizzle x2 simplifie` dans l'onglet `Alignement`. Cela permettra de récupérer la résolution initiale de votre capteur, que l'extraction des couches avait divisée par 2.
1. Une fois empilée les 2 couches monos, vous pourrez égaliser leurs niveaux avec un `Liner Match`. Chargez l'image OIII empilée, allez dans le menu `Traitement de l'image` , puis `Linear Match`. Sélectionnez l'image Ha empilée comme référence et appuyez sur `Appliquer`. N'oubliez pas de sauver le résultat du traitement en cliquant sur le bouton `Enregistrer`. Cette partie est aussi documentée dans le [tutorial sur la composition RVB](../tutorials/rgb_composition/).

## Quels sont les raccourcis existants ?
On peut accéder à la liste des raccourcis via le menu 'burger' de Siril. Cependant, nous avons fait quelques captures d'écran ici :
{{<figure src="Shortcuts-fr.png" link="Shortcuts-fr.png" width="100%">}}

## Puis-je utiliser Siril sans interface graphique ?
Oui. L'exécutable siril-cli est ce que vous recherchez. Il peut exécuter un script (option `-s`) ou recevoir des commandes depuis l'entrée standard ou depuis un *named pipe* (option `-p`).
Les scripts n'ont pas besoin de spécifier le répertoire de travail et peuvent être suffisamment génériques en définissant le répertoire de travail à partir de la ligne de commande (option `-d`). Voir la page du manuel pour plus d'informations et la [page des scripts](https://siril.readthedocs.io/fr/stable/Scripts.html).

## Comment puis-je contacter l'équipe de Siril ?
L'équipe Siril peut être contactée sur [le forum] (https://discuss.pixls.us/) ou sur [gitlab](https://gitlab.com/free-astro/siril/) pour les rapports de bogues.
Siril est un *logiciel libre*, ce qui signifie que tout le monde peut y contribuer, que ce soit dans le développement, la traduction, la documentation ou le support. Bien entendu, les dons sont les bienvenus.

# Problèmes courants

## Je suis sous Windows et je ne peux pas enregistrer mon image traitée
Certains antivirus, notamment **AVAST**, un antivirus gratuit et largement utilisé, sont connus pour poser problème lors de l'enregistrement des fichiers. Il est alors impossible de créer un nouveau fichier. Pour résoudre ce problème, assurez-vous de bien mettre Siril dans la liste des programmes autorisés par votre antivirus.

## Comment puis-je zoomer et me déplacer dans l'image ouverte ?
Depuis le changement d'interface dans les versions bêta de la 1.0, l'interaction avec les images a changé. Le zoom se fait maintenant avec la molette de la souris tout en appuyant sur une touche `control` (`cmd` sous macOS), le panoramique se fait avec le glissement de la souris tout en appuyant sur une touche `control` aussi. C'est une manière d'interagir avec l'image que l'on retrouve dans de nombreux logiciels modernes.

## Pourquoi les boutons Rétablir/Annuler ne fonctionnent pas ?
Rétablir/Annuler ont besoin de sauvegarder les données dans un dossier afin de les restaurer si nécessaire. Ce dossier s'appelle le répertoire *swap*. Si cela ne fonctionne pas, il est possible que vous deviez aller dans les préférences de Siril et restaurer le répertoire swap.

## Pourquoi mes images sont-elles à l'envers ?
Nous avons une [page dédiée](https://siril.readthedocs.io/fr/latest/file-formats/FITS.html#orientation-of-fits-images) dans la documentation. En résumé, votre logiciel d'acquisition enregistre les images de manière non standard. Vous pouvez donner le lien vers cette page à leur créateur si vous pouvez les contacter, sinon vous pouvez nous dire de quel logiciel il s'agit et nous les contacterons. Notez que si vous n'utilisez pas la dernière version de votre logiciel d'acquisition, il a peut-être déjà été corrigé (la plupart l'utilise en 2021).

## Quel est le motif qui apparaît en arrière-plan ?
Avec des expositions courtes ou avec peu d'images empilées, un motif diagonal peut apparaître dans les zones sombres lorsque l'histogramme est étiré. Ce phénomène est dû à l'appareil photo et il n'est pas facile à éliminer car il a une amplitude plus faible que le signal que nous avons habituellement dans les images et parce qu'il est causé par des pixels qui se comportent de manière asymétrique par rapport à la médiane.

Il existe cependant des moyens de l'éviter : augmenter le nombre d'images ou leur exposition, et surtout, utiliser le dithering. Dans ce cas, le *dithering* désigne le désalignement des images sur le ciel, ce qui oblige l'alignement à compenser. Si les images sont toutes exactement les mêmes, le motif apparaîtra, sinon, il peut être dilué. Si vous utilisez l'autoguidage, il y a probablement une option pour se déplacer aléatoirement toutes les quelques images afin de ne pas avoir les étoiles sur le même pixel tout le temps.

## Pourquoi les couleurs sont-elles irréelles dans l'image empilée ?
Siril ne conserve pas la balance des blancs et n'utilise pas les informations photométriques du capteur dans les étapes de conversion ou de traitement. Le résultat de l'empilement n'a pas besoin d'être conservé tel quel, car il ne peut pas être garanti d'être correct de toute façon. La bonne manière est de corriger les couleurs après empilement, lors du traitement des résultats, soit en utilisant les outils photométriques ou manuels de balance des couleurs, soit dans un logiciel externe.

Cependant, parfois les images semblent très violettes, cela peut indiquer que le motif de Bayer du capteur de couleur a été mal détecté. Regardez la console (dernier onglet dans l'interface graphique) pour voir s'il y a un avertissement lié à cela. Vous devrez peut-être spécifier le motif correct pour la matrice de Bayer dans les préférences si le problème persiste. En cas de doute, contactez-nous.

Il est parfaitement normal d'obtenir une image empilée à dominante verte, car les caméras sont plus sensibles à ces longueurs d'onde.

## Pourquoi les images exportées sont-elles si sombres ?
Les images exportées utilisent les valeurs de pixels réelles des images, pas nécessairement l'image que vous voyez à l'écran. L'image affichée est transformée par une fonction de transfert vers l'écran, souvent définie comme *Auto-ajustement* pour les images du ciel profond dans la partie inférieure de l'interface graphique. Les valeurs réelles des pixels ne sont visibles qu'avec la fonction *Linéaire* et avec les curseurs de coupure bas et haut réglés sur les valeurs minimale et maximale. Les images semblent sombres comme ça, parfois même complètement noires.

Pour exporter l'image avec un bon signal, réglez le mode en *Linéaire*, 0 pour le curseur bas, la valeur maximale pour le haut et utilisez les outils de transformation d'histogramme (Asinh, Histogramme ou CLAHE du menu *Traitement d'image*) pour changer les valeurs des pixels. Ensuite, vous pouvez exporter l'image, elle sera comme vous la voyez.

## Pourquoi les images TIFF/PNG 32 bits exportées dans Photoshop/Lightroom sont-elles si fades, mais pas dans GIMP?
Par défaut, Siril enregistre le profil de couleur sRGB dans les images TIFF / PNG: ce comportement peut d'ailleurs être modifié pour les images TIFF. Photoshop, Lightroom et de nombreux autres logiciels n'utilisent pas ce profil de couleur par défaut et la façon dont ils affichent l'image peut apparaître étrange s'ils ne lisent pas le profil intégré. Dans ce cas, pas besoin de jouer avec les courbes gamma ou le contraste et la luminosité, assurez-vous simplement que votre logiciel lit l'image en tant que sRGB. C'est le comportement par défaut de GIMP et c'est pourquoi il semble lire correctement votre image dans tous les cas.
{{<figure src="sRGB_fr.png" link="sRGB_fr.png" caption="Lire l'image avec un profile sRGB" >}}

## Pourquoi l'alignement ne trouve-t-il pas d'étoiles dans mes images ?
Cela peut se produire avec des images qui ont un niveau de signal inhabituel, soit un rapport signal / bruit trop faible, soit dont le signal des offsets a été supprimé deux fois, laissant l'arrière-plan avec de nombreuses valeurs nulles. Cela peut également arriver avec des images qui ont des étoiles très allongées en raison d'un mauvais suivi de la rotation terrestre.

Pour y remédier, ouvrez la fenêtre de dialogue *Dynamique PSF*, soit depuis les paramètres de l'alignement global, soit depuis le menu hamburger (trois lignes horizontales en haut à droite de la fenêtre) puis dans le sous-menu Information de l'Image. Vous trouverez ici quelques paramètres à modifier et un bouton pour démarrer la détection d'étoiles. Si vous trouvez un paramètre qui détecte au moins 20 étoiles dans vos images, vous pouvez redémarrer l'alignement, il utilisera ces paramètres. Si vous fermez Siril, vous devrez recommencer.

## Pourquoi la résolution astrométrique échoue-t-elle ?
Siril peut effectuer une résolution astrométrique à partir d'une base de données en ligne, que ce soit à des fins d'astrométrie ou d'étalonnage photométrique. Il peut échouer pour quelques raisons :
* L'image que vous essayez de résoudre n'est pas linéaire, ce qui signifie que vous avez déjà étiré son histogramme. Dans ce cas, la luminosité relative entre les étoiles n'est pas préservée et elles ne peuvent pas être identifiées.
* La taille des pixels ou les distances focales ne sont pas assez proches des valeurs réelles. N'oubliez pas de tenir compte des emplacements des lentilles de correction ou de zoom dans le chemin optique. Parfois, un petit changement au-dessus ou au-dessous des valeurs réelles peut suffire pour que cela fonctionne.
* Rarement, il est tout simplement trop difficile de résoudre une image automatiquement. Si vous pouvez partager l'image défaillante avec nous, cela peut nous aider à améliorer l'algorithme.
* Avec la version 1.0, vous pouvez dessiner une sélection dans l'image autour de l'objet que vous avez recherché dans la fenêtre de résolution astrométrique, utile pour les champs larges.

## Comment supprimer un gradient de fond de ciel complexe ?
Il arrive fréquemment que le gradient de fond de ciel soit trop complexe pour être supprimé avec un polynôme de degrés quatre, même si le master flat est parfaitement réalisé. Cela est dû à une pollution lumineuse trop changeante ou à la luminosité de la lune ou de l'horizon par rapport à l'angle des images acquises dans le temps. Les versions récentes de Siril fournissent un moyen de gérer cela : supprimer un gradient linéaire (polynôme de degré un) de toutes les expositions individuelles au lieu de le faire après l'empilement. Le résultat est en général bien meilleur, mais il prend plus de temps à traiter.

* Traitement manuel :
Après le prétraitement, aller dans le Menu `Traitement de l’image`->`Extraction du gradient`. Choisir un polynôme d’ordre 1 ou 2 puis cocher la case `Appliquer à la séquence`. Cliquer sur `Appliquer` puis procéder ensuite à l’alignement.

* Traitement par script ou par la console :
Ajouter une [commande `seqsubsky` ](https://siril.readthedocs.io/fr/stable/Commands.html#seqsubsky) après l’étape de prétraitement. Ne pas oublier de modifier la commande d’alignement suivante pour appeler la séquence préfixée avec `bkg_pp_`.

Note: Si vous utilisez l'extraction de couches sur des images couleurs (Ha or Ha/OIII), ce traitement doit être fait **après** l'extraction et donc sur chaque couche.

## Pourquoi reçois-je des messages d'erreur avec mes fichiers AVI ?
Bien que Siril soit capable de convertir, de charger et d'effectuer le traitement de base des AVI et de n'importe quel autre fichier vidéo, les algorithmes les plus complexes ne sont pas disponibles pour ce type de séquences. Nous avons [une page](https://siril.readthedocs.io/fr/latest/file-formats/Common.html#avi-format) dans la documentation expliquant pourquoi ces formats de fichiers ne devraient pas être utilisés pour l'astronomie et pourquoi le format SER devrait être utilisé à la place.

Les versions récentes de Siril peuvent également utiliser une séquence FITS à fichier unique, appelé parfois FITS cube, ou fitseq dans Siril. Par rapport au SER, le principal avantage est qu'il peut contenir des valeurs de pixel de 32 bits par canal, là où le SER est limité à 16. Le format FITS peut également utiliser la compression de fichiers de manière transparente.

## Pourquoi Siril est-il si lent ou bloque mon ordinateur ?
Le traitement des images est une opération gourmande en ressources. Siril essaie d'utiliser tous les cœurs et threads de mémoire et de processeur qu'il peut obtenir. En général, il s'adapte bien aux différentes conditions de fonctionnement, mais parfois il détecte mal la mémoire disponible, ou une seule image peut être trop volumineuse pour tenir entièrement en mémoire pour les différentes étapes de traitement. Dans ce cas, la mémoire virtuelle du système d'exploitation est utilisée si elle existe, et c'est extrêmement lent car il utilise les disques durs.

L'autre raison pourrait être que vous utilisez un disque dur rotatif, ce que vous ne devriez pas faire pour le traitement d'images car il est trop lent et s'use trop vite.

Recommandations:
* Quittez tous les autres logiciels que vous pouvez quitter lors de l'exécution de Siril, y compris l'antivirus qui ne fera que ralentir l'ordinateur et les navigateurs Web qui sont d'énormes consommateurs de mémoire
* Si vous souhaitez toujours utiliser votre ordinateur à d'autres fins pendant que Siril traite des images, vous pouvez réduire la quantité de threads de processeur que Siril utilisera en haut de l'interface graphique ou avec la [commande setcpu](https://siril.readthedocs.io/fr/stable/Commands.html#setcpu) ou réduire la quantité de mémoire que Siril détectera comme libre dans les préférences ou avec la [commande setmem](https://siril.readthedocs.io/en/stable/Commands.html#setmem). Dans tous les cas, si la mémoire disponible diminue pendant que Siril fonctionne, Siril peut manquer de mémoire et freezer ou planter votre système.
* Avoir au moins 2 Go de mémoire disponible pour le traitement d'image monochrome, 4 Go pour les images couleur. Cela dépend bien sûr de la taille des images.
* N'activez pas les images 32 bits par canal si vous n'avez pas beaucoup de mémoire (il peut être forcé à 16 dans les préférences, ce n'est pas la valeur par défaut dans les versions 0.99 et plus).
* N'utilisez pas d'alignement sub-pixels (également appelé *Drizzle* simplifié) dans l'interface.

Si vous pensez que la mémoire libre est mal détectée, contactez-nous, nous ne développons pas avec tous les systèmes d'exploitation.

## Que dois-je faire si mon image traitée n'a pas l'air bonne ?
Si vous utilisez des scripts, vous ne voyez que le résultat de l'empilement pouvez ne pas avoir remarqué que quelque chose a mal tourné avant. C'est une bonne idée de placer Siril dans le répertoire `process` après l'exécution du script, pour vérifier quelques points :
* Ouvrez les fichiers de calibration maître, puisqu'il s'agit d'images empilées, leur nom se termine par `_stacked.fit`, comme dark_stacked.fit et pp_flat_stacked.fit (`pp_` au début signifie qu'il a été calibré). Vérifiez qu'il n'y a pas de lumières parasites, que les niveaux de pixels sont corrects.
* Dans l'onglet séquences, cliquez sur `Chercher sequences` pour lister les séquences disponibles dans le répertoire. Vous pouvez les passer en revue, en ouvrant la liste des images de la séquence (bouton à droite de celui-ci), en vérifiant qu'il n'y a pas eu de problème évident pendant l'acquisition comme un mouvement du télescope ou des nuages.
* Si les images d'entrée semblent bonnes, ouvrez la séquence alignée, nommée par défaut `r_pp_lights_`, pour voir le tracé des données d'alignement pour chaque image, afin de voir comment il a évolué pendant la session. Peut-être que la mise au point s'est dégradée à un moment donné.

Si le bruit est trop élevé, peut-être avez-vous simplement besoin de plus de temps d'expositions, ou d'utiliser le [dithering](#quel-est-le-motif-qui-apparaît-en-arrière-plan-).
